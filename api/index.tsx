import Axios from "axios";
import * as Sentry from "@sentry/browser";

export default class api {
  getState: () => any;
  dispatch: () => any;
  maxRedirects: number;
  xhr: any;
  time_out: number;
  key: string;
  params: any;

  constructor(obj) {
    const { auth } = obj.getState();
    const headers = {
      "Content-Type": obj.content_type || "application/json; charset=UTF8",
    };

    this.params = {};

    if (auth.key) {
      this.params.token = auth.key;
    }

    this.key = auth.key || "";
    this.time_out = 10000;
    this.getState = obj.getState;
    this.dispatch = obj.dispatch;
    this.maxRedirects = 2;
    this.xhr = Axios.create({
      // baseURL: obj.baseUrl || "http://192.168.1.100:8000/v1/",
      // baseURL: obj.baseUrl || "http://192.168.1.16:8000/v1/",
      // baseURL: obj.baseUrl || "http://5.63.9.73:8000/v1/",
      baseURL: obj.baseUrl || "https://news.onmiz.org/v1/",
      // baseURL: obj.baseUrl || "http://5.63.9.72:8000/v1/",
      headers,
      responseType: obj.response_type || "json",
    });
  }

  handleErr = (err) => {
    if (err.response) {
      Sentry.captureException(JSON.stringify(err.response));
      if (err.response.status === 401) {
        // Logout...
      }
    } else {
      Sentry.captureException(
        JSON.stringify({
          err,
          message: "XHR error without any response",
        })
      );
    }
    if (!err.response) {
      // notif("سرور متصل نمی‌باشد", "danger", 3);
    } else if (err.response > 499 && err.response < 600) {
      // notif("خطا در اتصال، مجدداً تلاش کنید", "danger", 3);
    }
  };

  handleRes = (res) => {};

  Get(url, params) {
    return new Promise((resolve, reject) => {
      const data: any = { method: "get", params: { ...this.params } };
      if (params) data.params = { ...this.params, ...params };
      this.xhr
        .request(url, data)
        .then((res) => {
          this.handleRes(res);
          resolve(res);
        })
        .catch((err) => {
          this.handleErr(err);
          reject(err);
        });
    });
  }

  Put(url, params) {
    return new Promise((resolve, reject) => {
      let itemParams = { ...this.params };
      if (params) itemParams = { ...this.params, ...params };
      this.xhr
        .put(url, itemParams)
        .then((res) => {
          this.handleRes(res);
          resolve(res);
        })
        .catch((err) => {
          this.handleErr(err);
          reject(err);
        });
    });
  }

  Post(url, params) {
    return new Promise((resolve, reject) => {
      let itemParams = { ...this.params };
      if (params) itemParams = { ...this.params, ...params };
      this.xhr
        .post(url, itemParams)
        .then((res) => {
          this.handleRes(res);
          resolve(res);
        })
        .catch((err) => {
          this.handleErr(err);
          reject(err);
        });
    });
  }

  Upload(url, data, uploadCallback) {
    return new Promise(async (resolve, reject) => {
      this.xhr
        .post(url, data, {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data;",
          },
          onUploadProgress: uploadCallback
            ? (e) => {
                let progress = Math.floor((e.loaded * 100) / e.total);
                uploadCallback(progress);
              }
            : null,
        })
        .then((res) => {
          this.handleRes(res);
          resolve(res);
        })
        .catch((err) => {
          this.handleErr(err);
          reject(err);
        });
    });
  }

  Delete(url, data) {
    return new Promise((resolve, reject) => {
      let itemdata = { ...this.params };
      if (data) itemdata = { ...this.params, ...data };
      this.xhr
        .delete(url, { data: itemdata })
        .then((res) => {
          this.handleRes(res);
          resolve(res);
        })
        .catch((err) => {
          this.handleErr(err);
          reject(err);
        });
    });
  }
}
