import { FETCH_PACKAGES } from "../types";

export const fetchPackages = () => async (dispatch, getState, { Api }) => {
  const api = new Api({ dispatch, getState });
  try {
    const payload = { type: FETCH_PACKAGES, list: [], loading: false };
    dispatch({ ...payload, loading: true });
    const { data } = await api.Get("premium_packages", { sort: { _id: -1 } });
    dispatch({ ...payload, list: data.data.list });
  } catch (err) {
    // handle err
  }
};
