import { FETCH_HOME } from "../types";

export const fetchHome = (isHeader?) => async (dispatch, getState, { Api }) => {
  const api = new Api({ dispatch, getState });

  try {
    const payload = {
      type: FETCH_HOME,
      news: [],
      sources: [],
      header_news: [],
      loading: false,
    };

    const param: any = {};

    if (isHeader) {
      param.header = "header";
    }

    dispatch({ ...payload, loading: true });

    const { data } = await api.Get("", param);

    dispatch({ ...payload, ...data.data.item });
  } catch (err) {
    //  handle err...
  }
};
