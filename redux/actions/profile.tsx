import {
  CHAGNE_PROFILE_ACTIVE,
  FETCH_PROFILE,
  FETCH_CATEGORIES,
  HANDLE_AUTH,
  FETCH_PAYMENTS,
} from "../types";
import { changeColor } from "./changeColor";
import Router from "next/router";
import { logout, refreshToken } from "./auth";

export const changeActiveProfile = (count) => (dispatch) => {
  dispatch({ type: CHAGNE_PROFILE_ACTIVE, count });
};

export const editProfile = (data, isPass?) => (dispatch, getState, { Api }) => {
  const api = new Api({ dispatch, getState });
  const { user } = getState();

  return new Promise(async (resolve, reject) => {
    try {
      // if (!isPass)
      //   dispatch({ type: FETCH_PROFILE, item: { ...user.item }, loading: true });
      const item = { ...user.item, ...data };
      if (!isPass) dispatch({ type: FETCH_PROFILE, item, loading: false });
      const res = await api.Put("profile", data);
      resolve(res);
    } catch (err) {
      reject(err);
    }
  });
};

export const fetchProfile = () => async (dispatch, getState, { Api }) => {
  const api = new Api({ dispatch, getState });

  try {
    const payload = {
      type: FETCH_PROFILE,
      item: {},
      loading: false,
    };

    dispatch({ ...payload, loading: true });

    const { data } = await api.Get("profile", { all: true });

    dispatch(changeColor(data.data.item.theme || "light"));

    dispatch({ ...payload, item: data.data.item });
  } catch (err) {
    // handle err
  }
};

export const fetchCategories = () => async (dispatch, getState, { Api }) => {
  const api = new Api({ dispatch, getState });

  try {
    const payload = {
      loading: false,
      type: FETCH_CATEGORIES,
      list: [],
    };

    dispatch({ ...payload, loading: true });
    const { data } = await api.Get("categories");
    dispatch({ ...payload, list: data.data.list });
  } catch (err) {
    // handle errr...
  }
};

export const handlePayment = ({ package_id }, callback?) => async (
  dispatch,
  getState,
  { Api }
) => {
  const api = new Api({ dispatch, getState });
  const { user } = getState();
  const { item } = user;

  try {
    const { data } = await api.Post("payments", {
      package_id,
      user_id: item.id,
    });
    const { price, seq_id } = data.data.item;
    if (price === 0) {
      await dispatch(refreshToken(data.token));
      await dispatch(fetchProfile());
      Router.push("/");
    } else {
      dispatch(logout());
      window.location.href = `http://mizkhabar.com/payment?email=test@test.com&name=test&PayOrderId=${seq_id}&amount=${price}`;
    }
  } catch (err) {
    if (callback) callback(err);
  }
};

export const fetchPayments = () => async (dispatch, getState, { Api }) => {
  const api = new Api({ dispatch, getState });

  try {
    const payload = { type: FETCH_PAYMENTS, list: [], loading: false };

    dispatch({ ...payload, loading: true });
    const { data } = await api.Get("my_payments", { sort: { _id: -1 } });
    dispatch({ ...payload, list: data.data.list });
  } catch (err) {
    // handle err ...
  }
};
