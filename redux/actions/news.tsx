import {
  FETCH_NEWS,
  FETCH_NEWS_DETAILS,
  FETCH_SOURCES,
  FETCH_MORE_NEWS,
} from "../types";

export const fetchNews = (fetchMore?, txt?, category_id?, url?) => async (
  dispatch,
  getState,
  { Api }
) => {
  const api = new Api({ dispatch, getState });
  const { news, user } = getState();

  const activeFilter: any = (type?) => {
    if (user.item.filters) {
      const _activeFilter = user.item.filters.filter(
        (item) => item.active === true
      )[0];
      if (_activeFilter) {
        return type && type === "checkExists" ? true : { ..._activeFilter };
      }
    }
    return type && type === "checkExists" ? false : {};
  };

  const getKeyWords = () => {
    let arr = [];
    for (let i = 0; i < (activeFilter().keywords || []).length; i++) {
      const item = activeFilter().keywords[i];
      if (item.active) {
        arr.push(item.name);
      }
    }
    return arr;
  };

  try {
    const payload = {
      type: fetchMore ? FETCH_MORE_NEWS : FETCH_NEWS,
      list: [],
      loading: false,
      hasMore: news.hasMore,
    };

    dispatch({ ...payload, loading: true });

    if (category_id) {
      const { data } = await api.Get(url || "news", {
        sort: JSON.stringify({ create_date: -1 }),
        page_size: 20,
        page: 1,
        source_ids: JSON.stringify([]),
        keywords: JSON.stringify([]),
        conditions: JSON.stringify({ category_id }),
      });

      dispatch({
        ...payload,
        list: data.data.list,
        hasMore: data.data.list.length > 0,
      });

      return;
    }

    const { data } = await api.Get(url || "my_news", {
      sort: JSON.stringify({ create_date: -1 }),
      page_size: 20,
      page: fetchMore ? news.pageCount : 1,
      source_ids: JSON.stringify(activeFilter().sources || []),
      keywords: txt
        ? JSON.stringify([txt])
        : JSON.stringify(getKeyWords() || []),
    });

    dispatch({
      ...payload,
      list: data.data.list,
      hasMore: data.data.list.length > 0,
    });
  } catch (err) {
    // handle err...
  }
};

export const fetchSources = () => async (dispatch, getState, { Api }) => {
  const api = new Api({ dispatch, getState });

  try {
    const payload = {
      type: FETCH_SOURCES,
      list: [],
      loading: false,
    };

    dispatch({ ...payload, loading: true });

    const { data } = await api.Get("sources", { page_size: 10000 });

    dispatch({ ...payload, list: data.data.list });
  } catch (err) {
    // handle Err
  }
};

export const fetchNewsDetails = (id) => async (dispatch, getState, { Api }) => {
  const api = new Api({ dispatch, getState });

  try {
    const payload = {
      type: FETCH_NEWS_DETAILS,
      item: {},
      loading: false,
    };

    dispatch({ ...payload, loading: true });

    const { data } = await api.Get(`news/${id}`);

    dispatch({ ...payload, item: data.data.item });
  } catch (err) {
    // handle err
  }
};

export const saveNews = (id, type = "post") => async (
  dispatch,
  getState,
  { Api }
) => {
  const api = new Api({ dispatch, getState });
  const { news } = getState();
  let list = [...news.list];
  const item = list.filter((el) => el.mongo_id === id || el.id === id)[0];
  const index = list.indexOf(item);

  try {
    if (type === "post") {
      await api.Post("save_news", { news_id: id });
      list[index].bookmark = true;
    } else {
      await api.Delete("save_news", { id });
      list[index].bookmark = false;
    }

    dispatch({
      type: FETCH_NEWS,
      list,
      loading: false,
      hasMore: news.hasMore,
    });
  } catch (err) {
    // handle error...
  }
};

export const saveOneNews = (id, type = "post") => async (
  dispatch,
  getState,
  { Api }
) => {
  const api = new Api({ dispatch, getState });
  const { newsDetails } = getState();
  const { item } = newsDetails;
  let mainItem = { ...item };

  try {
    if (type === "post") {
      await api.Post("save_news", { news_id: id });
      mainItem.bookmark = true;
    } else {
      await api.Delete("save_news", { id });
      mainItem.bookmark = false;
    }

    dispatch({
      type: FETCH_NEWS_DETAILS,
      item: mainItem,
      loading: false,
    });
  } catch (err) {
    // handle error...
  }
};
