import Router from "next/router";
import {
  GET_REGISTER_CODE,
  HANDLE_AUTH,
  GET_FORGOT_CODE,
  RESET_CODE_MODE,
} from "../types";
import { setCookie, getCookie, deleteCookie } from "../../helpers/props";
import Cookies from "universal-cookie";
import { fetchProfile } from "./profile";

// 4 days
const time = 60 * 60 * 24 * 4;

export const checkAuth = (req?, res?) => async (dispatch) => {
  let auth = null;

  if (req) {
    const cookies = new Cookies(req.headers.cookie);
    auth = cookies.get("auth");
  } else {
    auth = getCookie("auth");
  }

  if (auth) {
    dispatch({
      type: HANDLE_AUTH,
      payload: req ? auth : JSON.parse(auth),
    });
    await dispatch(fetchProfile());
  }
};

export const logout = () => (dispatch) => {
  const payload = { key: "", role: "guest" };
  deleteCookie("auth");
  dispatch({
    type: HANDLE_AUTH,
    payload,
  });
  Router.push("/");
};

export const autoLogin = (key) => async (dispatch) => {
  const payload = { key, role: "user" };
  dispatch({
    type: HANDLE_AUTH,
    payload,
  });
  setCookie("auth", JSON.stringify(payload), time);
};

export const refreshToken = (key) => (dispatch, getState) => {
  const payload = { key, role: getState().auth.role };

  setCookie("auth", JSON.stringify(payload), time);

  dispatch({ type: HANDLE_AUTH, payload });
};

export const login = (params, callback?) => async (
  dispatch,
  getState,
  { Api }
) => {
  const api = new Api({ dispatch, getState });
  try {
    const { data } = await api.Post("login", params);
    const payload = { key: data.token, role: "user" };

    setCookie("auth", JSON.stringify(payload), time);

    dispatch({ type: HANDLE_AUTH, payload });
    await dispatch(fetchProfile());
    Router.push("/");
  } catch (err) {
    if (callback) callback(err);
  }
};

export const register = (params, callback) => async (
  dispatch,
  getState,
  { Api }
) => {
  const api = new Api({ dispatch, getState });

  try {
    const { data } = await api.Post("register", params);
    dispatch({
      type: GET_REGISTER_CODE,
      payload: { key: data.token, role: "guest", mobile: params.mobile },
    });
  } catch (err) {
    if (callback) callback(err);
  }
};

export const confirmCode = (code, callback) => async (
  dispatch,
  getState,
  { Api }
) => {
  const api = new Api({ dispatch, getState });
  const { auth } = getState();
  // 4 days
  const time = 60 * 60 * 24 * 4;

  try {
    const { data } = await api.Post("confirm_user", {
      activation_code: code,
      token: auth.key,
    });
    const payload = { key: data.data.token, role: "user" };
    setCookie("auth", JSON.stringify(payload), time);
    dispatch({
      type: HANDLE_AUTH,
      payload,
    });
    await dispatch(fetchProfile());
    Router.push("/");
  } catch (err) {
    if (callback) callback(err);
  }
};

export const handleForgot = (forgot_mode?, data?) => async (
  dispatch
  // getState,
  // { Api }
) => {
  // const api = new Api({ dispatch, getState });

  try {
    dispatch({
      type: GET_FORGOT_CODE,
      forgot_mode: forgot_mode || "mobile",
      payload: data ? { ...data } : {},
    });
  } catch (err) {
    // handle err...
  }
};

export const forgotPass = (mobile, callback?) => async (
  dispatch,
  getState,
  { Api }
) => {
  const api = new Api({ dispatch, getState });

  try {
    await api.Post("forgot_password", { mobile: Number(mobile).toString() });
    dispatch(handleForgot("code", { mobile: Number(mobile).toString() }));
  } catch (err) {
    if (callback) callback(err);
  }
};

export const handleResetPass = (
  { new_password, activation_code },
  callback?
) => async (dispatch, getState, { Api }) => {
  const api = new Api({ dispatch, getState });
  const { auth } = getState();
  const { mobile } = auth;

  try {
    await api.Post("reset_password", { new_password, activation_code, mobile });
    dispatch(login({ mobile, password: new_password }));
  } catch (err) {
    if (callback) callback(err);
  }
};

export const resetCodeMode = () => (dispatch) =>
  dispatch({ type: RESET_CODE_MODE });

export const resendCode = (callback?) => async (
  dispatch,
  getState,
  { Api }
) => {
  const { mobile } = getState().auth;
  const api = new Api({ dispatch, getState });

  try {
    await api.Post("resend_verification_code", { mobile });
    if (callback) callback();
  } catch (err) {
    // handle err...
  }
};
