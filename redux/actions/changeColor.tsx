import { CHANGE_THEME } from "../types";
import { darkColors, mainColors } from "../reducer/colors/colorReducer";
import { editProfile } from "./profile";

export const changeColor = (theme) => (dispatch, getState) => {
  const { colors } = getState();
  let themeToChange = theme || colors.theme;
  const payload =
    themeToChange === "dark" ? { ...mainColors } : { ...darkColors };
  dispatch({ type: CHANGE_THEME, payload });
  if (!theme) {
    dispatch(editProfile({ theme: themeToChange }, true));
  }
};
