import { AnyAction } from "redux";
import { FETCH_PACKAGES, FETCH_PAYMENTS } from "../types";

const initialPackages = {
  list: [],
  loading: false,
};

export const packages = (state = initialPackages, action: AnyAction) => {
  switch (action.type) {
    case FETCH_PACKAGES:
      return { ...state, list: action.list, loading: action.loading };

    default:
      return state;
  }
};

const initialPayment = {
  list: [],
  loading: false,
};

export const payments = (state = initialPayment, action: AnyAction) => {
  switch (action.type) {
    case FETCH_PAYMENTS:
      return { ...state, list: action.list, loading: action.loading };

    default:
      return state;
  }
};
