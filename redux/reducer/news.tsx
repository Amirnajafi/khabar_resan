import { AnyAction } from "redux";
import {
  FETCH_NEWS,
  FETCH_NEWS_DETAILS,
  FETCH_HOME,
  FETCH_SOURCES,
  FETCH_MORE_NEWS,
} from "../types";

const initialNews = {
  list: [],
  loading: false,
  pageCount: 1,
  hasMore: true,
};

export const news = (state = initialNews, action: AnyAction) => {
  switch (action.type) {
    case FETCH_NEWS:
      return {
        ...state,
        list: action.list,
        loading: action.loading,
        pageCount: 2,
        hasMore: action.hasMore,
      };

    case FETCH_MORE_NEWS:
      return {
        ...state,
        list: [...state.list, ...action.list],
        loading: action.loading,
        pageCount: state.pageCount + 1,
        hasMore: action.hasMore,
      };

    default:
      return state;
  }
};

const initialNewsDetails = {
  item: {},
  loading: false,
};

export const newsDetails = (state = initialNewsDetails, action: AnyAction) => {
  switch (action.type) {
    case FETCH_NEWS_DETAILS:
      return { ...state, item: action.item, loading: action.loading };

    default:
      return state;
  }
};

const initialHome = {
  news: [],
  sources: [],
  header_news: [],
  loading: false,
};

export const home = (state = initialHome, action: AnyAction) => {
  switch (action.type) {
    case FETCH_HOME:
      return {
        ...state,
        news: action.news,
        sources: action.sources,
        loading: action.loading,
        header_news: action.header_news,
      };

    default:
      return state;
  }
};

const initialSrouces = {
  list: [],
  loading: false,
};

export const sources = (state = initialSrouces, action: AnyAction) => {
  switch (action.type) {
    case FETCH_SOURCES:
      return { ...state, list: action.list, loading: action.loading };

    default:
      return state;
  }
};
