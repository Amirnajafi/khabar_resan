import { AnyAction } from "redux";
import {
  GET_REGISTER_CODE,
  GET_FORGOT_CODE,
  HANDLE_AUTH,
  RESET_CODE_MODE,
} from "../types";

const initialAuth = {
  key: "",
  role: "guest",
  code: "",
  forgot_mode: "",
  mobile: "",
};

export const auth = (state = initialAuth, action: AnyAction) => {
  switch (action.type) {
    case GET_REGISTER_CODE:
      return { ...state, code: "register", ...action.payload };

    case GET_FORGOT_CODE:
      return {
        ...state,
        code: "forgot",
        ...action.payload,
        forgot_mode: action.forgot_mode || "",
      };

    case HANDLE_AUTH:
      return { ...state, code: "", ...action.payload };

    case RESET_CODE_MODE:
      return { ...state, code: "", forgot_mode: "" };

    default:
      return state;
  }
};
