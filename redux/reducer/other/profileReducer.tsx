import { AnyAction } from "redux";
import { CHAGNE_PROFILE_ACTIVE } from "../../types";

export const activeProfile = (state = 1, action: AnyAction) => {
  switch (action.type) {
    case CHAGNE_PROFILE_ACTIVE:
      return action.count;

    default:
      return state;
  }
};
