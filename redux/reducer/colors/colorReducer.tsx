import { AnyAction } from "redux";
import { CHANGE_THEME } from "../../types";

export interface colorInterface {
  theme: string;
  topHead: string;
  main: string;
  main2: string;
  main3: string;
  main4: string;
  main5: string;
  font1: string;
  font2: string;
  font3: string;
  font4: string;
  font5: string;
  font6: string;
  font7: string;
  font8: string;
  blue?: string;
  headBackUrl?: string;
  headLogo?: string;
  footer1: string;
  footer2: string;
  blue2: string;
  green1: string;
  aside1: string;
  border1: string;
  border2: string;
  border3: string;
  border4: string;
  border5: string;
  border6: string;
  btn: string;
}

export const mainColors: colorInterface = {
  theme: "light",
  topHead: "#313131",
  main: "#ffffff",
  main2: "#FFFFFF",
  main3: "#f6f6f6",
  main4: "#ffffff",
  main5: "#9BA1AE",
  font1: "#313131",
  font2: "#5D5D5D",
  font3: "#5D5D5D",
  font4: "#374E88",
  font5: "#374E88",
  font6: "#313131",
  font7: "#5d5d5d",
  font8: "#374E88",
  blue: "#374E88",
  headBackUrl: "/img/news-header-bg.jpg",
  headLogo: "/img/miz-khabar-logo.svg",
  footer1: "#F2F3F6",
  footer2: "#DCDFE9",
  blue2: "#DFF1FF",
  green1: "#89D3A4",
  aside1: "#fff",
  border1: "#eaeaea",
  border2: "#e5e5e5",
  border3: "#eaeaea",
  border4: "#eaeaea",
  border5: "#89B3D3",
  border6: "#EDEDED",
  btn: "#DFF1FF",
};

export const darkColors: colorInterface = {
  theme: "dark",
  topHead: "#313131",
  main: "#181818",
  main2: "#212121",
  main3: "#313131",
  main4: "#212121",
  main5: "#212121",
  font1: "#FFFFFF",
  font2: "#5D5D5D",
  font3: "#ffffff",
  font4: "#5D5D5D",
  font5: "#ffffff",
  font6: "#4b5b67",
  font7: "#a7a7a7",
  font8: "#FFFFFF",
  blue: "#374E88",
  headBackUrl: "/img/news-header-bg-dark.jpg",
  headLogo: "/img/miz-khabar-logo-dark.svg",
  footer1: "#212121",
  footer2: "#212121",
  blue2: "#181818",
  green1: "#374E88",
  aside1: "#212121",
  border1: "#313131",
  border2: "#313131",
  border3: "#212121",
  border4: "#545454",
  border5: "#545454",
  border6: "#434343",
  btn: "#374E88",
};

export const defaultColors = {
  danger: "#E2574C",
  danger2: "#CD382C",
  warn: "#FFC95C",
  warn2: "#E0A01E",
  success: "#89D3A4",
  success2: "#5AB17A",
  blue: "#3D78F9",
  blue2: "#131520",
  blue3: "rgba(21, 36, 74, 0.798978)",
  blue4: "#374E88",
  blue5: "#9BA1AE",
  grey: "#d8d8d8",
};

export const colors = (state = darkColors, action: AnyAction) => {
  switch (action.type) {
    case CHANGE_THEME:
      return { ...state, ...action.payload };

    default:
      return state;
  }
};
