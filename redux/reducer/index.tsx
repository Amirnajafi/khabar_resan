import { combineReducers } from "redux";

import { colors, colorInterface } from "./colors/colorReducer";
import { activeProfile } from "./other/profileReducer";
import { home, news, newsDetails, sources } from "./news";
import { auth } from "./auth";
import { user, categories } from "./users";
import { packages, payments } from "./packages";

export interface State {
  colors: colorInterface;
  activeProfile: number;
}

export default combineReducers({
  colors,
  activeProfile,
  home,
  news,
  newsDetails,
  auth,
  sources,
  user,
  categories,
  packages,
  payments,
});
