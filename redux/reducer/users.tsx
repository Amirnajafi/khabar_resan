import { AnyAction } from "redux";
import { FETCH_PROFILE, FETCH_CATEGORIES } from "../types";

const initialAuth = {
  item: {
    filters: [],
    name: "",
    email: "",
    mobile: "",
    customFilterNumber: 0,
  },
  loading: false,
};

export const user = (state = initialAuth, action: AnyAction) => {
  switch (action.type) {
    case FETCH_PROFILE:
      return {
        ...state,
        item: { ...action.item, filters: action.item.filters || [] },
        loading: action.loading || false,
      };

    default:
      return state;
  }
};

const initialCategories = {
  list: [],
  loading: false,
};

export const categories = (state = initialCategories, action: AnyAction) => {
  switch (action.type) {
    case FETCH_CATEGORIES:
      return {
        ...state,
        list: action.list || [],
        loading: action.loading || false,
      };

    default:
      return state;
  }
};
