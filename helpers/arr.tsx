export default [
  {
    mongo_id: "5ebfd3b9906f740a22388f79",
    source_name: "خبر فوری",
    title: "درخواست‌ها برای پس گرفتن رای اعتماد به رئیس پارلمان تونس",
    source_id: "5ea580eb7631f639c489ad0c",
    create_date: "2020-05-16T11:51:21.366000",
    image: "",
    id: "D1BbHXIB_nmBVDIwHjyK",
    summary:
      '\n                                                                                    فراکسیون حزب "قانون اساسی آزاد" تونس لغو رای اعتماد داده شده به رئیس پارلمان این کشور را خواستار شده و این شخص را تهدیدی برای امنیت ملی این کشور توصیف کرد.\n                                                                            ',
  },
  {
    mongo_id: "5ebfd3b8906f740a22388f75",
    source_name: "خبر فوری",
    title: "شغل جدید کی‌روش در کلمبیا",
    source_id: "5ea580eb7631f639c489ad0c",
    create_date: "2020-05-16T11:51:20.305000",
    image: "",
    id: "ClBbHXIB_nmBVDIwHTz3",
    summary:
      "\n                                                                                    سرمربی تیم ملی فوتبال کلمبیا به تازگی شغل جدیدی را برای خود دست و پا کرده است.\n                                                                            ",
  },
  {
    mongo_id: "5ebfd3b7906f740a22388f71",
    source_name: "خبر فوری",
    title: "جشنواره بزرگ آب در قزوین برگزار می شود",
    source_id: "5ea580eb7631f639c489ad0c",
    create_date: "2020-05-16T11:51:19.198000",
    image: "",
    id: "FFBbHXIB_nmBVDIwHzwo",
    summary:
      "\n                                                                                    مدیر کل حفاظت محیط زیست استان قزوین از برگزاری جشنواره بزرگ  آب، ایده‌ها، خلاقیت و محصولات محیط زیستی در استان قزوین خبر داد.\n                                                                            ",
  },
  {
    mongo_id: "5ebfd3b6906f740a22388f6b",
    source_name: "خبر فوری",
    title: "کاسیاس: در فوتبال دشوار است که ۲ متر فاصله حفظ شود",
    source_id: "5ea580eb7631f639c489ad0c",
    create_date: "2020-05-16T11:51:18.158000",
    image: "",
    id: "FVBbHXIB_nmBVDIwHzww",
    summary:
      "\n                                                                                    دروازه‌بان اسپانیایی معتقد است در فوتبال سخت می‌توان دو متر فاصله را حفظ کرد.\n                                                                            ",
  },
  {
    mongo_id: "5ebfd3b5906f740a22388f65",
    source_name: "خبر فوری",
    title: "اعتراض جامعه کارگری به عدم اجرای قانون است/حق مسکن موضوع فرعی است",
    source_id: "5ea580eb7631f639c489ad0c",
    create_date: "2020-05-16T11:51:17.015000",
    image: "",
    id: "ElBbHXIB_nmBVDIwHzwi",
    summary:
      "\n                                                                                    رئیس کانون عالی انجمن‌های صنفی کارگران با بیان اینکه حق مسکن موضوع فرعی است،گفت: اعتراض جامعه کارگری به عدم اجرای قانون است و در صورت عدم توجه به این موضوع، ما شکایت خود را دنبال خواهیم کرد.\n                                                                            ",
  },
  {
    mongo_id: "5ebfd3b3906f740a22388f60",
    source_name: "خبر فوری",
    title:
      "شیوه برگزاری نماز عید فطر و راهپیمایی روز قدس در کهگیلویه و بویراحمد اعلام شد",
    source_id: "5ea580eb7631f639c489ad0c",
    create_date: "2020-05-16T11:51:15.837000",
    image: "",
    id: "C1BbHXIB_nmBVDIwHjwf",
    summary:
      "\n                                                                                    استاندار کهگیلویه و بویراحمد دستورالعمل چگونگی برگزاری نماز عید فطر و روز قدس را در  استان صادر کرد.\n                                                                            ",
  },
  {
    mongo_id: "5ebfd3a7906f740a22388f38",
    source_name: "رکنا",
    title:
      "\n                   خودسوزی در آشیانه بوئینگ 747  پایگاه یکم شکاری نیروی هوایی                 ",
    source_id: "5ea55f127631f639c489aca2",
    create_date: "2020-05-16T11:51:03.636000",
    image:
      "https://static2.rokna.net/thumbnail/s3WffV5v64F6/lcpxeITW3yJ7OqQILJa6xq7g_OafmJ6PG_8oTuzN5Kmx41I0COQPXDnjCruaSj3zd_Pd4Xw3RxA,/%D8%AE%D9%88%D8%AF%D8%B3%D9%88%D8%B2%DB%8C+%D8%AF%D8%B1+%D9%BE%D8%A7%DB%8C%DA%AF%D8%A7%D9%87+%DB%8C%DA%A9%D9%85+%D9%86%DB%8C%D8%B1%D9%88%DB%8C+%D9%87%D9%88%D8%A7%DB%8C%DB%8C+%D8%A7%D8%B1%D8%AA%D8%B4.jpg",
    id: "HlBbHXIB_nmBVDIwITwG",
    summary: "",
  },
  {
    mongo_id: "5ebfd3a7906f740a22388f37",
    source_name: "خبر گزاری برنا",
    title:
      "\n                  وزارت بهداشت: طرح ترافیک تا زمان کنترل کرونا اجرا نشود                ",
    source_id: "5ea4b6e67631f639c489ab37",
    create_date: "2020-05-16T11:51:03.615000",
    image:
      "https://static2.borna.news/thumbnail/rIbEtrV08gyF/zKlnR2CgDMsLPypExSqkWTsxfpMrgrriPtLy05fBFkcCZYBL8OGb1tkXRmfG0ZrOzG4dCpLzLSmmrKvCe4wUTqU3CPDNGwuv/%D8%B7%D8%B1%D8%AD+%D8%AA%D8%B1%D8%A7%D9%81%DB%8C%DA%A9.jpg",
    id: "GFBbHXIB_nmBVDIwHzxl",
    summary:
      "\n                  رییس گروه سلامت هوا و تغییر اقلیم مرکز سلامت محیط و کار وزارت بهداشت، درمان و آموزش پزشکی گفت: لغو طرح ترافیک در تهران…                ",
  },
  {
    mongo_id: "5ebfd3a5906f740a22388f31",
    source_name: "خبر گزاری برنا",
    title:
      "\n                  لرستان کاملاً در وضعیت هشدار قراردارد /روند ابتلا به کرونا درلرستان نوسانی است / هموطنان به لرستان سفر نکنند                 ",
    source_id: "5ea4b6e67631f639c489ab37",
    create_date: "2020-05-16T11:51:01.885000",
    image:
      "https://static2.borna.news/thumbnail/KOGgip6wTC5Y/zKlnR2CgDMsLPypExSqkWTsxfpMrgrriPtLy05fBFkcCZYBL8OGb1tkXRmfG0ZrOzG4dCpLzLSmmrKvCe4wUTiXBbNHztE2X/IMG_20200516_160507_424.jpg",
    id: "EFBbHXIB_nmBVDIwHjy5",
    summary:
      "\n                  سیدموسی خادمی گفت :طبق استاندارد های وزارت بهداشت اکثر شهرهای استان در وضعیت قرمز و زرد هستند و وضعیت سفید نداریم .                ",
  },
  {
    mongo_id: "5ebfd3a4906f740a22388f2d",
    source_name: "خبر گزاری برنا",
    title:
      "\n                  تاجگذاری لاروخا در اروپا با گلزنی ال‌نینو/ ببینید                ",
    source_id: "5ea4b6e67631f639c489ab37",
    create_date: "2020-05-16T11:51:00.755000",
    image:
      "https://static1.borna.news/thumbnail/W2cP5EpxCaN5/zKlnR2CgDMsLPypExSqkWTsxfpMrgrriPtLy05fBFkcCZYBL8OGb1tkXRmfG0ZrOzG4dCpLzLSmmrKvCe4wUTsYMNIiGpmgg/70..jpg",
    id: "GVBbHXIB_nmBVDIwHzxm",
    summary:
      "\n                  تیم ملی فوتبال اسپانیا با گلزنی تورس قهرمان جام ملت‌های اروپا شد.                ",
  },
  {
    mongo_id: "5ebfd3a3906f740a22388f2a",
    source_name: "خبر گزاری برنا",
    title:
      "\n                  هشدار؛ مراقب تست‌های تبلیغاتی کرونا باشید+ فیلم                ",
    source_id: "5ea4b6e67631f639c489ab37",
    create_date: "2020-05-16T11:50:59.623000",
    image:
      "https://static2.borna.news/thumbnail/N3BiEfQLMnhM/zKlnR2CgDMsLPypExSqkWTsxfpMrgrriPtLy05fBFkcCZYBL8OGb1tkXRmfG0ZrOzG4dCpLzLSmmrKvCe4wUTlHJM_nFm7KE/%D9%85%DB%8C%D9%86%D9%88+%D9%85%D8%AD%D8%B1%D8%B2.jpg",
    id: "DlBbHXIB_nmBVDIwHjxt",
    summary:
      "\n                  محرز گفت: انجام تست‌ها و آزمایشات بدون داشتن علایم بیماری کرونا را توصیه نمی‌کنم، این تست ها جنبه تبلیغاتی دارند.                ",
  },
  {
    mongo_id: "5ebfd3a1906f740a22388f26",
    source_name: "خبرگزاری میزان آنلاین",
    title: "اشک‌‌های «سردار سلیمانی» هنگام زیارت در حرم مطهر رضوی",
    source_id: "5ea4983a7631f639c489aafc",
    create_date: "2020-05-16T11:50:57.149000",
    image: "/files/fa/news/1399/2/27/2591277_798.jpg",
    id: "CVBbHXIB_nmBVDIwHTy1",
    summary:
      "تصاویری از زیارت و اشک ریختن شهید سپهبد حاج قاسم سلیمانی در حرم مطهر رضوی را در ادامه مشاهده می‌کنید.",
  },
  {
    mongo_id: "5ebfd3a0906f740a22388f25",
    source_name: "خبرگزاری ایلنا",
    title:
      "\n                                درمان رایگان اتباع خارجی مبتلا به کرونا در ایران                            ",
    source_id: "5ea489a87631f639c489aab2",
    create_date: "2020-05-16T11:50:56.226000",
    image:
      "https://static3.ilna.news/thumbnail/DCX4zoDz9HWX/YdHYY4gWkVxziNocTLLHoYkxM8c2r25UrkxXmcKklv15Hc_1VoAc02R5PeisCjWL9ULcwlXt6ht7JMG7f2xXElgNtaCd5wQzwWcx25nfUEjSpDXtPsNSLA,,/%D9%85%D9%87%D8%AF%DB%8C+%D9%85%D8%AD%D9%85%D9%88%D8%AF%DB%8C.jpg",
    id: "HVBbHXIB_nmBVDIwIDzV",
    summary:
      "\n                                ایلنا: مدیرکل امور اتباع و مهاجرین وزارت کشور و دبیر کمیسیون ساماندهی اتباع خارجی با تشریح اقدامات و خدمات کشورمان به اتباع افغانستانی در زمینه مهار و کنترل شیوع بیماری کرونا تاکید کرد: سیاست جمهوری اسلامی ایران، پیشگیری از اعمال هر گونه تبعیض در ارایه خدمات و مراقبتهای بهداشتی بین اتباع ایرانی و خارجی مقیم ایران است و در همین راستا، همه شهروندان خارجی مبتلا به کرونا به صورت رایگان در بیمارستانهای ایران درمان می شوند.                            ",
  },
  {
    mongo_id: "5ebfd3a0906f740a22388f24",
    source_name: "خبرگزاری میزان آنلاین",
    title: "آغاز تمرینات تیم‌های لیگ‌برتری از اواخر هفته",
    source_id: "5ea4983a7631f639c489aafc",
    create_date: "2020-05-16T11:50:56.225000",
    image: "/files/fa/news/1399/2/27/2591283_528.gif",
    id: "F1BbHXIB_nmBVDIwHzxd",
    summary:
      "به نظر می‌رسد با شروع مسابقات لیگ‌برتر در ۲۰ خرداد، تیم‌ها تمرینات خود را از اواخر همین هفته آغاز کنند.",
  },
  {
    mongo_id: "5ebfd39f906f740a22388f21",
    source_name: "خبرگزاری ایلنا",
    title:
      "\n                                بهره مندی بیش از 83 هزار نفر از خدمات توانبخشی جمعیت هلال احمر زنجان                             ",
    source_id: "5ea489a87631f639c489aab2",
    create_date: "2020-05-16T11:50:55.010000",
    image:
      "https://static1.ilna.news/thumbnail/kRyW5m5XVkvX/YdHYY4gWkVxziNocTLLHoYkxM8c2r25UrkxXmcKklv15Hc_1VoAc02R5PeisCjWL9ULcwlXt6ht7JMG7f2xXElgNtaCd5wQzZ3jBe4kslq_SpDXtPsNSLA,,/%D8%AA%D8%B5%D8%A7%D8%AF%D9%81+%D9%87%D9%84%D8%A7%D9%84+%D8%A7%D8%AD%D9%85%D8%B1.jpg",
    id: "DFBbHXIB_nmBVDIwHjww",
    summary:
      "\n                                ایلنا: مدیر عامل جمعیت هلال احمر استان زنجان گفت: سال گذشته 83 هزار و 90 نفر از خدمات توانبخشی هلال احمر این استان در بخش‌های مختلف بهره مند شده اند.                            ",
  },
  {
    mongo_id: "5ebfd39e906f740a22388f1f",
    source_name: "خبرگزاری میزان آنلاین",
    title:
      "کلاف سردرگم تشکیل کابینه رژیم صهیونیستی؛ اختلافات در لیکود بالا گرفت",
    source_id: "5ea4983a7631f639c489aafc",
    create_date: "2020-05-16T11:50:54.799000",
    image: "/files/fa/news/1399/2/27/2591292_744.jpg",
    id: "GlBbHXIB_nmBVDIwHzxx",
    summary:
      "پس از ماه‌ ها بن بست سیاسی در اراضی اشغالی، در نهایت بنیامین نتانیاهو و بنی گانتز بر سر تشکیل کابینه ائتلافی به توافق رسیدند، اما به نظر می‌رسد که اختلافات داخلی در حزب لیکود همچنان ادامه دارد.",
  },
  {
    mongo_id: "5ebfd39d906f740a22388f1e",
    source_name: "خبرگزاری میزان آنلاین",
    title: "مهمترین اعمال شب بیست و سوم ماه مبارک رمضان",
    source_id: "5ea4983a7631f639c489aafc",
    create_date: "2020-05-16T11:50:53.601000",
    image: "/files/fa/news/1399/2/27/2591287_896.jpg",
    id: "DVBbHXIB_nmBVDIwHjw8",
    summary:
      "یک کارشناس علوم قرآنی گفت:  غسل و دو رکعت نماز که در هر دو رکعت یک مرتبه سوره حمد و هفت مرتبه سوره توحید خوانده و بعد از نماز هفتاد بار ذکر «استغفرالله و اتوب الیه» گفته شود بیشار در شب بیست و سوم ماه مبارک رمضان سفارش شده است.",
  },
  {
    mongo_id: "5ebfd39c906f740a22388f1b",
    source_name: "خبرگزاری میزان آنلاین",
    title: "پرداخت بخشی از مطالبات اعضای تیم امید استقلال",
    source_id: "5ea4983a7631f639c489aafc",
    create_date: "2020-05-16T11:50:52.372000",
    image: "/files/fa/news/1399/2/27/2591296_608.jpg",
    id: "CFBbHXIB_nmBVDIwHTyj",
    summary:
      "باشگاه استقلال در ادامه پرداختی‌های خود به اعضای تیم، بخشی از مطالبات بازیکنان تیم امید خود را پرداخت کرد.",
  },
  {
    mongo_id: "5ebfd39a906f740a22388f18",
    source_name: "خبرگزاری ایلنا",
    title:
      "\n                                آغاز عملیات نظامی ارتش عراق در شرق این کشور                            ",
    source_id: "5ea489a87631f639c489aab2",
    create_date: "2020-05-16T11:50:50.886000",
    image:
      "https://static1.ilna.news/thumbnail/Ea92vQ9Xgcqp/YdHYY4gWkVxziNocTLLHoYkxM8c2r25UrkxXmcKklv15Hc_1VoAc02R5PeisCjWL9ULcwlXt6ht7JMG7f2xXElgNtaCd5wQzacht1Z7oaODSpDXtPsNSLA,,/5ebfb5764c59b711082ce78b+%281%29.jpg",
    id: "EVBbHXIB_nmBVDIwHjzQ",
    summary:
      "\n                                ایلنا: ارتش عراق و ریاست پلیس استان دیاله در شرق این کشور عملیات نظامی برای تعقیب بقایای تروریستی داعش در جنوب شهر بهروز را آغاز کردند.                            ",
  },
  {
    mongo_id: "5ebfd399906f740a22388f15",
    source_name: "خبرگزاری ایلنا",
    title:
      "\n                                مسیر شهدا در دفاع از عزت کشور ادامه خواهد یافت                            ",
    source_id: "5ea489a87631f639c489aab2",
    create_date: "2020-05-16T11:50:49.964000",
    image:
      "https://static2.ilna.news/thumbnail/PqLlTS1vd0SH/YdHYY4gWkVxziNocTLLHoYkxM8c2r25UrkxXmcKklv15Hc_1VoAc02R5PeisCjWL9ULcwlXt6ht7JMG7f2xXElgNtaCd5wQz0fs16wu_BwvSpDXtPsNSLA,,/%D8%B3%DB%8C%D8%AF+%D9%82%D8%A7%D8%B3%D9%85+%D8%AE%D8%A7%D9%85%D9%88%D8%B4%DB%8C.jpg",
    id: "FlBbHXIB_nmBVDIwHzwz",
    summary:
      "\n                                ایلنا: فرمانده پایگاه چهارم هوانیروز ارتش جمهوری اسلامی گفت: مسیر شهدا در دفاع از عزت و اقتدار جمهوری اسلامی ایران با تمام قدرت ادامه خواهد یافت.                            ",
  },
  {
    mongo_id: "5ebfd398906f740a22388f12",
    source_name: "خبرگزاری ایلنا",
    title:
      "\n                                افزایش ۹ هزار و ۲۰۰ نفره مبتلایان به کرونا در روسیه                            ",
    source_id: "5ea489a87631f639c489aab2",
    create_date: "2020-05-16T11:50:48.874000",
    image:
      "https://static3.ilna.news/thumbnail/AteBZrQXcGLa/YdHYY4gWkVxziNocTLLHoYkxM8c2r25UrkxXmcKklv15Hc_1VoAc02R5PeisCjWL9ULcwlXt6ht7JMG7f2xXElgNtaCd5wQzyAu3oYmOsW7SpDXtPsNSLA,,/coronavirus.jpg",
    id: "E1BbHXIB_nmBVDIwHzwl",
    summary:
      "\n                                ایلنا: شمار قربانیان ویروس کرونا در روسیه با ۱۱۹ نفر افزایش یافت.                            ",
  },
  {
    mongo_id: "5ebfd38f906f740a22388eff",
    source_name: "پایگاه خبری تحلیلی پارسینه",
    title:
      "\n                کرونا فقط در زمین فوتبال است و نه در دورهمی و کوه‌نوردی؟\n            ",
    source_id: "5e9f21297631f639c489a826",
    create_date: "2020-05-16T11:50:39.186000",
    image: "/files/fa/news/1399/2/27/921499_526.jpg",
    id: "G1BbHXIB_nmBVDIwHzyO",
    summary:
      "\n            این روزها بار دیگر بحث تعطیلی بدون نتیجه در فوتبال ایران مطرح شده است.\n        ",
  },
  {
    mongo_id: "5ebfd384906f740a22388ee6",
    source_name: "خبرگزاری مهر",
    title: "وعده دولت برای تحویل ۵۰۰ اتوبوس به ناوگان شهری محقق نشد",
    source_id: "5e43e6502e431e4804e79dc2",
    create_date: "2020-05-16T11:50:28.116000",
    image: "https://media.mehrnews.com/d/2017/07/02/2/2501841.jpg",
    id: "HFBbHXIB_nmBVDIwIDxm",
    summary:
      "‌رییس مرکز ارتباطات شهرداری تهران با اشاره به آماده نبودن ۵۰۰ دستگاه اتوبوس برای افزوده شدن به ناوگان حمل و نقل شهری گفت: تحویل این اتوبوس‌ها تا پایان اردیبهشت منتفی است. \n        ",
  },
  {
    mongo_id: "5ebfd37c906f740a22388edd",
    source_name: "خبر آنلاین",
    title: "چرا ولادیمیر پوتین، رباعیات خیام را دوست دارد؟",
    source_id: "5e9dc4e77631f639c489a1fd",
    create_date: "2020-05-16T11:50:20.486000",
    image: "https://media.khabaronline.ir/d/2020/05/16/1/5394032.jpg",
    id: "H1BbHXIB_nmBVDIwITwP",
    summary:
      "\n                    ادبیاتسیدحسین طباطبایی، در یادداشتی درباره تاثیر اشعار خیام در روسیه، به یک گفت‌وگوی ولادیمیر پوتین، رییس‌جمهور آن کشور اشاره کرده است. \n        ",
  },
  {
    mongo_id: "5ebfd379906f740a22388ed9",
    source_name: "باشگاه خبرنگاران جوان",
    title:
      "\n\t\t\t\t\t\t\tمجوز بازگشایی باشگاه‌های ورزشی انفرادی صادر شد\n\t\t\t\t\t\t",
    source_id: "5e43f3712e431e4804e79ff2",
    create_date: "2020-05-16T11:50:17.979000",
    image: "https://cdn.yjc.ir/files/fa/news/1399/2/27/11908403_716.jpg",
    id: "IFBbHXIB_nmBVDIwJDz2",
    summary:
      "\n\t\t\t\t\tوزیر بهداشت در نامه ای به وزیر ورزش و جوانان مجوز فعالیت باشگاه‌های ورزشی انفرادی و غیرگروهی را صادر کرد.\n\t\t\t\t",
  },
  {
    mongo_id: "5ebfd379906f740a22388ed8",
    source_name: "ایرنا",
    title: "شوک بورس به سهامداران",
    source_id: "5dff77c86edd2c10d1c4f833",
    create_date: "2020-05-16T11:50:17.535000",
    image: "https://img9.irna.ir/d/r1/2020/05/09/1/157115276.jpg",
    id: "IlBbHXIB_nmBVDIwKDxO",
    summary:
      "\n                    اقتصاد > بورستهران – ایرنا - شاخص کل در بازار بورس امروز (شنبه) با ۳۰ هزار و ۱۸۶ واحد افت از کانال یک میلیون واحد عقب‌نشینی کرد و در نهایت این شاخص به رقم ۹۸۷ هزار و ۴۷۵ واحد رسید.\n        ",
  },
  {
    mongo_id: "5ebfd377906f740a22388ed5",
    source_name: "ایرنا",
    title: "دستگاه سی تی اسکن بیمارستان صحنه به بهره‌برداری رسید",
    source_id: "5dff77c86edd2c10d1c4f833",
    create_date: "2020-05-16T11:50:15.960000",
    image: "https://img9.irna.ir/d/r2/2020/05/16/1/157127879.jpg",
    id: "IVBbHXIB_nmBVDIwJzw0",
    summary:
      "\n                    استان‌ها > کرمانشاهکرمانشاه- ایرنا- دستگاه سی تی اسکن ۱۶ اسلایس بیمارستان صحنه روز شنبه با حضور معاون سیاسی، امنیتی و اجتماعی استاندار و رییس دانشگاه علوم پزشکی کرمانشاه به بهره‌برداری رسید.\n        ",
  },
  {
    mongo_id: "5ebfd377906f740a22388ed4",
    source_name: "خبرگزاری مهر",
    title:
      "جزئیات زمان و نحوه برگزاری امتحانات دانشگاه آزاد/ امتحانات حضوری شد",
    source_id: "5e43e6502e431e4804e79dc2",
    create_date: "2020-05-16T11:50:15.716000",
    image: "https://media.mehrnews.com/d/2020/02/09/2/3376319.jpg",
    id: "I1BbHXIB_nmBVDIwKTyY",
    summary:
      "معاون دانشگاه آزاد با اشاره به اینکه امتحانات از ۲۴ خرداد تا ۱۳ تیرماه برگزار می شود، تاکید کرد که امتحانات حضوری است.\n        ",
  },
  {
    mongo_id: "5ebfd377906f740a22388ed2",
    source_name: "خبر آنلاین",
    title: "واکنش مدیرعامل بورس تهران به کاهش ۳۰ هزار واحدی شاخص بورس",
    source_id: "5e9dc4e77631f639c489a1fd",
    create_date: "2020-05-16T11:50:15.048000",
    image: "https://media.khabaronline.ir/d/2020/05/16/1/5394030.jpg",
    id: "J1BbHXIB_nmBVDIwLzxM",
    summary:
      "\n                    بازار مالیایسنا نوشت: بورس تهران که در سال جدید روزهای طلایی را پشت سر گذاشت و شاخص کل این بازار از یک میلیون واحد هم فراتر رفت، حدود پنج روز است که رویه دیگری در پیش گرفته و شاهد روشن شدن چراغ قرمز این بازار هستیم. اتفاقی که مدیرعامل شرکت بورس تهران ضمن تشریح عوامل آن، اعلام کرد که کاهش شاخص بورس باعث خروج منابع از بازار سرمایه نشده است.\n        ",
  },
  {
    mongo_id: "5ebfd376906f740a22388ed1",
    source_name: "ایسنا",
    title: 'حمایت آمریکا از رئیس "بانک لبنان"',
    source_id: "5dff79136edd2c10d1c4f842",
    create_date: "2020-05-16T11:50:14.925000",
    image: "https://cdn.isna.ir/d/2019/12/25/2/61521663.jpg",
    id: "JVBbHXIB_nmBVDIwLjwB",
    summary:
      'روزنامه الاخبار چاپ لبنان از تهدید جدی آمریکا علیه لبنان با حمایت از رئیس "بانک لبنان" خبر داد.',
  },
];

export const asideArr = [
  {
    mongo_id: "5ebfd379906f740a22388ed8",
    source_name: "ایرنا",
    title: "شوک بورس به سهامداران",
    source_id: "5dff77c86edd2c10d1c4f833",
    create_date: "2020-05-16T11:50:17.535000",
    image: "https://img9.irna.ir/d/r1/2020/05/09/1/157115276.jpg",
    id: "IlBbHXIB_nmBVDIwKDxO",
    summary:
      "\n                    اقتصاد > بورستهران – ایرنا - شاخص کل در بازار بورس امروز (شنبه) با ۳۰ هزار و ۱۸۶ واحد افت از کانال یک میلیون واحد عقب‌نشینی کرد و در نهایت این شاخص به رقم ۹۸۷ هزار و ۴۷۵ واحد رسید.\n        ",
  },
  {
    mongo_id: "5ebfd377906f740a22388ed5",
    source_name: "ایرنا",
    title: "دستگاه سی تی اسکن بیمارستان صحنه به بهره‌برداری رسید",
    source_id: "5dff77c86edd2c10d1c4f833",
    create_date: "2020-05-16T11:50:15.960000",
    image: "https://img9.irna.ir/d/r2/2020/05/16/1/157127879.jpg",
    id: "IVBbHXIB_nmBVDIwJzw0",
    summary:
      "\n                    استان‌ها > کرمانشاهکرمانشاه- ایرنا- دستگاه سی تی اسکن ۱۶ اسلایس بیمارستان صحنه روز شنبه با حضور معاون سیاسی، امنیتی و اجتماعی استاندار و رییس دانشگاه علوم پزشکی کرمانشاه به بهره‌برداری رسید.\n        ",
  },
  {
    mongo_id: "5ebfd377906f740a22388ed4",
    source_name: "خبرگزاری مهر",
    title:
      "جزئیات زمان و نحوه برگزاری امتحانات دانشگاه آزاد/ امتحانات حضوری شد",
    source_id: "5e43e6502e431e4804e79dc2",
    create_date: "2020-05-16T11:50:15.716000",
    image: "https://media.mehrnews.com/d/2020/02/09/2/3376319.jpg",
    id: "I1BbHXIB_nmBVDIwKTyY",
    summary:
      "معاون دانشگاه آزاد با اشاره به اینکه امتحانات از ۲۴ خرداد تا ۱۳ تیرماه برگزار می شود، تاکید کرد که امتحانات حضوری است.\n        ",
  },
  {
    mongo_id: "5ebfd377906f740a22388ed2",
    source_name: "خبر آنلاین",
    title: "واکنش مدیرعامل بورس تهران به کاهش ۳۰ هزار واحدی شاخص بورس",
    source_id: "5e9dc4e77631f639c489a1fd",
    create_date: "2020-05-16T11:50:15.048000",
    image: "https://media.khabaronline.ir/d/2020/05/16/1/5394030.jpg",
    id: "J1BbHXIB_nmBVDIwLzxM",
    summary:
      "\n                    بازار مالیایسنا نوشت: بورس تهران که در سال جدید روزهای طلایی را پشت سر گذاشت و شاخص کل این بازار از یک میلیون واحد هم فراتر رفت، حدود پنج روز است که رویه دیگری در پیش گرفته و شاهد روشن شدن چراغ قرمز این بازار هستیم. اتفاقی که مدیرعامل شرکت بورس تهران ضمن تشریح عوامل آن، اعلام کرد که کاهش شاخص بورس باعث خروج منابع از بازار سرمایه نشده است.\n        ",
  },
  {
    mongo_id: "5ebfd376906f740a22388ed1",
    source_name: "ایسنا",
    title: 'حمایت آمریکا از رئیس "بانک لبنان"',
    source_id: "5dff79136edd2c10d1c4f842",
    create_date: "2020-05-16T11:50:14.925000",
    image: "https://cdn.isna.ir/d/2019/12/25/2/61521663.jpg",
    id: "JVBbHXIB_nmBVDIwLjwB",
    summary:
      'روزنامه الاخبار چاپ لبنان از تهدید جدی آمریکا علیه لبنان با حمایت از رئیس "بانک لبنان" خبر داد.',
  },
  {
    mongo_id: "5ebfd3b3906f740a22388f60",
    source_name: "خبر فوری",
    title:
      "شیوه برگزاری نماز عید فطر و راهپیمایی روز قدس در کهگیلویه و بویراحمد اعلام شد",
    source_id: "5ea580eb7631f639c489ad0c",
    create_date: "2020-05-16T11:51:15.837000",
    image: "",
    id: "C1BbHXIB_nmBVDIwHjwf",
    summary:
      "\n                                                                                    استاندار کهگیلویه و بویراحمد دستورالعمل چگونگی برگزاری نماز عید فطر و روز قدس را در  استان صادر کرد.\n                                                                            ",
  },
  {
    mongo_id: "5ebfd3a7906f740a22388f38",
    source_name: "رکنا",
    title:
      "\n                   خودسوزی در آشیانه بوئینگ 747  پایگاه یکم شکاری نیروی هوایی                 ",
    source_id: "5ea55f127631f639c489aca2",
    create_date: "2020-05-16T11:51:03.636000",
    image:
      "https://static2.rokna.net/thumbnail/s3WffV5v64F6/lcpxeITW3yJ7OqQILJa6xq7g_OafmJ6PG_8oTuzN5Kmx41I0COQPXDnjCruaSj3zd_Pd4Xw3RxA,/%D8%AE%D9%88%D8%AF%D8%B3%D9%88%D8%B2%DB%8C+%D8%AF%D8%B1+%D9%BE%D8%A7%DB%8C%DA%AF%D8%A7%D9%87+%DB%8C%DA%A9%D9%85+%D9%86%DB%8C%D8%B1%D9%88%DB%8C+%D9%87%D9%88%D8%A7%DB%8C%DB%8C+%D8%A7%D8%B1%D8%AA%D8%B4.jpg",
    id: "HlBbHXIB_nmBVDIwITwG",
    summary: "",
  },
];

export const bottomPostArr = [
  {
    mongo_id: "5ebfd379906f740a22388ed8",
    source_name: "ایرنا",
    title: "شوک بورس به سهامداران",
    source_id: "5dff77c86edd2c10d1c4f833",
    create_date: "2020-05-16T11:50:17.535000",
    image: "https://img9.irna.ir/d/r1/2020/05/09/1/157115276.jpg",
    id: "IlBbHXIB_nmBVDIwKDxO",
    summary:
      "\n                    اقتصاد > بورستهران – ایرنا - شاخص کل در بازار بورس امروز (شنبه) با ۳۰ هزار و ۱۸۶ واحد افت از کانال یک میلیون واحد عقب‌نشینی کرد و در نهایت این شاخص به رقم ۹۸۷ هزار و ۴۷۵ واحد رسید.\n        ",
  },
  {
    mongo_id: "5ebfd377906f740a22388ed5",
    source_name: "ایرنا",
    title: "دستگاه سی تی اسکن بیمارستان صحنه به بهره‌برداری رسید",
    source_id: "5dff77c86edd2c10d1c4f833",
    create_date: "2020-05-16T11:50:15.960000",
    image: "https://img9.irna.ir/d/r2/2020/05/16/1/157127879.jpg",
    id: "IVBbHXIB_nmBVDIwJzw0",
    summary:
      "\n                    استان‌ها > کرمانشاهکرمانشاه- ایرنا- دستگاه سی تی اسکن ۱۶ اسلایس بیمارستان صحنه روز شنبه با حضور معاون سیاسی، امنیتی و اجتماعی استاندار و رییس دانشگاه علوم پزشکی کرمانشاه به بهره‌برداری رسید.\n        ",
  },
  {
    mongo_id: "5ebfd377906f740a22388ed4",
    source_name: "خبرگزاری مهر",
    title:
      "جزئیات زمان و نحوه برگزاری امتحانات دانشگاه آزاد/ امتحانات حضوری شد",
    source_id: "5e43e6502e431e4804e79dc2",
    create_date: "2020-05-16T11:50:15.716000",
    image: "https://media.mehrnews.com/d/2020/02/09/2/3376319.jpg",
    id: "I1BbHXIB_nmBVDIwKTyY",
    summary:
      "معاون دانشگاه آزاد با اشاره به اینکه امتحانات از ۲۴ خرداد تا ۱۳ تیرماه برگزار می شود، تاکید کرد که امتحانات حضوری است.\n        ",
  },
  {
    mongo_id: "5ebfd377906f740a22388ed2",
    source_name: "خبر آنلاین",
    title: "واکنش مدیرعامل بورس تهران به کاهش ۳۰ هزار واحدی شاخص بورس",
    source_id: "5e9dc4e77631f639c489a1fd",
    create_date: "2020-05-16T11:50:15.048000",
    image: "https://media.khabaronline.ir/d/2020/05/16/1/5394030.jpg",
    id: "J1BbHXIB_nmBVDIwLzxM",
    summary:
      "\n                    بازار مالیایسنا نوشت: بورس تهران که در سال جدید روزهای طلایی را پشت سر گذاشت و شاخص کل این بازار از یک میلیون واحد هم فراتر رفت، حدود پنج روز است که رویه دیگری در پیش گرفته و شاهد روشن شدن چراغ قرمز این بازار هستیم. اتفاقی که مدیرعامل شرکت بورس تهران ضمن تشریح عوامل آن، اعلام کرد که کاهش شاخص بورس باعث خروج منابع از بازار سرمایه نشده است.\n        ",
  },
];
