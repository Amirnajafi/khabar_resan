FROM node
RUN mkdir /app
WORKDIR /app
COPY package.json /app/
#RUN npm install -g yarn
COPY . /app/
RUN yarn install
#RUN yarn build
EXPOSE 3000
CMD yarn start
