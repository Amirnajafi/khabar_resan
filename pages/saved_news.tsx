import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import NewsList from "../components/NewsList";
import Layout from "../components/layout/Layout";
import { checkProtected } from "../helpers/props";
import { fetchNews } from "../redux/actions";
import Head from "next/head";

const SavedNews = () => {
  return (
    <>
      <Head>
        <title>میز خبر | اخبار ذخیره شده</title>
      </Head>
      <Layout>
        <Main>
          <div className="container">
            <div className="row">
              <section className="col-12">
                <NewsList type="saved_news" />
              </section>
            </div>
          </div>
        </Main>
      </Layout>
    </>
  );
};

const Main = styled.main`
  margin: 30px 0;
  min-height: 500px;
`;

SavedNews.getInitialProps = async (ctx) => {
  const { store } = ctx;

  await checkProtected(ctx, async () => {
    await store.dispatch(fetchNews(null, null, null, "save_news"));
  });

  return {};
};

export default connect((state) => state)(SavedNews);
