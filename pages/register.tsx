import React from "react";
import styled from "styled-components";
import Header from "../components/layout/Header";
import Footer from "../components/layout/Footer";
import Auth from "../components/auth/Auth";
import { connect } from "react-redux";
import { register } from "../redux/actions";
import * as Yup from "yup";
import Head from "next/head";
import Link from "next/link";

const registerSchema = Yup.object().shape({
  mobile: Yup.string()
    .min(9, "شماره موبایل کوتاه می‌باشد")
    .max(11, "شماره موبایل صحیح نمی‌باشد")
    .trim("شماره موبایل اجباری می‌باشد")
    .required("شماره موبایل اجباری می‌باشد"),
  password: Yup.string()
    .min(8, "حداقل ۸ کاراکتر وارد کنید")
    .max(40, "بیشتر از ۴۰ کاراکتر مجاز نیست")
    .required("رمز عبور خود را وارد کنید"),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref("password"), null], "رمزعبور مطابقت ندارد")
    .required("تکرار رمزعبور اجباری می‌باشد"),
});

const Register = (props) => {
  const { register } = props;
  const initialRegister = {
    mobile: "",
    password: "",
    confirmPassword: "",
  };

  const handleRegisterSubmit = async (values, actions) => {
    const { setSubmitting, setErrors } = actions;
    let data = {
      mobile: Number(values.mobile).toString(),
      password: values.password,
    };

    setSubmitting(true);
    await register(data, (err) => {
      if (err.response.status === 400 || err.response.status === 401) {
        setErrors({ mobile: "شماره موبایل تکراری می‌باشد" });
      }
    });
    setSubmitting(false);
  };

  const renderNav = () => {
    return (
      <>
        <li className="d-none d-md-inline">
          <Link href="/login">
            <span>ورود</span>
          </Link>
        </li>
        <li className="d-none d-md-inline">
          <Link href="/packages">
            <span>پلن‌های اشتراکی</span>
          </Link>
        </li>
      </>
    );
  };

  return (
    <>
      <Head>
        <title>ثبت نام</title>
      </Head>
      <Header
        darkOnly
        noRecommend
        noCategories
        noNavOptions
        noPodcast
        renderNav={renderNav}
      />
      <Main>
        <Auth
          handleSubmit={handleRegisterSubmit}
          initialValues={initialRegister}
          isRegister
          validationSchema={registerSchema}
        />
      </Main>
      <Footer noTop darkOnly />
    </>
  );
};

const Main = styled.main`
  @media only screen and (min-width: 1050px) {
    height: calc(100vh - 243px);
  }
  min-height: 600px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default connect(null, { register })(Register);
