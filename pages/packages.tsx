import React, { useEffect, Fragment, useState } from "react";
import styled from "styled-components";
import Header from "../components/layout/Header";
import Footer from "../components/layout/Footer";
import PackagesInfoBox from "../components/package_page/PackagesInfoBox";
import { darkColors } from "../redux/reducer/colors/colorReducer";
import { useDispatch, useSelector } from "react-redux";
import { flex } from "../public/styles/modules";
import Head from "next/head";
// import PackagesOfferForm from "../components/package_page/PackagesOfferForm";
import PackagesPrice from "../components/package_page/PackagesPrice";
import { fetchPackages, handlePayment } from "../redux/actions";
import { toPersian } from "../helpers/props";
import Router from "next/router";
import Link from "next/link";

const Packages = () => {
  const initialOffer = { offerCode: "" };
  const dispatch = useDispatch();
  const { packages, auth } = useSelector((state) => state);
  const [loading, setLoading] = useState(true);

  // const handleOfferValidate = (values) => {
  //   const errors: { offerCode?: string } = {};
  //   if (!values.offerCode.trim()) {
  //     errors.offerCode = "لطفاً شماره موبایل خود را وارد کنید";
  //   }

  //   return errors;
  // };

  // const handleOfferSubmit = (values, { setSubmitting }) => {
  //   setSubmitting(false);
  // };

  const renderNav = () => {
    return (
      <>
        {auth.role === "user" ? (
          <li className="d-none d-md-inline">
            <Link href="/profile">
              <a>ناحیه کاربری</a>
            </Link>
          </li>
        ) : (
          <>
            <li className="d-none d-md-inline">
              <Link href="/login">
                <a>ورود</a>
              </Link>
            </li>
            <li className="d-none d-md-inline">
              <Link href="/register">
                <a>ثبت نام</a>
              </Link>
            </li>
          </>
        )}
      </>
    );
  };

  const handleClick = async (item) => {
    if (auth.role !== "user") {
      Router.push("/login");
      return;
    }

    setLoading(true);
    await dispatch(handlePayment({ package_id: item.id }, (err) => {}));
    setLoading(false);
  };

  return (
    <>
      <Head>
        <style>
          {`
            body {
              background-color: #000;
            }
          `}
        </style>
        <title>میز خبر | تمدید اشتراک</title>
      </Head>
      <Header
        noRecommend
        noCategories
        noNavOptions
        noPodcast
        darkOnly
        renderNav={renderNav}
      />
      <PackagesBox>
        <div className="packagesBox">
          <header className="packagesMainHeader">
            <h1>برای استفاده از امکانات به طور کامل باید دارای اشتراک باشید</h1>
            <p>
              برای استفاده بهتر و مفیدتر از امکانات میز خبر نیاز است تا ثبت نام
              وبعد از آن اشتراک مورد نظر خود را خریداری کنید
            </p>
          </header>
          {(packages.list || []).map((item, index) => {
            const key = `itemPackages${index}${item.id}`;
            if (item.price === 0) {
              return (
                <Fragment key={key}>
                  <PackagesInfoBox
                    title="اشتراک ۱۰ روزه رایگان"
                    imgUrl="/img/packages/journalism.png"
                    type="success"
                    description="به مدت ۱۰ روز به صورت رایگان می‌توانید کلیه خدمات را استفاده کنید"
                    onClick={() => handleClick(item)}
                  />
                </Fragment>
              );
            }

            return (
              <Fragment key={key}>
                <PackagesInfoBox
                  title={`اشتراک ${toPersian(item.days)} روزه`}
                  imgUrl="/img/packages/paper.png"
                  description={`با پرداخت این پکیج لورم ایپسوم برای شما نوشته می‌شود`}
                  renderPrice={() => <PackagesPrice mainPrice={item.price} />}
                  onClick={() => handleClick(item)}
                  // renderPrice={() => (
                  //   <PackagesPrice mainPrice={20000} offerPrice={10000} />
                  // )}
                  // percent={50}
                />
              </Fragment>
            );
          })}
          {/* do not remove this component if you want to don't fuck up bitch!!!  */}
          {/* <h4 className="formOfferTitle">کدتخفیف/کارت‌هدیه/کدمیزکوین</h4>
          <PackagesOfferForm
            handleValidate={handleOfferValidate}
            handleSubmit={handleOfferSubmit}
            initialValues={initialOffer}
          /> */}
        </div>
      </PackagesBox>
      <Footer noTop darkOnly />
    </>
  );
};

const PackagesBox = styled.main`
  min-height: calc(100vh - 243px);
  .packagesBox {
    max-width: 640px;
    margin: 30px auto;
    padding: 30px;
    border-radius: 4px;
    background-color: ${darkColors.aside1};
    header {
      ${flex}
      justify-content: center;
      flex-direction: column;
      margin: 30px 0;
      h1,
      p {
        color: #fff;
      }
      h1 {
        font-size: 1.1rem;
        margin-bottom: 3px;
      }
      p {
        font-size: 0.75rem;
      }
    }
  }
  .formOfferTitle {
    margin-bottom: 15px;
    color: #fff;
    font-size: 0.85rem;
    font-weight: 300;
  }
`;

Packages.getInitialProps = async ({ store }) => {
  await store.dispatch(fetchPackages());
  return {};
};

export default Packages;
