import * as React from "react";
import App from "next/app";
import { ThemeProvider, createGlobalStyle } from "styled-components";
import * as Sentry from "@sentry/browser";
import Router from "next/router";
import NProgress from "nprogress";
import Alert from "react-s-alert";
import { checkAuth } from "../redux/actions";
import Head from "next/head";

// Redux...
import { Provider } from "react-redux";
import withRedux, { ReduxWrapperAppProps } from "next-redux-wrapper";
import { makeStore } from "../redux/store";
import { State } from "../redux/reducer";

// CSS...
import reset from "../public/styles/reset";
import grid from "../public/styles/grid";
// Css Fonts
import "../public/styles/IRANSans/Farsi_numerals/webFonts/css/style.css";
import "../public/styles/IRANSans/WebFonts/css/style.css";
// Css LineAwesome
import "../public/fonts/line/css/line-awesome.min.css";
// Css FontIcon
import "../public/fonts/fontello-e659baaf/css/fontello.css";
// Css carousel
import "react-multi-carousel/lib/styles.css";
// Css nprogress
import "nprogress/nprogress.css";
// s-alert
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/jelly.css";
import Meta from "../components/Meta";

const GlobalStyle = createGlobalStyle`
  ${grid}
  ${reset}
`;

Sentry.init({
  dsn:
    "https://e7ceae89f8074b0aa6987be2e4322077@o332778.ingest.sentry.io/5211417",
});

const theme = {
  primary: "green",
};

Router.events.on("routeChangeStart", () => {
  NProgress.start();
});

Router.events.on("routeChangeComplete", () => {
  NProgress.done();
});

Router.events.on("routeChangeError", () => {
  NProgress.done();
});

class MyApp extends App<ReduxWrapperAppProps<State>> {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    const { getState } = ctx.store;

    if (getState().auth.role !== "user") {
      await ctx.store.dispatch(checkAuth(ctx.req, ctx.res));
    }

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ...ctx });
    }

    return { pageProps: { ...pageProps } };
  }

  componentDidMount() {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles && jssStyles.parentNode)
      jssStyles.parentNode.removeChild(jssStyles);
  }

  componentDidCatch(error, errorInfo) {
    Sentry.withScope((scope) => {
      Object.keys(errorInfo).forEach((key) => {
        scope.setExtra(key, errorInfo[key]);
      });

      Sentry.captureException(error);
    });

    super.componentDidCatch(error, errorInfo);
  }

  render() {
    const { Component, pageProps, store } = this.props;
    return (
      <>
        <Head>
          <Meta />
        </Head>
        <Provider store={store}>
          <ThemeProvider theme={theme}>
            <Component {...pageProps} />
            <Alert stack={{ limit: 3 }} />
            <GlobalStyle />
          </ThemeProvider>
        </Provider>
      </>
    );
  }
}

export default withRedux(makeStore, { debug: false })(MyApp);
