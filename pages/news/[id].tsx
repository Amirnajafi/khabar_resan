import React from "react";
import styled from "styled-components";
import AsidePost from "../../components/AsidePost";
import BottomPost from "../../components/BottomPost";
import MainPost from "../../components/MainPost";
import Layout from "../../components/layout/Layout";
import { connect, useSelector } from "react-redux";
import { fetchNewsDetails } from "../../redux/actions";
import Head from "next/head";
import { useRouter } from "next/router";
import { TelegramShareButton, WhatsappShareButton } from "react-share";
import Link from "next/link";

const NewsDetails = (props) => {
  const { newsDetails, auth } = useSelector((state) => state);
  const { item } = newsDetails;
  const { colors } = props;
  const router = useRouter();
  const url = `mizkhabar.com${router.asPath}`;

  const handleCopy = () => {
    const el = document.createElement("textarea");
    el.value = url;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
  };

  const { title, summary, image } = item;

  return (
    <>
      <Head>
        <title>{`میزخبر | ${title}`}</title>
        <meta name="title" content={title} />
        <meta name="description" content={summary} />

        <meta property="og:type" content="website" />
        <meta property="og:url" content={"http://mizkhabar.com"} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={summary} />
        <meta property="og:image" content={image} />

        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content={"http://mizkhabar.com"} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={summary} />
        <meta property="twitter:image" content={image} />
      </Head>
      <Layout
        renderNav={() => {
          if (auth.role !== "user") {
            return (
              <>
                <li className="d-none d-sm-inline-block">
                  <Link href="/login">
                    <a style={{ marginLeft: 0 }}>
                      <span style={{ color: colors.font1 }}>ورود/ثبت‌نام</span>
                    </a>
                  </Link>
                </li>
                <li className="d-none d-sm-inline-block">
                  <Link href="/packages">
                    <a style={{ marginLeft: 0 }}>
                      <span style={{ color: colors.font1 }}>
                        پلن های اشتراک
                      </span>
                    </a>
                  </Link>
                </li>
              </>
            );
          }
        }}
      >
        <Main
          borderBox={colors.border3}
          bgBox={colors.aside1}
          icon={colors.font3}
        >
          <div className="container">
            <div className="row">
              <div className="col-12 col-lg-9">
                <div className="mainPostContainer">
                  <div className="mainPostContainer">
                    <MainPost />
                  </div>
                  <div className="postPageSotialMedia">
                    <ul className="postSotialMediaBox">
                      <li className="postSotialMediaItem">
                        <WhatsappShareButton url={url}>
                          <span className="lab la-whatsapp"></span>
                        </WhatsappShareButton>
                      </li>
                      <li className="postSotialMediaItem">
                        <TelegramShareButton url={url}>
                          <span className="lab la-telegram"></span>
                        </TelegramShareButton>
                      </li>
                      <li className="postSotialMediaItem" onClick={handleCopy}>
                        <span className="la la-copy"></span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-12 col-lg-3 d-none d-lg-block">
                <AsidePost />
              </div>
              <div className="col-12">
                <BottomPost />
              </div>
            </div>
          </div>
        </Main>
      </Layout>
    </>
  );
};

const Main = styled.main((props) => {
  const { borderBox, bgBox, icon } = props;

  return {
    margin: "30px 0",
    ".mainPostContainer": {
      display: "flex",
      "@media only screen and (max-width: 1200px)": {
        flexDirection: "column",
        ".postPageSotialMedia": {
          marginRight: "0 !important",
          display: "flex",
          justifyContent: "flex-end",
        },
        ".postSotialMediaBox": {
          display: "inline-flex",
          li: {
            marginBottom: "0 !important",
            marginLeft: "15px",
            "&:last-of-type": {
              marginLeft: 0,
            },
          },
        },
      },
      ".mainPostContainer": {
        flex: 1,
      },
      ".postPageSotialMedia": {
        marginRight: 15,
        ".postSotialMediaBox": {
          border: `1px solid ${borderBox}`,
          padding: 15,
          borderRadius: "25px",
          position: "sticky",
          top: 30,
          backgroundColor: bgBox,
          ".postSotialMediaItem": {
            marginBottom: 15,
            "&:last-of-type": {
              marginBottom: 0,
            },
            span: {
              display: "flex",
              fontSize: "1.2rem",
              color: icon,
              cursor: "pointer",
            },
          },
        },
      },
    },
  };
});

NewsDetails.getInitialProps = async (ctx) => {
  const { store, query } = ctx;
  const { id } = query;
  await store.dispatch(fetchNewsDetails(id));
};

export default connect((state) => state)(NewsDetails);
