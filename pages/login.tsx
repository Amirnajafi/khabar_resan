import React from "react";
import styled from "styled-components";
import Header from "../components/layout/Header";
import Footer from "../components/layout/Footer";
import Auth from "../components/auth/Auth";
import Alert from "react-s-alert";
import { login } from "../redux/actions";
import { connect } from "react-redux";
import * as Yup from "yup";
import Head from "next/head";
import Link from "next/link";

const loginSchema = Yup.object().shape({
  mobile: Yup.string()
    .min(9, "شماره موبایل کوتاه می‌باشد")
    .max(11, "شماره موبایل صحیح نمی‌باشد")
    .trim("شماره موبایل اجباری می‌باشد")
    .required("شماره موبایل اجباری می‌باشد"),
  password: Yup.string()
    .min(8, "حداقل ۸ کاراکتر وارد کنید")
    .max(40, "بیشتر از ۴۰ کاراکتر مجاز نیست")
    .required("رمز عبور خود را وارد کنید"),
});

const Login = (props) => {
  const { login } = props;
  const initialLogin = { mobile: "", password: "" };

  const handleLoginSubmit = async (values, actions) => {
    const { setSubmitting, setErrors } = actions;
    let data = {
      mobile: Number(values.mobile).toString(),
      password: values.password,
    };

    setSubmitting(true);
    await login(data, (err) => {
      if (err.response.status === 400) {
        setErrors({
          mobile: "شماره همراه یا رمزعبور اشتباه است",
          password: "شماره همراه یا رمزعبور اشتباه است",
        });
      } else if (err.response.status === 401) {
        setErrors({
          mobile: "شماره همراه در سیستم ثبت نشده است",
        });
      }
    });
    setSubmitting(false);
  };

  const renderNav = () => {
    return (
      <>
        <li className="d-none d-md-inline">
          <Link href="register">
            <span>ثبت نام</span>
          </Link>
        </li>
        <li className="d-none d-md-inline">
          <Link href="/packages">
            <span>پلن‌های اشتراکی</span>
          </Link>
        </li>
      </>
    );
  };

  return (
    <>
      <Head>
        <title>ورود</title>
      </Head>
      <Header
        noRecommend
        noCategories
        darkOnly
        noNavOptions
        noPodcast
        renderNav={renderNav}
      />
      <Main>
        <Auth
          validationSchema={loginSchema}
          handleSubmit={handleLoginSubmit}
          initialValues={initialLogin}
        />
      </Main>
      <Footer noTop darkOnly />
    </>
  );
};

const Main = styled.main`
  @media only screen and (min-width: 1050px) {
    height: calc(100vh - 243px);
  }
  min-height: 600px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default connect(null, { login })(Login);
