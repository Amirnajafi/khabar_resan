import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import ProfileInfo from "../components/user/ProfileInfo";
import ProfileTransactions from "../components/user/ProfileTransactions";
import Layout from "../components/layout/Layout";
import AsideProfile from "../components/user/AsideProfile";
import Subscribe from "../components/user/Subscribe";
import { checkProtected, daysRemaining } from "../helpers/props";
import Head from "next/head";
import MetaOg from "../components/MetaOg";

const Profile = (props) => {
  const { activeProfile, user } = props;
  const { premium_date } = user.item;
  const count = daysRemaining(premium_date);

  const renderMain = () => {
    switch (activeProfile) {
      case 1:
        return <ProfileInfo />;

      case 2:
        return <ProfileTransactions />;

      // case 3:
      //   return <ProfileDevices />;

      default:
        return <ProfileInfo />;
    }
  };

  const subType = () => {
    if (premium_date) {
      if (count < 0) {
        return "danger";
      } else if (count < 5) {
        return "warn";
      } else {
        return "success";
      }
    } else {
      return "danger";
    }
  };

  return (
    <>
      <Head>
        <title>میز خبر | ناحیه کاربری</title>
      </Head>
      <Layout>
        <ProfileBox>
          <div className="container">
            <div className="row">
              <div className="col-12 col-xl-3">
                <AsideProfile />
              </div>
              <div className="col-12 col-xl-9">
                <Subscribe type={subType()} />
                {renderMain()}
              </div>
            </div>
          </div>
        </ProfileBox>
      </Layout>
    </>
  );
};

const ProfileBox = styled.main`
  margin: 30px 0;
  min-height: calc(100vh - 446px);
`;

Profile.getInitialProps = async (ctx) => {
  await checkProtected(ctx, async () => {});
};

export default connect((state) => state)(Profile);
