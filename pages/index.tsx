import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import Landing from "../components/landing/Landing";
import {
  fetchHome,
  fetchNews,
  fetchSources,
  autoLogin,
  fetchProfile,
} from "../redux/actions";
import News from "../components/News";
import { checkPremiumDate } from "../helpers/props";
import Head from "next/head";
import Router from "next/router";

const Home = (props) => {
  const { auth, token } = props;
  const dispatch = useDispatch();

  useEffect(() => {
    if (token) {
      (async () => {
        await dispatch(autoLogin(token));
        await dispatch(fetchProfile());
        Router.push("/");
      })();
    }
  }, []);

  return (
    <>
      <Head>
        <title>
          میز خبر |{" "}
          {auth.role === "user" ? "اخبار اختصاصی" : "روزنامه اختصاصی شما"}
        </title>
      </Head>
      {auth.role === "user" ? <News /> : <Landing />}
    </>
  );
};

Home.getInitialProps = async (ctx) => {
  const { store, query } = ctx;
  const { category_id, token } = query;
  const { auth } = store.getState();

  if (auth.role !== "user") {
    await store.dispatch(fetchHome());
  } else {
    await checkPremiumDate(ctx, async () => {
      if (category_id) {
        await store.dispatch(fetchNews(false, false, category_id));
      } else {
        await store.dispatch(fetchNews());
      }
      await store.dispatch(fetchSources());
    });
  }

  return { token };
};

export default connect((state) => state, { fetchHome })(Home);
