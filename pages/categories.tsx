import React, { Fragment } from "react";
import styled from "styled-components";
import Layout from "../components/layout/Layout";
import { flex } from "../public/styles/modules";
import { fetchCategories } from "../redux/actions";
import { useSelector } from "react-redux";
import Link from "next/link";
import { checkProtected } from "../helpers/props";
import Head from "next/head";

const Categories = () => {
  const { categories } = useSelector(({ categories }) => ({ categories }));

  const renderImages = () => {
    return categories.list.map((item, index) => {
      return (
        <Fragment key={`${item.id}mainCategoryItem${index}`}>
          {item.name === "بورس" ? null : (
            <Link href={{ pathname: "/", query: { category_id: item.id } }}>
              <a
                className={`categoryItem ${
                  index === 0 ? "bigCategoryItem" : ""
                }`}
                style={{
                  backgroundImage: `url(${item.image})`,
                }}
              >
                <span>{item.name}</span>
              </a>
            </Link>
          )}
        </Fragment>
      );
    });
  };

  return (
    <>
      <Head>
        <title>میزخبر | دسته بندی ها</title>
      </Head>
      <Layout>
        <div className="clearfix"></div>
        <CategoriesBox>
          <div className="container">
            <div className="boxCategoriesContainer">
              <div className="boxCategories">{renderImages()}</div>
            </div>
          </div>
          <div className="clearfix"></div>
        </CategoriesBox>
      </Layout>
    </>
  );
};

const CategoriesBox = styled.main`
  margin: 30px 0;
  min-height: calc(100vh - 300px);
  .clearfix::after {
    content: "";
    clear: both;
    display: table;
  }
  .boxCategoriesContainer {
    ${flex}
    justify-content: center;
  }
  .boxCategories {
    @media only screen and (max-width: 539px) {
      max-width: 340px;
    }
    .bigCategoryItem {
      height: 277px !important;
      @media only screen and (max-width: 767px) {
        width: 325px !important;
      }
    }
    .categoryItem {
      ${flex}
      background-size: cover;
      background-position: center;
      justify-content: center;
      width: 262.5px;
      height: 131px;
      margin: 7.5px;
      float: right;
      border-radius: 4px;
      position: relative;
      @media only screen and (max-width: 1190px) {
        width: 295px;
      }
      @media only screen and (max-width: 991px) {
        width: 330px;
      }
      @media only screen and (max-width: 767px) {
        width: 155px;
      }
      span {
        ${flex}
        position: absolute;
        width: 95%;
        height: 30px;
        background-color: rgba(0, 0, 0, 0.75);
        color: #fff;
        font-weight: 500;
        font-size: 0.9rem;
        bottom: 7px;
        padding: 0 15px;
      }
    }
  }
`;

Categories.getInitialProps = async (ctx) => {
  const { store } = ctx;
  const { dispatch } = store;
  await checkProtected(ctx, async () => {
    await dispatch(fetchCategories());
  });
};

export default Categories;
