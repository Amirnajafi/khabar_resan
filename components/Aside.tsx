import React, {
  useState,
  useRef,
  useEffect,
  FormEvent,
  ChangeEvent,
} from "react";
import { connect, useDispatch } from "react-redux";
import ToggleButton from "./ToggleButton";
import styled from "styled-components";
import { mainColors } from "../redux/reducer/colors/colorReducer";
import DropDown from "./reuse/DropDown";
import useFilter from "../hooks/useFilter";
import { fetchNews } from "../redux/actions";

const Aside = (props) => {
  const [open, setOpen] = useState(false);
  const { colors, user, sources } = props;
  const [showFilter, setShowFilter] = useState(false);
  const [search, setSearch] = useState("");
  const filterBoxRef = useRef(null);
  const filterBtnRef = useRef(null);
  const {
    filterTxt,
    setFilterTxt,
    addNewFilter,
    deleteFilter,
    keywordTxt,
    handleKyewordTxtChange,
    addNewOption,
    activeFilter,
    setActiveFilter,
  } = useFilter(setShowFilter);

  const handleToggle = () => setOpen(!open);

  const dispatch = useDispatch();

  const filterExists = activeFilter("checkExists");

  const handleOut = (e) => {
    if (filterBoxRef.current && !e.target.contains(filterBoxRef.current))
      return;
    if (!e.target.contains(filterBtnRef.current)) return;
    setShowFilter(false);
  };

  useEffect(() => {
    window.addEventListener("click", handleOut);
    return () => window.removeEventListener("click", handleOut);
  }, []);

  useEffect(() => {
    setSearch("");
  }, [user.item.filters]);

  return (
    <DropDown
      title="فیلترها"
      open={open}
      handleToggle={handleToggle}
      btnClassName="d-xl-none"
      dpStyle={{
        "@media only screen and (min-width: 1200px)": {
          height: "auto !important",
        },
      }}
    >
      <Box
        className="NewsAsideBox"
        style={{
          border: `1px solid ${colors.border1}`,
          backgroundColor: colors.aside1,
        }}
      >
        <div className="newsFilterOption">
          <div
            className="newsBtnFilterOption"
            ref={filterBtnRef}
            onClick={() => setShowFilter(!showFilter)}
          >
            <span className="titleBtnFilterOption">
              فیلتر: {filterExists ? activeFilter().name : "بدون فیلتر"}
            </span>
            <span
              className={showFilter ? "la la-angle-up" : "la la-angle-down"}
            ></span>
          </div>
          {showFilter && (
            <div
              className="newsFilterOptionChildBox"
              style={{
                backgroundColor: colors.aside1,
                boxShadow: `0px 5px 7px 0px rgba(0, 0, 0, .${
                  colors.theme === "dark" ? "6" : "2"
                }0)`,
              }}
              ref={filterBoxRef}
            >
              <div
                className="newsAsideAddMore"
                style={{
                  backgroundColor: colors.main3,
                }}
              >
                <span
                  className="addKeywordTitle"
                  onClick={() => {
                    setActiveFilter(-1);
                    setShowFilter(false);
                  }}
                >
                  بدون فیلتر
                </span>
              </div>
              {user.item.filters.map((item, index) => {
                const key = `newsAsideFilterParentItem${item.name}${index}`;

                return (
                  <div
                    key={key}
                    className="newsAsideAddMore"
                    style={{
                      backgroundColor: colors.main3,
                    }}
                  >
                    <span
                      className="addKeywordTitle"
                      onClick={() => {
                        setActiveFilter(index);
                        setShowFilter(false);
                      }}
                    >
                      {item.name}
                    </span>
                    <span
                      onClick={() => deleteFilter(index)}
                      className="la la-trash"
                    ></span>
                  </div>
                );
              })}
              {user.item.filters && user.item.filters.length < 5 && (
                <div className="addNewFilterAsideNews">
                  <form
                    onSubmit={(e: FormEvent<HTMLFormElement>) => {
                      e.preventDefault();
                      addNewFilter();
                    }}
                  >
                    <input
                      type="text"
                      placeholder="افزودن فیلتر جدید"
                      style={{
                        backgroundColor: colors.main3,
                        color: colors.font1,
                      }}
                      value={filterTxt}
                      onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        setFilterTxt(e.target.value);
                      }}
                    />
                    <button
                      style={{
                        backgroundColor: colors.main3,
                        color: colors.font1,
                      }}
                    >
                      <span className="la la-plus-square"></span>
                    </button>
                  </form>
                </div>
              )}
            </div>
          )}
        </div>
        <form
          onSubmit={(e: FormEvent<HTMLFormElement>) => {
            e.preventDefault();
            dispatch(fetchNews(false, search));
          }}
          className="SearchNews"
          style={{
            border: `1px solid ${colors.border2}`,
          }}
        >
          <input
            value={search}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              e.target.value.length < 40 && setSearch(e.target.value)
            }
            style={{ color: colors.font1 }}
            type="text"
            placeholder="جستجو در اخبار ..."
          />
          <button className="icon-search"></button>
        </form>
        <>
          <div className="newsFilterName">
            <span>نام فیلتر : </span>
            <span
              className="newsFilterNameSpan"
              style={{ color: colors.font4 }}
            >
              {filterExists ? activeFilter().name : "بدون فیلتر"}
            </span>
          </div>
          <ToggleButton text="کلیدواژه‌ها">
            {activeFilter().keywords &&
              activeFilter().keywords.map((item, index) => {
                const key = `asideNewsKeyword${item.name}${index}`;
                return (
                  <div className="asideNewsListItem" key={key}>
                    <div className="asideNewsItemRight">
                      <div
                        className="asideNewsRadio"
                        onClick={() =>
                          addNewOption("changeKeyword", false, { index })
                        }
                      >
                        {item.active && (
                          <div
                            className="asideNewsRadioCheck"
                            style={{
                              backgroundColor: colors.green1,
                            }}
                          ></div>
                        )}
                      </div>
                      <span
                        className="asideNewsTitle"
                        style={{ color: colors.font3, cursor: "pointer" }}
                        onClick={() =>
                          addNewOption("changeKeyword", false, { index })
                        }
                      >
                        {item.name}
                      </span>
                    </div>
                    <div className="asideNewsIcons">
                      <span
                        className="la la-trash"
                        onClick={() => addNewOption("changeKeyword", { index })}
                      ></span>
                    </div>
                  </div>
                );
              })}
            {activeFilter().keywords && activeFilter().keywords.length < 10 && (
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  addNewOption("changeKeyword");
                }}
                className="newsAsideAddMore"
                style={{
                  backgroundColor: colors.main3,
                }}
              >
                <input
                  type="text"
                  className="addKeywordTitle"
                  placeholder="افزودن کلیدواژه"
                  onChange={(e) => handleKyewordTxtChange(e.target.value)}
                  value={keywordTxt}
                />
                <button className="addKeywordIcon">
                  <span className="la la-plus"></span>
                </button>
              </form>
            )}
            {!filterExists && (
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  addNewOption("changeKeyword");
                }}
                className="newsAsideAddMore"
                style={{
                  backgroundColor: colors.main3,
                }}
              >
                <input
                  type="text"
                  className="addKeywordTitle"
                  placeholder="افزودن کلیدواژه"
                  onChange={(e) => handleKyewordTxtChange(e.target.value)}
                  value={keywordTxt}
                />
                <button className="addKeywordIcon">
                  <span className="la la-plus"></span>
                </button>
              </form>
            )}
          </ToggleButton>
          <ToggleButton text="خبرگزاری‌ها">
            {sources.list.map((item, index) => {
              const key = `asideNewsKhabargozari${item.id}${index}`;
              return (
                <div className="asideNewsListItem" key={key}>
                  <div className="asideNewsItemRight">
                    <div
                      className="asideNewsRadio"
                      onClick={() => addNewOption(item.id)}
                    >
                      {activeFilter() &&
                        activeFilter().sources &&
                        activeFilter().sources.includes(item.id) && (
                          <div
                            className="asideNewsRadioCheck"
                            style={{
                              backgroundColor: colors.green1,
                            }}
                          ></div>
                        )}
                    </div>
                    <span
                      className="asideNewsTitle"
                      style={{ color: colors.font3, cursor: "pointer" }}
                      onClick={() => addNewOption(item.id)}
                    >
                      {item.name}
                    </span>
                  </div>
                  <div className="asideNewsIcons">
                    <span
                      className="la la-star"
                      style={{ color: colors.border6 }}
                    ></span>
                  </div>
                </div>
              );
            })}
          </ToggleButton>
        </>
      </Box>
    </DropDown>
  );
};

const flexRow1: string = `
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const flex: string = `
  display: flex;
  align-items: center;
`;

const Box = styled.div`
  max-height: calc(100vh - 60px);
  min-height: 500px;
  overflow-y: auto;
  &::-webkit-scrollbar {
    width: 0.3em !important;
    height: 0.2em;
    background: #00000000;
    border-radius: 15px;
  }
  &::-webkit-scrollbar-track {
    border-radius: 15px;
  }
  &::-webkit-scrollbar-thumb {
    border-radius: 15px;
    background-color: ${mainColors.blue};
    outline: 1px solid slategrey;
  }
  padding: 15px;
  .newsFilterOption {
    position: relative;
    .newsFilterOptionChildBox {
      position: absolute;
      width: 100%;
      z-index: 100;
      top: 103%;
      padding: 15px;
      .addNewFilterAsideNews {
        display: flex;
        align-items: center;
        form {
          display:flex;
          align-items: center;
          width: 100%;
          input {
            width: 100%;
            flex: 1;
            height: 30px;
            padding: 0 15px;
            font-size: .85rem;
            &::placeholder {
              font-size: .85rem;
              font-weight: 300;
            }
          }
          button {
            ${flex}
            justify-content: center;
            width: 30px;
            height: 30px;
            span {
              ${flex}
              justify-content: center;
              font-size: 1.3rem;
              margin-top: 1px;
            }
          }
        }
      }
      .newsAsideAddMore {
        transition: all ease-in-out .2s;
        -webkit-transition: all ease-in-out .2s;
        -ms-transition: all ease-in-out .2s;
        -moz-transition: all ease-in-out .2s;
        &:hover {
          transform: scale(1.04)
        }
      }
    }
    .newsBtnFilterOption {
      ${flexRow1};
      cursor: pointer;
      background-color: ${mainColors.blue};
      color: #fff;
      padding: 5px 10px;
      border-radius: 4px;
      margin-bottom: 15px;
      .titleBtnFilterOption {
        font-weight: 500;
        font-size: 0.8rem;
      }
      span {
        ${flex}
        font-size: 0.6rem;
      }
    }
  }
  .SearchNews {
    ${flex}
    margin-bottom: 10px;
    height: 45px;
    padding: 15px;
    border-radius: 4px;
    overflow: hidden;
    input {
      flex: 1;
      color: #444;
      font-size: 0.8rem;
      &::placeholder {
        color: #a7a7a7;
        font-weight: 300;
      }
    }
    button {
      ${flex}
      color: #E3E3E3;
      background: transparent;
      font-size: 1.5rem;
      &::before {
        margin: 0 !important;
      }
    }
  }
  .newsFilterName {
    ${flex}
    border: 1px solid ${mainColors.blue};
    border-radius: 4px;
    margin-bottom: 10px;
    padding: 5px 10px;
    span {
      font-size: 0.85rem;
      color: #999;
    }
    .newsFilterNameSpan {
      font-weight: 500;
      margin-right: 5px;
    }
  }
.asideNewsListItem {
  ${flexRow1}
  margin-bottom: 10px;
  .asideNewsItemRight {
    ${flex}
    .asideNewsTitle {
      font-size: .85rem;
      margin-right: 10px;
    }
    .asideNewsRadio {
      cursor: pointer;
      width: 15px;
      height: 15px;
      background-color: #F6F6F6;
      border-radius: 4px;
      position: relative;
      .asideNewsRadioCheck {
        position: absolute;
        width: 10px;
        height: 10px;
        border-radius: 2px;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        margin: auto;
      }
    }
  }
  .asideNewsIcons {
    ${flex};
    span {
      color: #A7A7A7;
      cursor: pointer;
    }
    .la-edit {
      margin-left: 5px;
    }
  }
}
.newsAsideAddMore {
  ${flexRow1}
  cursor: pointer;
  background-color: #F6F6F6;
  margin-bottom: 10px;
  padding: 5px 10px;
  border-radius: 4px;
  span {
    color: #A7A7A7;
  }
  .addKeywordTitle {
    font-size: .85rem;
    font-weight: 500;
    flex: 1;
    color: #A7A7A7;
    padding: 0 10px;
    height: 100%;
  }
  .addKeywordIcon {
    ${flex}
    justify-content: center;
    padding: 1px;
    background-color: transparent;
    border: 2px solid #A7A7A7;
    border-radius: 2px;
    width: 40px;
    span {
      ${flex}
      font-size: .8rem;
    }
  }
}
`;

export default connect((state) => state)(Aside);
