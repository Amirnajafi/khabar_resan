import React from "react";

const MetaOg = (props) => {
  const { title, desription, image } = props;

  return (
    <>
      <title>{`میزخبر | ${title}`}</title>
      <meta name="title" content={title} />
      <meta name="description" content={desription} />

      <meta property="og:type" content="website" />
      <meta property="og:url" content={"mizkhabar.com"} />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={desription} />
      <meta property="og:image" content={image} />

      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:url" content={image} />
      <meta property="twitter:title" content={title} />
      <meta property="twitter:description" content={desription} />
      <meta property="twitter:image" content={image} />
    </>
  );
};

export default MetaOg;
