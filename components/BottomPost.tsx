import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { mainColors } from "../redux/reducer/colors/colorReducer";
import { bottomPostArr } from "../helpers/arr";
import { trunc } from "../helpers/props";
import Link from "next/link";

const BottomPost = (props) => {
  const { colors, newsDetails } = props;

  const { item } = newsDetails;
  const { similar } = item;

  return (
    <Box>
      <div
        className="bottomPostRecommend"
        style={{
          backgroundColor: colors.main4,
          border: `1px solid ${colors.border1}`,
        }}
      >
        <span className="icon-miz-logo"></span>
        <span
          className="bottomPostRecommendTitle"
          style={{ color: colors.font1 }}
        >
          اخبار مشابه این خبر
        </span>
      </div>
      <div className="row">
        {(similar || []).map((item, index) => {
          const key = `bottomPostRecommendItem${item.id}${index}`;
          if (index < 8) return null;
          return (
            <div className="col-12 col-md-3" key={key}>
              <Link href={`/news/[id]`} as={`/news/${item.id}`}>
                <a>
                  <div
                    className="bottomPostRecommendItem"
                    style={{ backgroundImage: `url(${item.image})` }}
                  >
                    <p className="bottomPostRecommendTitle">
                      {trunc(item.title, 30)}
                    </p>
                  </div>
                </a>
              </Link>
            </div>
          );
        })}
      </div>
    </Box>
  );
};

const flex: string = `
  display: flex;
  align-items: center;
`;

const Box = styled.div`
  margin: 30px 0;
  .bottomPostRecommend {
    ${flex};
    padding: 10px;
    margin-bottom: 15px;
    .icon-miz-logo {
      color: ${mainColors.blue};
      font-size: 0.7rem;
      margin-left: 10px;
    }
    .bottomPostRecommendTitle {
      font-weight: 500;
      font-size: 0.9rem;
    }
  }
  .bottomPostRecommendItem {
    height: 130px;
    background-position: center;
    background-size: cover;
    border-radius: 4px;
    position: relative;
    @media only screen and (max-width: 767px) {
      margin-bottom: 30px;
    }
    .bottomPostRecommendTitle {
      position: absolute;
      bottom: 5px;
      right: 5px;
      width: calc(100% - 10px);
      font-size: 0.85rem;
      font-weight: 500;
      background-color: rgba(0, 0, 0, 0.8);
      padding: 5px 10px;
      color: #fff;
    }
  }
`;

export default connect((state) => state)(BottomPost);
