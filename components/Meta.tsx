import React from "react";
import { mainColors } from "../redux/reducer/colors/colorReducer";

const Meta = () => {
  return (
    <>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta
        name="viewport"
        content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
      />
      <meta name="description" content="Description" />
      <meta name="keywords" content="Keywords" />

      <link rel="manifest" href="/manifest.json" />
      <link rel="shortcut icon" type="image/png" href="/icons/favicon.ico" />
      <link
        href="/icons/favicon-16x16.png"
        rel="icon"
        type="image/png"
        sizes="16x16"
      />
      <link
        href="/icons/favicon-96x96.png"
        rel="icon"
        type="image/png"
        sizes="96x96"
      />
      <link rel="apple-touch-icon" href="/apple-icon.png"></link>
      <meta name="theme-color" content={mainColors.blue} />
    </>
  );
};

export default Meta;
