import React, { useState, useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import styled from "styled-components";
import { mainColors } from "../redux/reducer/colors/colorReducer";
import { trunc, toPersian } from "../helpers/props";
import Link from "next/link";
import moment from "jalali-moment";
import InfiniteScroll from "react-infinite-scroll-component";
import Loading from "./Loading";
import { fetchNews, saveNews } from "../redux/actions";
import Share from "./Share";

const NewsList = (props) => {
  const { news, type } = props;
  const { list: mainList } = news;
  let list =
    type && type === "saved_news"
      ? mainList.filter((el) => el.bookmark === true)
      : mainList;
  const dispatch = useDispatch();
  const [shareId, setShareId] = useState("");
  const [message, setMssage] = useState("");

  const [active, setActive] = useState(1);
  const { colors } = props;

  useEffect(() => {
    const preActive = localStorage.getItem("list-view");
    if (preActive) setActive(Number(preActive));
  }, []);

  useEffect(() => {
    setTimeout(() => {
      setMssage("");
    }, 300);
  }, [message]);

  const listNewsCategorized = () => {
    return (
      <div className="listNewsCategorized">
        <div className="row">
          {list.map((item, index) => {
            const date = toPersian(
              moment(item.create_date, "YYYY-MM-DD hh:mm:ss")
                .locale("fa")
                .format("DD MMMM YYYY")
            );
            const key = `listNewsCategorized${item.id}${index}`;
            return (
              <div className="col-12 col-md-4" key={key}>
                <div className="newsCategorizedItem">
                  <Link
                    href={`/news/[id]`}
                    as={`/news/${item.mongo_id || item.id}`}
                  >
                    <a
                      className="newsCategorizedImg"
                      style={{ backgroundImage: `url(${item.image})` }}
                    />
                  </Link>
                  <div className="newsCategorizedContent">
                    <div className="newsCategorizedTopContent">
                      <span
                        className="newsSourceName"
                        style={{
                          backgroundColor: colors.btn,
                          color: colors.font5,
                        }}
                      >
                        {trunc(item.source_name, 12)}
                      </span>
                      <div className="newsCategorizedTopLeft">
                        <span className="newsCategorizedDate">{date}</span>
                        <Share
                          type="news"
                          location={`/news/${item.mongo_id || item.id}`}
                          isShow={key === shareId}
                          shareId={key}
                          set={setShareId}
                        />
                        <span
                          style={{
                            color: item.bookmark
                              ? colors.font5
                              : colors.border5,
                            cursor: "pointer",
                          }}
                          className={`icon-bookmark bookmark-icon`}
                          onClick={async () => {
                            setMssage(key);
                            await dispatch(
                              saveNews(
                                item.mongo_id || item.id,
                                item.bookmark ? "delete" : "post"
                              )
                            );
                          }}
                        >
                          <div style={{ opacity: message === key ? 1 : 0 }}>
                            انجام شد
                          </div>
                        </span>
                      </div>
                    </div>
                    <div className="newsCategorizedBottomContent">
                      <Link
                        href={`/news/[id]`}
                        as={`/news/${item.mongo_id || item.id}`}
                      >
                        <h3 style={{ color: colors.font1, cursor: "pointer" }}>
                          {trunc(item.title.trim(), 30)}
                        </h3>
                      </Link>
                      <p style={{ color: colors.font1 }}>
                        {trunc(item.summary.trim(), 90)}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  const listNewsCluttered = () => {
    return (
      <>
        <div className="listNewsCluttered">
          <div className="row">
            {list.map((item, index) => {
              const date = toPersian(
                moment(item.create_date, "YYYY-MM-DD hh:mm:ss")
                  .locale("fa")
                  .format("DD MMMM YYYY")
              );
              const key = `listNewsClutteredItem${item.id}${index}`;
              return (
                <div className="col-12 col-md-4" key={key}>
                  <div
                    className="listNewsClutteredItem"
                    style={{ backgroundImage: `url(${item.image})` }}
                  >
                    <div className="listNewsClutteredTop">
                      <span className="listNewsClutteredSource">
                        {trunc(item.source_name, 12)}
                      </span>
                      <div className="listNewsClutteredTopLeft">
                        <span className="listNewsClutteredDate">{date}</span>
                        <Share
                          type="news"
                          location={`/news/${item.mongo_id || item.id}`}
                          isShow={key === shareId}
                          shareId={key}
                          set={setShareId}
                        />
                        <span
                          style={{
                            color: item.bookmark
                              ? colors.font5
                              : colors.border5,
                            cursor: "pointer",
                          }}
                          className={`icon-bookmark bookmark-icon`}
                          onClick={async () => {
                            setMssage(key);
                            await dispatch(
                              saveNews(
                                item.mongo_id || item.id,
                                item.bookmark ? "delete" : "post"
                              )
                            );
                          }}
                        >
                          <div style={{ opacity: message === key ? 1 : 0 }}>
                            انجام شد
                          </div>
                        </span>
                      </div>
                    </div>
                    <div className="listNewsClutteredBottom">
                      <h3 style={{ cursor: "pointer" }}>
                        <Link
                          href={`/news/[id]`}
                          as={`/news/${item.mongo_id || item.id}`}
                        >
                          <a>{trunc(item.title.trim(), 25)}</a>
                        </Link>
                      </h3>
                      <p>{trunc(item.summary.trim(), 50)}</p>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </>
    );
  };

  const listNewsRow = () => {
    return (
      <>
        <div className="listNewsRow">
          {list.map((item, index) => {
            const date = toPersian(
              moment(item.create_date, "YYYY-MM-DD hh:mm:ss")
                .locale("fa")
                .format("DD MMMM YYYY")
            );
            const key = `listNewsRowItem${item.id}${index}`;
            return (
              <div
                className="listNewsRowItem"
                key={key}
                style={{ borderColor: colors.border3 }}
              >
                <div className="listNewsRowRight">
                  <Link
                    href={`/news/[id]`}
                    as={`/news/${item.mongo_id || item.id}`}
                  >
                    <a
                      className="listNewsRowImg"
                      style={{ backgroundImage: `url(${item.image})` }}
                    ></a>
                  </Link>
                </div>
                <div className="listNewsRowLeft">
                  <div className="listNewsRowContent">
                    <h3 style={{ color: colors.font1 }}>
                      {trunc(item.title.trim(), 25)}
                    </h3>
                    <p style={{ color: colors.font1 }}>
                      {trunc(item.summary.trim(), 70)}
                    </p>
                  </div>
                  <div className="listNewsRowLeftEnd">
                    <span className="listNewsRowDate">{date}</span>
                    <Share
                      type="news"
                      location={`/news/${item.mongo_id || item.id}`}
                      isShow={key === shareId}
                      shareId={key}
                      set={setShareId}
                    />
                    <span
                      style={{
                        color: item.bookmark ? colors.font5 : colors.border5,
                        cursor: "pointer",
                      }}
                      className={`icon-bookmark bookmark-icon`}
                      onClick={async () => {
                        setMssage(key);
                        await dispatch(
                          saveNews(
                            item.mongo_id || item.id,
                            item.bookmark ? "delete" : "post"
                          )
                        );
                      }}
                    >
                      <div style={{ opacity: message === key ? 1 : 0 }}>
                        انجام شد
                      </div>
                    </span>
                    <span className="listNewsRowSource">
                      {trunc(item.source_name, 12)}
                    </span>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </>
    );
  };

  const handleList = () => {
    switch (active) {
      case 1:
        return listNewsCategorized();

      case 2:
        return listNewsCluttered();

      case 3:
        return listNewsRow();

      default:
        return listNewsCategorized();
    }
  };

  return (
    <Box>
      <div
        className="headerShowNews"
        style={{ border: `1px solid ${colors.border3}` }}
      >
        <div className="rightHeaderShowNews">
          <span className="icon-miz-logo"></span>
          <span
            className="rightHeaderShowNewsText"
            style={{ color: colors.font3 }}
          >
            {type === "saved_news" ? "اخبار اختصاصی شما" : "آخرین اخبار"}
          </span>
        </div>
        <div className="leftHeaderShowNews">
          <span
            className="icon-list-view-03"
            style={{
              color: active === 3 ? colors.font5 : "#D8D8D8",
            }}
            onClick={() => {
              localStorage.setItem("list-view", "3");
              setActive(3);
            }}
          ></span>
          <span
            className="icon-list-view-02"
            style={{
              color: active === 2 ? colors.font5 : "#D8D8D8",
            }}
            onClick={() => {
              localStorage.setItem("list-view", "2");
              setActive(2);
            }}
          ></span>
          <span
            className="icon-list-view-01"
            style={{
              color: active === 1 ? colors.font5 : "#D8D8D8",
            }}
            onClick={() => {
              localStorage.setItem("list-view", "1");
              setActive(1);
            }}
          ></span>
        </div>
      </div>
      <InfiniteScroll
        dataLength={news.list.length}
        next={() => {
          if (type && type === "saved_news") {
            dispatch(fetchNews(true, null, null, "save_news"));
          } else {
            dispatch(fetchNews(true));
          }
        }}
        hasMore={news.hasMore}
        loader={
          type === "saved_news" ? null : (
            <div className="centerContainer">
              <Loading />
            </div>
          )
        }
        className="infinitScroll"
      >
        {handleList()}
      </InfiniteScroll>
    </Box>
  );
};

const flexRow1: string = `
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const flex: string = `
  display: flex;
  align-items: center;
`;

const Box = styled.div`
  .bookmark-icon {
    position: relative;
    font-size: 1.4rem;
    div {
      position: absolute;
      top: 15px;
      left: 15px;
      background-color: #99999955;
      padding: 5px;
      border-radius: 4px;
      color: #fff;
      font-size: .85rem;
      width: 60px;
      font-family: IRANSans;
      z-index: 100;
      transition: all linear 0.3s;
    }
  }
  .headerShowNews {
    ${flexRow1}
    margin-bottom: 10px;
    padding: 10px;
    .rightHeaderShowNews {
      .icon-miz-logo {
        color: ${mainColors.blue};
        font-size: 0.7rem;
      }
      .rightHeaderShowNewsText {
        font-size: 0.85rem;
        font-weight: 500;
        margin-right: 10px;
      }
    }
    .leftHeaderShowNews {
      ${flex}
      padding: 0 15px;
      span {
        ${flex}
        cursor: pointer;
        margin-right: 10px;
        font-size: 0.9rem;
      }
    }
  }
  .listNewsCategorized {
    .newsCategorizedItem {
      margin-bottom: 15px;
      .newsCategorizedImg {
        display: block;
        cursor: pointer;
        border: 1px solid #55555522;
        height: 140px;
        border-radius: 4px;
        background-position: center center;
        background-size: cover;
      }
      .newsCategorizedContent {
        padding: 10px;
        .newsCategorizedTopContent {
          ${flexRow1}
          margin-bottom: 10px;
          .newsSourceName {
            font-size: 0.75rem;
            padding: 3px 10px;
            border-radius: 4px;
            font-weight: 500;
          }
          .newsCategorizedTopLeft {
            ${flex}
            .newsCategorizedDate {
              margin-left: 5px;
              color: #bdbdbd;
              font-size: 0.7rem;
            }
            .icon-share, .lar {
              color: ${mainColors.blue};
              font-size: 1.4rem;
            }
          }
        }
        .newsCategorizedBottomContent {
          margin-top: 10px;
          h3 {
            font-size: 0.8rem;
            font-weight: 500;
          }
          p {
            font-size: 0.7rem;
          }.listNewsClutteredItem
        }
      }
    }
  }
  .listNewsCluttered {
    .listNewsClutteredItem {
      margin-bottom: 15px;
      background-color: #99999955;
      height: 150px;
      background-position: center;
      background-size: cover;
      border-radius: 4px;
      position: relative;
      .listNewsClutteredTop {
        ${flexRow1}
        background: rgb(0, 0, 0);
        background: linear-gradient(
          0deg,
          rgba(0, 0, 0, 0) 0%,
          rgba(0, 0, 0, 0.4066001400560224) 100%
        );
        padding: 5px;
        position: absolute;
        top: 0;
        right: 0;
        width: 100%;
        .listNewsClutteredSource {
          background-color: ${mainColors.btn};
          color: ${mainColors.font5};
          font-size: 0.8rem;
          border-radius: 4px;
          padding: 1px 5px;
        }
        .listNewsClutteredTopLeft {
          ${flex};
          .listNewsClutteredDate {
            font-size: 0.7rem;
            color: #fff;
            margin-left: 5px;
          }
          .icon-share, .lar {
            font-size: 1.4rem;
            color: #fff;
          }
        }
      }
      .listNewsClutteredBottom {
        position: absolute;
        background: rgb(0, 0, 0);
        background: linear-gradient(
          0deg,
          rgba(0, 0, 0, 0.46262254901960786) 0%,
          rgba(0, 0, 0, 0) 100%
        );
        bottom: 0;
        padding: 5px;
        right: 0;
        width: 100%;
        h3 {
          a {
            display: block;
            font-weight: 500;
            font-size: 0.8rem;
            color: #fff;
          }
        }
        p {
          font-size: 0.7rem;
          color: #fff;
        }
      }
    }
  }
  .listNewsRowItem {
    ${flexRow1}
    border-bottom: 1px solid;
    padding: 10px 0;
    .shareNewsPageItem {
      top: -14px;
      left: -5px;
    }
    @media only screen and (max-width: 740px) {
      .listNewsRowRight {
        .listNewsRowImg {
          height: 80px !important;
        }
      }
      .listNewsRowLeft {
        flex-direction: column-reverse;
        width: 100%;
        align-items: flex-start !important;
        .listNewsRowLeftEnd {
          width: 100%;
          margin-bottom: 5px;
          .listNewsRowSource {
            order: -1;
            margin-right: 0;
            margin-left: auto;
          }
        }
      }
    }
    .listNewsRowRight {
      ${flex}
      .listNewsRowImg {
        border: 1px solid #99999920;
        border-radius: 2px;
        height: 55px;
        background-size: cover;
        background-position: center;
        width: 80px;
        margin-left: 10px;
      }
    }
    .listNewsRowLeft {
      ${flex}
      flex: 1;
      .listNewsRowContent {
        flex: 1;
        h3 {
          font-weight: 500;
          font-size: 0.9rem;
        }
        p {
          font-size: 0.9rem;
        }
      }
      .listNewsRowLeftEnd {
        ${flex}
      }
      .listNewsRowSource {
        background-color: ${mainColors.blue};
        color: #fff;
        font-size: 0.7rem;
        border-radius: 4px;
        padding: 2px 8px;
        margin-right: 10px;
        font-weight: 500;
      }
      .icon-share, .lar {
        ${flex}
        font-size: 1.3rem;
        margin-right: 10px;
        &::before {
          ${flex}
          margin: 0 !important;
        }
      }
      .listNewsRowDate {
        font-size: 0.85rem;
        color: #bdbdbd;
      }
    }
  }
`;

export default connect((state) => state)(NewsList);
