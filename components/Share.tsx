import React, { useRef, useEffect } from "react";
import { TelegramShareButton, WhatsappShareButton } from "react-share";
import styled from "styled-components";
import { useSelector } from "react-redux";

const Share = (props) => {
  const { isShow, set, shareId } = props;
  const url = `mizkhabar.com${props.location}`;
  const container = useRef(null);

  const { colors } = useSelector((state) => state);

  const style = isShow ? { display: "block" } : { display: "none" };

  useEffect(() => {
    window.addEventListener("click", (e: any) => {
      if (!e.target.contains(container.current)) {
        return;
      }
      set("");
    });
  }, []);

  const handleCopy = () => {
    const el = document.createElement("textarea");
    el.value = url;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
  };

  return (
    <>
      <div style={{ position: "relative" }} ref={container}>
        <ShareBox
          style={style}
          borderBox={colors.border3}
          bgBox={colors.aside1}
          icon={colors.font3}
          className="shareNewsPageItem"
        >
          <div className="postPageSotialMedia">
            <ul className="postSotialMediaBox">
              <li className="postSotialMediaItem">
                <WhatsappShareButton url={url}>
                  <span
                    onClick={() => set("")}
                    className="lab la-whatsapp"
                  ></span>
                </WhatsappShareButton>
              </li>
              <li className="postSotialMediaItem">
                <TelegramShareButton url={url}>
                  <span
                    onClick={() => set("")}
                    className="lab la-telegram"
                  ></span>
                </TelegramShareButton>
              </li>
              <li className="postSotialMediaItem" onClick={handleCopy}>
                <span onClick={() => set("")} className="la la-copy"></span>
              </li>
            </ul>
          </div>
        </ShareBox>
        {props.type && props.type === "news" ? (
          <>
            <span
              className="icon-share"
              onClick={() => set(shareId)}
              style={{ color: colors.font5, cursor: "pointer" }}
            ></span>
          </>
        ) : (
          <>
            {!props.type ? (
              <div className="shareFooterSearch" onClick={() => set(shareId)}>
                <i className="la la-share-alt" />
                <i className="la la-twitter" />
                <span>میزخبر را به اشتراک بگذار</span>
              </div>
            ) : (
              <span
                className="icon-share"
                onClick={() => set(shareId)}
                style={{ color: colors.font5, cursor: "pointer" }}
              ></span>
            )}
          </>
        )}
      </div>
    </>
  );
};

const ShareBox = styled.div((props) => {
  const { borderBox, bgBox, icon } = props;

  return {
    position: "absolute",
    top: "-4px",
    left: "-4px",
    zIndex: 1000,
    ".postPageSotialMedia": {
      ".postSotialMediaBox": {
        display: "flex",
        alignItems: "center",
        border: `1px solid ${borderBox}`,
        padding: 15,
        borderRadius: "25px",
        position: "sticky",
        top: 30,
        backgroundColor: bgBox,
        ".postSotialMediaItem": {
          display: "flex",
          alignItems: "center",
          marginRight: 15,
          "&:first-of-type": {
            marginRight: 0,
          },
          span: {
            display: "flex",
            fontSize: "1.2rem",
            color: icon,
            cursor: "pointer",
          },
        },
      },
    },
  };
});

export default Share;
