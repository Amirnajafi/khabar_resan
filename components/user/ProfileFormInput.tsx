import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { defaultColors } from "../../redux/reducer/colors/colorReducer";
import { flexRow1 } from "../../public/styles/modules";

const ProfileFormInput = (props) => {
  const {
    onChange,
    onBlur,
    value,
    labelId,
    label,
    colors,
    errMsg,
    hasErr,
    editAble,
    type,
    name,
    disable,
  } = props;
  const id = `inputForm${labelId}`;

  return (
    <Input>
      <label
        style={{ color: hasErr ? defaultColors.danger : defaultColors.grey }}
        htmlFor={id}
      >
        {hasErr ? errMsg : label}
      </label>
      <div
        className="newsProfileInputBox"
        style={{ borderColor: hasErr ? defaultColors.danger : colors.border5 }}
      >
        <input
          style={{ color: colors.font1 }}
          name={name}
          value={value}
          onChange={onChange}
          onBlur={onBlur}
          id={id}
          type={type || "text"}
          autoComplete="off"
          disabled={!editAble || disable}
        />
      </div>
    </Input>
  );
};

const Input = styled.div`
  label {
    font-size: 0.85rem;
    font-weight: 500;
    display: block;
    margin-bottom: 5px;
  }
  .newsProfileInputBox {
    ${flexRow1}
    border: 1px solid;
    padding: 0 15px;
    border-radius: 4px;
    height: 40px;
    input {
      flex: 1;
      width: 100px;
      height: 100%;
      font-size: 0.9rem;
    }
  }
`;

export default connect((state) => state)(ProfileFormInput);
