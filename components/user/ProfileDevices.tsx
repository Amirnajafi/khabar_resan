import React from "react";
import styled from "styled-components";
import ProfileInfoBox from "./ProfileInfoBox";
import Table from "../reuse/Table";

const headers = [
  { id: "devicesNameOrType", name: "دستگاه / سیستم عامل" },
  { id: "devicesIp", name: "آدرس IP" },
  { id: "devicesFirstActivity", name: "تاریخ اولین فعالیت" },
  { id: "devicesOffer", name: "تخفیف" },
  { id: "devicesStartDate", name: "شروع اشتراک" },
];

const rows = [
  {
    id: 1,
    devicesNameOrType: "os x Macintosh Chrome Desktop",
    devicesIp: "123.322.23.21",
    devicesFirstActivity: "۳۰۰۰ تومان",
    devicesOffer: "۴۰۰ تومان",
    devicesStartDate: "۱۱ تیر ۱۳۴۳",
  },
  {
    id: 2,
    devicesNameOrType: "Desktop os",
    devicesIp: "123.322.23.21",
    devicesFirstActivity: "۳۰۰۰ تومان",
    devicesOffer: "۴۰۰ تومان",
    devicesStartDate: "۱۱ تیر ۱۳۴۳",
  },
  {
    id: 3,
    devicesNameOrType: "Desktop os Chrome",
    devicesIp: "123.322.23.21",
    devicesFirstActivity: "۳۰۰۰ تومان",
    devicesOffer: "۴۰۰ تومان",
    devicesStartDate: "۱۱ تیر ۱۳۴۳",
  },
];

const ProfileDevices = () => {
  return (
    <ProfileDevicesBox>
      <ProfileInfoBox title="در حال حاضر حسبا کاربری شما در دستگاه های زیر فعال است" />
      <Table headers={headers} rows={rows} />
    </ProfileDevicesBox>
  );
};

const ProfileDevicesBox = styled.div``;

export default ProfileDevices;
