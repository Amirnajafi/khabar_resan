import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import ProfileFormInput from "./ProfileFormInput";
import { Formik } from "formik";
import { toPersian, setIfNumber } from "../../helpers/props";
import Button from "../reuse/Button";

const ProfileFormInfo = (props) => {
  const {
    editAble,
    handleSubmit,
    initialData,
    infoInputs,
    validationSchema,
  } = props;

  return (
    <Box>
      <Formik
        initialValues={initialData}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        enableReinitialize={true}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue,
        }) => {
          return (
            <form className="row" onSubmit={handleSubmit}>
              {infoInputs.map(
                ({ objKey, id, labelId, label, type, col, disable }, index) => {
                  const key = `itemInputProfile${index}${id}${labelId}`;
                  const hasErr = errors[objKey] && touched[objKey];
                  const errMsg = errors[objKey];
                  return (
                    <div className={col || `col-4`} key={key}>
                      <ProfileFormInput
                        onChange={
                          objKey === "mobile"
                            ? (e) => setIfNumber(e, setFieldValue, 11)
                            : handleChange
                        }
                        onBlur={handleBlur}
                        hasErr={hasErr}
                        errMsg={errMsg}
                        type={type}
                        disable={disable}
                        value={
                          type === "password" || type === "email"
                            ? values[objKey]
                            : toPersian(values[objKey])
                        }
                        label={label}
                        name={objKey}
                        labelId={labelId}
                        editAble={editAble}
                        setFieldValue={setFieldValue}
                      />
                    </div>
                  );
                }
              )}
              {editAble && (
                <div className="col-12">
                  <div className="btnSubmitProfile">
                    <Button style={{ width: 120, height: 30 }}>ثبت</Button>
                  </div>
                </div>
              )}
            </form>
          );
        }}
      </Formik>
    </Box>
  );
};

const Box = styled.div`
  margin-bottom: 30px;
  .btnSubmitProfile {
    display: flex;
    justify-content: flex-end;
    margin: 30px 0 0;
  }
`;

export default connect((state) => state)(ProfileFormInfo);
