import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { mainColors } from "../../redux/reducer/colors/colorReducer";
import { flex, flexRow1 } from "../../public/styles/modules";

const ProfileInfoBox = (props) => {
  const { colors, title, hasBorder, renderLeft } = props;

  return (
    <Box
      style={{
        backgroundColor: colors.aside1,
        borderColor: hasBorder ? colors.border4 : colors.border3,
      }}
    >
      <div className="profileInfoBoxRight">
        <span className="icon-miz-logo"></span>
        <span className="titleHeadPf" style={{ color: colors.font7 }}>
          {title}
        </span>
      </div>
      {renderLeft && renderLeft()}
    </Box>
  );
};

const Box = styled.div`
  ${flexRow1}
  margin-bottom: 15px;
  border: 1px solid;
  padding: 0 15px;
  height: 40px;
  .profileInfoBoxRight {
    ${flex}
  }
  span {
    ${flex}
  }
  .icon-miz-logo {
    color: ${mainColors.blue};
    font-size: 0.7rem;
    margin-left: 10px;
  }
  .titleHeadPf {
    font-weight: 500;
    font-size: 0.85rem;
  }
`;

export default connect((state) => state)(ProfileInfoBox);
