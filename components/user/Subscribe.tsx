import React, { Fragment } from "react";
import styled from "styled-components";
import Button from "../reuse/Button";
import { defaultColors } from "../../redux/reducer/colors/colorReducer";
import { flexRow1, flex } from "../../public/styles/modules";
import { useSelector } from "react-redux";
import { daysRemaining, toPersian } from "../../helpers/props";
import Link from "next/link";

const Subscribe = (props) => {
  const { type } = props;

  const { user } = useSelector(({ user }) => ({ user }));
  const { premium_date, last_package_days } = user.item;
  const count = daysRemaining(premium_date);

  const checkType = () => {
    switch (type) {
      case "danger":
        return {
          backgroundColor: defaultColors.danger,
          borderColor: defaultColors.danger2,
        };

      case "success":
        return {
          backgroundColor: defaultColors.success,
          borderColor: defaultColors.success2,
        };

      case "warn":
        return {
          backgroundColor: defaultColors.warn,
          borderColor: defaultColors.warn2,
        };

      default:
        return {
          backgroundColor: defaultColors.warn,
          borderColor: defaultColors.warn2,
        };
    }
  };

  const checkType2 = () => {
    switch (type) {
      case "danger":
        return {
          backgroundColor: defaultColors.danger2,
        };

      case "success":
        return {
          backgroundColor: defaultColors.success2,
        };

      case "warn":
        return {
          backgroundColor: defaultColors.warn2,
        };

      default:
        return {
          backgroundColor: defaultColors.warn2,
        };
    }
  };

  const txt =
    premium_date && count > 0
      ? `از اشتراک شما ${count} روز باقی مانده است`
      : "شما اشتراک فعالی ندارید";

  return (
    <MainContainer style={{ ...checkType() }}>
      <div className="subcribeRight">
        <div className="subcribeRightTime" style={{ ...checkType2() }}>
          <span>{`${toPersian(last_package_days) || "۰"} روزه`}</span>
        </div>
        <div className="subcribeRightContent">
          <h4>وضعیت اشتراک</h4>
          <p>{txt}</p>
        </div>
      </div>
      <Link href="/packages">
        <div className="subcribeLeft">
          <Button style={{ width: 120, height: 30 }}>تمدید اشتراک</Button>
        </div>
      </Link>
    </MainContainer>
  );
};

const MainContainer = styled.div`
  ${flexRow1}
  margin-bottom: 30px;
  padding: 0 30px;
  border: 1px solid;
  border-radius: 4px;
  height: 80px;
  @media only screen and (max-width: 575px) {
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    height: auto !important;
    padding: 15px;
    .subcribeRight {
      margin-bottom: 15px;
    }
    .subcribeLeft {
      width: 100%;
      button {
        width: 100%;
      }
    }
  }
  .subcribeRight {
    ${flex}
    .subcribeRightTime {
      ${flex}
      margin-left: 10px;
      height: 44px;
      width: 66px;
      border-radius: 4px;
      justify-content: center;
      span {
        color: #fff;
        font-weight: 500;
      }
    }
    .subcribeRightContent {
      h4,
      p {
        color: #fff;
      }
      h4 {
        font-size: 0.9rem;
      }
      p {
        font-size: 0.9rem;
      }
    }
  }
  .subcribeLeft {
    font-size: 0.8rem;
  }
`;

export default Subscribe;
