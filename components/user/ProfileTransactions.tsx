import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import ProfileInfoBox from "./ProfileInfoBox";
import Table from "../reuse/Table";
import { fetchPayments } from "../../redux/actions";
import moment from "jalali-moment";
import { toPersian } from "../../helpers/props";

const headers = [
  { id: "transactionsPayDate", name: "تاریخ پرداخت" },
  { id: "transactionsType", name: "عنوان بسته" },
  { id: "transactionsPayment", name: "قیمت" },
];

const ProfileTransactions = () => {
  const dispatch = useDispatch();
  const { payments } = useSelector((state) => state);
  const [rows, setRows] = useState([]);

  useEffect(() => {
    dispatch(fetchPayments());
  }, []);

  useEffect(() => {
    const array = [];

    for (let i = 0; i < payments.list.length; i++) {
      const item = payments.list[i];
      array.push({
        id: 3,
        transactionsPayDate: toPersian(
          moment(item.create_date).locale("fa").format("jDD jMMMM jYYYY")
        ),
        transactionsType: item.package_title,
        transactionsPayment: item.price
          ? `${toPersian(item.price)} ریال`
          : "رایگان",
      });
    }

    setRows(array);
  }, [payments]);

  return (
    <ProfileTransactionsBox>
      <ProfileInfoBox title="پرداخت های شما" />
      <Table headers={headers} rows={rows} />
    </ProfileTransactionsBox>
  );
};

const ProfileTransactionsBox = styled.div``;

export default ProfileTransactions;
