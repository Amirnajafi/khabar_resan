import React, { useState } from "react";
import styled from "styled-components";
import DropDown from "../reuse/DropDown";
import DropDownBtn from "../reuse/DropDownBtn";
import { connect } from "react-redux";
import { flexRow1 } from "../../public/styles/modules";
import { defaultColors } from "../../redux/reducer/colors/colorReducer";
import { changeActiveProfile } from "../../redux/actions";

export const AsideProfileBtn = connect((state) => state, {
  changeActiveProfile,
})((props) => {
  const { title, active, activeProfile, changeActiveProfile } = props;
  const isActive = active === activeProfile;

  return (
    <Button
      onClick={() => changeActiveProfile(active)}
      style={{
        backgroundColor: isActive ? defaultColors.blue : defaultColors.blue5,
      }}
    >
      <span>{title}</span>
      {isActive && <span className="la la-arrow-left"></span>}
    </Button>
  );
});

const AsideProfile = (props) => {
  const [open, setOpen] = useState(false);
  const { colors } = props;

  const handleToggle = () => setOpen(!open);

  return (
    <DropDown
      title="اطلاعات کاربری"
      open={open}
      handleToggle={handleToggle}
      btnClassName="d-xl-none"
      dpStyle={{
        "@media only screen and (min-width: 1200px)": {
          height: "auto !important",
        },
      }}
    >
      <Box
        style={{ borderColor: colors.border1, backgroundColor: colors.aside1 }}
      >
        <AsideProfileBtn active={1} title={"اطلاعات کاربری"} />
        <AsideProfileBtn active={2} title={"تراکنش‌های شما"} />
        {/* <AsideProfileBtn active={3} title={"دستگاه‌های شما"} /> */}
      </Box>
    </DropDown>
  );
};

const Button = styled.button`
  ${flexRow1}
  width: 100%;
  height: 30px;
  margin-bottom: 15px;
  padding: 0 15px;
  border-radius: 4px;
  span {
    color: #fff;
    font-size: 0.9rem;
  }
  .la {
    font-size: 1.3rem;
  }
  &:last-of-type {
    margin-bottom: 0;
  }
`;

const Box = styled.div`
  padding: 15px;
  border: 1px solid;
  @media only screen and (max-width: 1199px) {
    margin-bottom: 15px;
  }
`;

export default connect((state) => state)(AsideProfile);
