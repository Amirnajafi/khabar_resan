import React, { useState } from "react";
import styled from "styled-components";
import { connect, useDispatch } from "react-redux";
import ProfileInfoBox from "./ProfileInfoBox";
import ProfileFormInfo from "./ProfileFormInfo";
import { defaultColors } from "../../redux/reducer/colors/colorReducer";
import * as Yup from "yup";
import { toPersian } from "../../helpers/props";
import Alert from "react-s-alert";
import { editProfile } from "../../redux/actions";

const infoInputs = [
  {
    id: 1,
    label: "نام و نام‌خانوادگی",
    labelId: "nameProfile",
    type: "text",
    objKey: "name",
    disable: false,
    col: "col-12 col-md-4 marginBtMd15",
  },
  {
    id: 2,
    label: "شماره همراه",
    labelId: "mobileProfile",
    type: "text",
    objKey: "mobile",
    disable: true,
    col: "col-12 col-md-4 marginBtMd15",
  },
  {
    id: 3,
    label: "آدرس ایمیل",
    labelId: "emailProfile",
    type: "email",
    objKey: "email",
    disable: false,
    col: "col-12 col-md-4 marginBtMd15",
  },
];

const infoPassword = [
  {
    id: 2,
    label: "رمز عبور جدید",
    labelId: "oldPassProfile",
    type: "password",
    objKey: "newPass",
    disable: false,
    col: "col-12 col-md-6 marginBtMd15",
  },
  {
    id: 3,
    label: "تکرار رمز عبور جدید",
    labelId: "repeatPassProfile",
    type: "password",
    objKey: "repeatPass",
    disable: false,
    col: "col-12 col-md-6 marginBtMd15",
  },
];

const profileSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, "کمتر از ۳ حرف مجاز نیست")
    .max(50, "بیشتر از ۵۰ حرف مجاز نیست")
    .trim(),
  mobile: Yup.string()
    .min(9, "کمتر از ۹ حرف مجاز نیست")
    .max(11, "بیشتر از ۱۱ حرف مجاز نیست")
    .trim()
    .required("شماره همراه اجباری می‌باشد"),
  email: Yup.string().email("ایمیل وارد شده صحیح نیست"),
});

const newPassSchema = Yup.object().shape({
  newPass: Yup.string()
    .min(8, "حداقل ۸ کاراکتر وارد کنید")
    .max(40, "بیشتر از ۴۰ کاراکتر مجاز نیست")
    .required("رمز عبور خود را وارد کنید"),
  repeatPass: Yup.string()
    .oneOf([Yup.ref("newPass"), null], "رمزعبور مطابقت ندارد")
    .required("تکرار رمزعبور اجباری می‌باشد"),
});

const ProfileInfo = (props) => {
  const [editAble, setEditAble] = useState(false);
  const dispatch = useDispatch();
  const { user } = props;
  const { item } = user;

  const initialInfoData = {
    name: item.name || "",
    mobile: item.mobile || "",
    email: item.email || "",
  };

  const initialPasswordData = {
    newPass: "",
    repeatPass: "",
  };

  const handleInfoSubmit = async (values, actions) => {
    const { setSubmitting, setErrors } = actions;

    setSubmitting(true);
    try {
      await dispatch(editProfile(values));
      Alert.success("اطلاعات با موفقیت ویرایش شد", {
        position: "top-right",
        effect: "jelly",
      });
      setEditAble(false);
    } catch (err) {
      if (err.response.status === 401) {
        setErrors({ email: "ایمیل تکراری می‌باشد" });
      }
    }
    setSubmitting(false);
  };

  const handlePasswordSubmit = async ({ newPass }, actions) => {
    const { setSubmitting, resetForm } = actions;
    setSubmitting(true);
    await dispatch(editProfile({ password: newPass }, true));
    resetForm();
    Alert.success("رمز عبور شما با موفقیت ویرایش شد", {
      position: "top-right",
      effect: "jelly",
    });
    setSubmitting(false);
  };

  return (
    <Box>
      {/* Info */}
      {/* ================================================================================= */}
      <ProfileInfoBox
        title="مشخصات شما"
        renderLeft={() =>
          !editAble && (
            <span className="profileInfoEdit" onClick={() => setEditAble(true)}>
              ویرایش
            </span>
          )
        }
      />
      <ProfileFormInfo
        handleSubmit={handleInfoSubmit}
        initialData={initialInfoData}
        editAble={editAble}
        infoInputs={infoInputs}
        validationSchema={profileSchema}
      />
      {/* ================================================================================= */}
      {/* Password */}
      {/* ================================================================================= */}
      <ProfileInfoBox hasBorder={true} title="تغییر کلمه عبور" />
      <ProfileFormInfo
        handleSubmit={handlePasswordSubmit}
        initialData={initialPasswordData}
        editAble={true}
        validationSchema={newPassSchema}
        infoInputs={infoPassword}
      />
      {/* ================================================================================= */}
    </Box>
  );
};

const Box = styled.div`
  .marginBtMd15 {
    @media only screen and (max-width: 767px) {
      margin-bottom: 15px;
    }
  }
  .profileInfoEdit {
    color: ${defaultColors.blue};
    cursor: pointer;
  }
`;

export default connect((state) => state)(ProfileInfo);
