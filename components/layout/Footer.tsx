import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { darkColors } from "../../redux/reducer/colors/colorReducer";
import {
  FacebookShareButton,
  TelegramShareButton,
  LinkedinShareButton,
} from "react-share";

const logoList = [
  { url: "/img/footer/Bitmap5.png" },
  { url: "/img/footer/Bitmap4.png" },
  { url: "/img/footer/Bitmap3.png" },
  { url: "/img/footer/Bitmap2.png" },
  { url: "/img/footer/Bitmap.png" },
];

const Footer = (props) => {
  const { colors: colorState, noTop, darkOnly } = props;
  const colors = darkOnly ? darkColors : colorState;
  const url = `mizkhabar.com`;

  const handleCopy = () => {
    const el = document.createElement("textarea");
    el.value = url;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
  };

  return (
    <>
      <MainFooter>
        {!noTop && (
          <section
            className="topMainFooter"
            style={{ backgroundColor: colors.footer1 }}
          >
            <div className="container">
              <div className="topFooterContainer  d-none d-md-flex">
                <div className="rightTopFooter">
                  <div className="logoFooter">
                    <span className="icon-miz-logo"></span>
                    <span className="icon-miz-typo"></span>
                  </div>
                </div>
                <div className="textFooter">
                  <p>
                    میز خبر محصولی از هلدینگ دانش بنیان آریانا، یک اپلیکیشن تحت
                    وب برای دسترسی آسان به خبرهای روز دنیا است که برای راحتی
                    کاربران امکان ایجاد روزنامه اختصاصی را فراهم کرده است.
                    کاربران می توانند خبرهای دلخواهشان را از معتبرترین خبرگزاری
                    ها انتخاب و بر اساس آن اخبار مورد نیازشان را دنبال کنند.
                  </p>
                </div>
                <div className="leftTopFooter d-none d-lg-block">
                  {logoList.map((item, index) => {
                    const key = `خبرگذاری${index}`;
                    return (
                      <img key={`${key}footerItem`} src={item.url} alt={key} />
                    );
                  })}
                </div>
              </div>
            </div>
          </section>
        )}
        <section
          className="centerMainFooter d-none d-md-flex"
          style={{
            backgroundColor: colors.footer2,
            border: colors.theme === "dark" ? "1px solid #313131" : 0,
          }}
        >
          <div className="container">
            <div className="centerFooterContainer">
              <ul>
                {/* <li>
                  <a href="#">
                    درباره <strong>میزخبر</strong>
                  </a>
                </li> */}
                <li>
                  <a href="tel:02188665157" target="_blank">
                    تماس با <strong>میزخبر</strong>
                  </a>
                </li>
                <li>
                  <a href="https://www.onmiz.com" target="_blank">
                    اپلیکیشن <strong>میز</strong>
                  </a>
                </li>
                <li>
                  <a
                    href="http://www.ariana-holding.com/jobs.html"
                    target="_blank"
                  >
                    همکاری با ما
                  </a>
                </li>
                <li>
                  <a target="_blank" href="http://ariana-holding.com">
                    هلدینگ آریانا
                  </a>
                </li>
              </ul>
              <div className="shareFooterLinks d-none d-lg-flex">
                <div className="shareFooterItem">
                  <FacebookShareButton url={url}>
                    <span className="la la-facebook"></span>
                  </FacebookShareButton>
                </div>
                <div className="shareFooterItem">
                  <LinkedinShareButton url={url}>
                    <span className="lab la-linkedin-in"></span>
                  </LinkedinShareButton>
                </div>
                <div className="shareFooterItem">
                  <TelegramShareButton url={url}>
                    <span className="lab la-telegram"></span>
                  </TelegramShareButton>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="bottomMainFooter">
          <div className="container">
            <div className="bottomFooterContainer">
              <p className="d-none d-xl-block">
                <strong>میزخبر</strong> موتور جستجوی خبر در خبرگزاری های جمهوری
                اسلامی ایران - نماد تجاری وابسته به شرکت آریانا بازرگانی سیستم
                صبا
              </p>
              <p dir="ltr">
                Copyright © 2019-2020{" "}
                <a href="http://www.ariana-holding.com" target="_blank">
                  Ariana Co
                </a>
                . All rights reserved.
              </p>
            </div>
          </div>
        </section>
      </MainFooter>
    </>
  );
};

const flexRow1: string = `
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const flex: string = `
  display: flex;
  align-items: center;
`;

const MainFooter = styled.footer`
  > .topMainFooter {
    .topFooterContainer {
      height: 100px;
      ${flex}
      .rightTopFooter {
        ${flex}
        .logoFooter {
          margin-left: 15px;
          ${flex}
          span {
            font-size: 1.5rem;
            color: #d3d3d3;
          }
        }
      }
      .textFooter {
        flex: 1;
        p {
          font-size: 0.85rem;
          text-align: justify;
          color: #a2a1a1;
        }
      }
      .leftTopFooter {
        margin-right: 40px;
        img {
          margin-right: 10px;
        }
      }
    }
  }
  > .centerMainFooter {
    .centerFooterContainer {
      ${flexRow1}
      height: 70px;
      ul,
      .shareFooterLinks {
        ${flex}
      }
      ul {
        li {
          margin-left: 30px;
          @media only screen and (max-width: 991px) {
            margin-left: 15px;
          }
          a {
            color: #59689c;
            font-size: 0.85rem;
            span,
            strong {
              color: #364f88;
            }
          }
        }
      }
      .shareFooterLinks {
        .shareFooterItem {
          margin-right: 15px;
          span {
            color: #9ba1ae;
            font-size: 1.2rem;
          }
        }
      }
    }
  }
  > .bottomMainFooter {
    background-color: #313131;
    .bottomFooterContainer {
      ${flexRow1}
      height: 40px;
      @media only screen and (max-width: 1199px) {
        justify-content: center !important;
      }
      p {
        text-align: justify;
        color: #fff;
        font-size: 0.8rem;
        a,
        strong {
          font-weight: bold;
          color: #fff;
        }
      }
    }
  }
`;

export default connect((state) => state)(Footer);
