import React, { Fragment, FunctionComponent } from "react";
import Header from "./Header";
import Footer from "./Footer";

const Layout: FunctionComponent<any> = ({ children, renderNav }) => {
  return (
    <Fragment>
      <Header renderNav={renderNav} />
      {children}
      <Footer />
    </Fragment>
  );
};

export default Layout;
