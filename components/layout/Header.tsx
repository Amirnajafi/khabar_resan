import React, { useEffect, useRef, useState, Fragment } from "react";
import styled from "styled-components";
import { connect, useDispatch } from "react-redux";
import moment from "jalali-moment";
import { toPersian } from "../../helpers/props";
import {
  changeColor,
  fetchHome,
  logout,
  changeActiveProfile,
  fetchCategories,
} from "../../redux/actions";
import Head from "next/head";
import {
  mainColors,
  darkColors,
} from "../../redux/reducer/colors/colorReducer";
import Link from "next/link";

const Header = (props) => {
  const dispatch = useDispatch();
  const {
    colors: colorState,
    changeColor,
    noRecommend,
    noCategories,
    noPodcast,
    noNavOptions,
    renderNav,
    home,
    categories,
    darkOnly,
    auth,
  } = props;
  const colors = darkOnly ? darkColors : colorState;
  const { role } = auth;

  const { header_news } = home;
  const [open, setOpen] = useState(false);

  const container = useRef(null);
  const btn = useRef(null);

  const handleWindowClick = ({ target }) => {
    if (target.contains(btn.current)) {
      return;
    }
    setOpen(false);
  };

  useEffect(() => {
    if (role === "user") {
      dispatch(fetchHome(true));
      dispatch(fetchCategories());
      window.addEventListener("click", handleWindowClick);
      return () => window.removeEventListener("click", handleWindowClick);
    }
  }, []);

  return (
    <>
      <Head>
        <style>
          {`
            body {
              background-color: ${colors.main} !important;
            }
          `}
        </style>
      </Head>
      <MainHeader>
        <div
          className="mainTopHead"
          style={{ backgroundColor: colors.topHead }}
        >
          <div className="container">
            <div className="topHeaderContainer">
              <div className="rightTopHead">
                {!noPodcast && role === "user" && (
                  <div className="rightTopHeadTitle d-none d-sm-flex">
                    <span className="icon-headphones"></span>
                    <span>پادکست</span>
                  </div>
                )}
                <div className="rightTopHeadDate">
                  <span>
                    {toPersian(
                      moment().locale("fa").format("dddd, jDD jMMMM jYYYY")
                    )}
                  </span>
                </div>
              </div>
              {!noCategories && role === "user" && (
                <div className="leftTopHead">
                  {categories.list.map((item, index) => {
                    const key = `categoriesHeaderItem${index}${item.id}`;
                    if (index > 10) return null;
                    return (
                      <Fragment key={key}>
                        {item.name === "بورس" ? null : (
                          <Link
                            href={{
                              pathname: "/",
                              query: { category_id: item.id },
                            }}
                          >
                            <a className="leftTopHeadItem d-none d-xl-inline">
                              {item.name}
                            </a>
                          </Link>
                        )}
                      </Fragment>
                    );
                  })}
                </div>
              )}
            </div>
          </div>
        </div>
        <div
          className="bottomHeaderContainer"
          style={{ backgroundColor: colors.main2 }}
        >
          <div className="container">
            <div
              className="bottomHeader"
              style={{ backgroundImage: `url(${colors.headBackUrl})` }}
            >
              <h1 className="rightBottomHead">
                <Link href="/">
                  <a>
                    <img src={colors.headLogo} alt="میز خبر" />
                  </a>
                </Link>
              </h1>
              <nav className="leftBottomHead">
                <ul>
                  {renderNav && renderNav()}
                  {!noNavOptions && role === "user" && (
                    <>
                      <li className="d-none d-sm-inline-block">
                        <Link href="/categories">
                          <a style={{ marginLeft: 0 }}>
                            <span style={{ color: colors.font1 }}>
                              دسته‌بندی ها
                            </span>
                          </a>
                        </Link>
                      </li>
                      <li className="d-none d-sm-inline-block">
                        <Link href="/saved_news">
                          <a style={{ marginLeft: 0 }}>
                            <span style={{ color: colors.font1 }}>
                              اخبار اختصاصی شما
                            </span>
                          </a>
                        </Link>
                      </li>
                      <li onClick={() => changeColor()} title="روز و شب">
                        <span
                          className="icon-adjust"
                          style={{ color: colors.font1 }}
                        ></span>
                      </li>
                      <li className="dropDownMenu">
                        <span
                          className="las la-ellipsis-v"
                          onClick={() => setOpen(!open)}
                          ref={btn}
                          style={{
                            color: colors.font1,
                          }}
                        ></span>
                        {open && (
                          <div
                            className="dropDownMenuContainer"
                            ref={container}
                            style={{
                              backgroundColor: colors.aside1,
                            }}
                          >
                            <Link href="/profile">
                              <a
                                onClick={() => dispatch(changeActiveProfile(1))}
                                style={{ color: colors.font3 }}
                              >
                                اطلاعات کاربری
                              </a>
                            </Link>
                            <Link href="/profile">
                              <a
                                style={{ color: colors.font3 }}
                                onClick={() => dispatch(changeActiveProfile(2))}
                              >
                                تراکنش‌های شما
                              </a>
                            </Link>
                            <Link href={"/packages"}>
                              <a style={{ color: colors.font3 }}>
                                تمدید اشتراک
                              </a>
                            </Link>
                            <Link href="/categories">
                              <a
                                className="d-sm-none"
                                style={{ marginLeft: 0 }}
                              >
                                <span style={{ color: colors.font1 }}>
                                  دسته‌بندی ها
                                </span>
                              </a>
                            </Link>
                            <Link href="/saved_news">
                              <a
                                className="d-sm-none"
                                style={{ marginLeft: 0 }}
                              >
                                <span style={{ color: colors.font1 }}>
                                  اخبار اختصاصی شما
                                </span>
                              </a>
                            </Link>
                            <div
                              className="logoutBtn"
                              onClick={() => dispatch(logout())}
                              style={{ color: colors.font3 }}
                            >
                              خروج
                            </div>
                          </div>
                        )}
                      </li>
                    </>
                  )}
                </ul>
              </nav>
            </div>
          </div>
        </div>
        {!noRecommend && role === "user" && (
          <div
            className="recommendTopNews d-none d-md-block"
            style={{
              border: `1px solid ${colors.border1}`,
              backgroundColor: colors.main4,
            }}
          >
            <div className="container">
              <div className="recommendTopNewsBox">
                <div className="recommendTopNewsRight">
                  <p style={{ color: colors.font3 }}>
                    {header_news.length !== 0 && header_news[0].summary.trim()}
                  </p>
                  <Link
                    href={`/news/[id]`}
                    as={`/news/${
                      header_news.length !== 0
                        ? header_news[0].mongo_id || header_news[0].id
                        : ""
                    }`}
                  >
                    <a>بیشتر بخوانید</a>
                  </Link>
                </div>
                <div className="recommendTopNewsLeft"></div>
              </div>
            </div>
          </div>
        )}
      </MainHeader>
    </>
  );
};

const flexRow1: string = `
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const flex: string = `
  display: flex;
  align-items: center;
`;

const MainHeader = styled.header`
  > .mainTopHead {
    padding: 10px 0;
    > .container {
      > .topHeaderContainer {
        ${flexRow1}
        @media (max-width: 576px) {
          justify-content: center;
        }
        > .rightTopHead {
          ${flex}
          > .rightTopHeadTitle {
            margin-left: 30px;
            ${flex}
            > .icon-headphones {
              font-size: 1rem;
              margin-left: 5px;
            }
            > span {
              color: #fff;
              font-weight: 500;
              font-size: 0.85rem;
              ${flex}
            }
          }
          > .rightTopHeadDate {
            color: #fff;
            font-size: 0.85rem;
            font-weight: 500;
          }
        }
        > .leftTopHead {
          ${flex}
          > .leftTopHeadItem {
            margin-right: 15px;
            color: #fff;
            font-size: 0.85rem;
            font-weight: 200;
            line-height: 14px;
            cursor: pointer;
            letter-spacing: -0.5px;
          }
        }
      }
    }
  }
  > .bottomHeaderContainer {
    box-shadow: 0px 0px 7px rgba(94, 94, 94, 0.135134);
    .bottomHeader {
      ${flexRow1}
      height: 90px;
      background-repeat: no-repeat;
      background-position: 97% center;
      @media only screen and (max-width: 575px) {
        background-position: 80% center;
      }
      .rightBottomHead {
        span {
          margin-left: 15px;
        }
      }
      img {
        vertical-align: middle;
      }
      .icon-miz-logo {
        font-size: 1.5rem;
        margin-left: 10px;
      }
      .icon-miz-typo {
        font-size: 1.3rem;
      }
      .leftBottomHead {
        ul {
          ${flex}
          li {
            ${flex}
            cursor: pointer;
            span,
            a {
              ${flex}
              color: #ffffff;
              font-size: 0.9rem;
              margin-left: 15px;
            }
            .icon-adjust,
            .la-ellipsis-v {
              font-size: 1.2rem;
              margin-left: 5px;
            }
            .icon-menu {
              font-size: 1.4rem;
            }
            .la-ellipsis-v {
              font-size: 1.6rem;
            }
          }
        }
      }
    }
  }
  > .recommendTopNews {
    .recommendTopNewsBox {
      .recommendTopNewsRight {
        ${flex}
        margin: 10px 0;
        p {
          font-size: 0.85rem;
          white-space: nowrap;
          max-width: 700px;
          overflow: hidden;
          text-overflow: ellipsis;
          @media only screen and (max-width: 1199px) {
            max-width: 500px;
          }
          @media only screen and (max-width: 991px) {
            max-width: 300px;
          }
        }
        a {
          color: ${mainColors.blue};
          font-size: 0.85rem;
          font-weight: 500;
          margin-right: 15px;
        }
      }
    }
  }
  .dropDownMenu {
    position: relative;
    .dropDownMenuContainer {
      box-shadow: 0px 0px 7px rgba(94, 94, 94, 0.135134);
      position: absolute;
      z-index: 100;
      top: calc(110% + 10px);
      left: 0;
      width: 180px;
      a,
      .logoutBtn {
        font-size: 0.85rem !important;
        padding: 10px;
        display: block;
        width: 100%;
        margin-left: 0 !important;
        text-align: center !important;
        justify-content: center;
        &:hover {
          background-color: rgba(0, 0, 0, 0.03);
        }
      }
    }
  }
`;

export default connect((state) => state, { changeColor })(Header);
