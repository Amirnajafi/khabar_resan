import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import Aside from "../components/Aside";
import NewsList from "../components/NewsList";
import Layout from "../components/layout/Layout";

const Home = () => {
  return (
    <>
      <Layout>
        <Main>
          <div className="container">
            <div className="row">
              <aside className="col-12 col-xl-3">
                <Aside />
              </aside>
              <section className="col-12 col-xl-9">
                <NewsList />
              </section>
            </div>
          </div>
        </Main>
      </Layout>
    </>
  );
};

const Main = styled.main`
  margin: 30px 0;
`;

export default connect((state) => state)(Home);
