import * as React from "react";
import { mount } from "enzyme";

import "jsdom-global/register";

test("text in p should be Hello Jest!", () => {
  const wrapper = mount(<p>Hello Jest!</p>);
  expect(wrapper.text()).toMatch("Hello Jest!");
});
