import React, { Fragment } from "react";
import styled from "styled-components";
import Head from "next/head";
import { connect } from "react-redux";
import { defaultColors } from "../../redux/reducer/colors/colorReducer";
import { flexRow1, flex } from "../../public/styles/modules";
import Carousel from "react-multi-carousel";
import Footer from "../layout/Footer";
import moment from "jalali-moment";
import { toPersian, trunc } from "../../helpers/props";
import Link from "next/link";

export const LandingNewsContent = (props) => {
  const { style, title, description, date, id } = props;

  return (
    <>
      <Content style={style}>
        <Link href="/news/[id]" as={`/news/${id}`}>
          <a className="landingNewsContentBox d-flex">
            <h4>{title}</h4>
            <p>{description}</p>
            <div className="landingNewsContentDate">{date}</div>
          </a>
        </Link>
      </Content>
    </>
  );
};

export const LandingNewsContent2 = (props) => {
  const { title, description, id } = props;

  return (
    <>
      <Link href="/news/[id]" as={`/news/${id}`}>
        <a>
          <Content2>
            <h4>جدیدترین در میزخبر</h4>
            <div className="landingNewsContent2Box">
              <h3>{title}</h3>
              <p>{description}</p>
            </div>
          </Content2>
        </a>
      </Link>
    </>
  );
};

export const LandingMaskBox = (props) => {
  const { dir, url, text, maskUrl, title } = props;

  return (
    <section className="landingMask">
      <div className="landingMaskImg">
        <div
          className="landingMaskMatchImg"
          style={{ justifyContent: dir === "ltr" ? "flex-start" : "flex-end" }}
        >
          <div className="landingMaskMatchImgEl1">
            <div
              className={`landingMaskContent`}
              style={dir === "rtl" ? { right: "-140px" } : { left: "-200px" }}
            >
              <h3>{title}</h3>
              <p>{text}</p>
            </div>
            <img src={url} />
          </div>
          <img
            src={maskUrl}
            className="landingMaskMatchImgEl2"
            style={dir === "ltr" ? { right: 0 } : { left: 0 }}
          />
        </div>
      </div>
    </section>
  );
};

export const MainNewsComponent = (props) => {
  const { title, date, id } = props;

  return (
    <Fragment>
      <Link href="/news/[id]" as={`/news/${id}`}>
        <a>
          <MainNewsItem {...props}>
            <div className="imgMainNewsItem"></div>
            <div className="contentMainNewsItem">
              <h2>{trunc(title, 50)}</h2>
              <div className="dateMainNewsItem">
                <span>{date}</span>
              </div>
            </div>
          </MainNewsItem>
        </a>
      </Link>
    </Fragment>
  );
};

const Landing = (props) => {
  const { home } = props;

  const responsive = {
    desktop: {
      breakpoint: { max: 10000, min: 1024 },
      items: 9,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 6,
    },
    mobile: {
      breakpoint: { max: 767, min: 0 },
      items: 3,
    },
  };

  const getItemDetails = (index) => {
    if (home.news && home.news.length !== 0) {
      const _item = home.news[index];
      const { id, title, summary, create_date, image } = _item;
      const date = toPersian(
        moment(create_date, "YYYY-MM-DD hh:mm:ss")
          .locale("fa")
          .format("DD MMMM YYYY")
      );

      return { _item, id, title, summary, date, image };
    }
    return {};
  };

  return (
    <>
      <Head>
        <style>
          {`
          body {
            background-color: #000 !important;
            background-image: url(/img/landing/bg-1.jpg);
            background-repeat: no-repeat;
            background-position: center top;
          }
          * {
            color: #fff;
          }
        `}
        </style>
      </Head>
      <Main>
        <header className="landingHeader">
          <div className="container">
            <div className="landingHeaderButtons">
              <h1>
                <img src={"/img/landing/miz-khabar-logo.png"} alt="میز خبر" />
              </h1>
              <nav className="landingLastSectionButtons">
                <Link href="/login">
                  <a className="ladingBottomBtn1">
                    <span className="la la-arrow-left"></span>
                    <span>ورود/ثبت‌نام</span>
                  </a>
                </Link>
                <Link href="/packages">
                  <a className="ladingBottomBtn2">
                    <span>نمایش پلن ها</span>
                    <span className="la la-arrow-right"></span>
                  </a>
                </Link>
              </nav>
            </div>
          </div>
        </header>
        <section className="landingPaper">
          <div className="container">
            <section className="row mb30 d-none d-xl-flex">
              {[0, 1, 2].map((item) => {
                const { id } = getItemDetails(item);
                const key = `landingPageNewsTop${item}${id}`;
                return (
                  <div className="col-12 col-lg-4" key={key}>
                    <div className="landingNewsContent">
                      <MainNewsComponent {...getItemDetails(item)} />
                    </div>
                  </div>
                );
              })}
            </section>
            <section className="row mb30">
              <div className="col-12 col-lg-8">
                <div className="landingNewsMainSection">
                  <p>
                    میز خبر با قابلیت ایجاد روزنامه اختصاصی برای هر کاربر،مزیتی
                    چشمگیر نسبت به رقبای خود دارد. کاربران با انتخاب کلید واژه
                    خبری و همچنین انتخاب خبرگزاری دلخواه خود می توانند
                    تنها،اخبار مورد نیازشان را مشاهده کنند.
                  </p>
                </div>
              </div>
              <div className="col-12 col-lg-4">
                <div className="landingNewsImgBox">
                  <MainNewsComponent {...getItemDetails(3)} />
                </div>
              </div>
            </section>
            <div className="row">
              <div className="col-12 col-lg-8">
                <div className="landingMainSection">
                  <img src="/img/landing/bg-2.png" alt="اخبار اختصاصی" />
                  <h2>اخبار اختصاصی خودتون رو بسازید</h2>
                </div>
              </div>
              <aside className="col-12 col-lg-4">
                <Link href="/news/[id]" as={`/news/${getItemDetails(4).id}`}>
                  <div className="asideNewsImg" style={{ cursor: "pointer" }}>
                    <div
                      className="asideImgBox"
                      style={{
                        backgroundImage: `url(${getItemDetails(4).image})`,
                      }}
                    ></div>
                    <div className="asideNewsImgContent">
                      <h2>{getItemDetails(4).title}</h2>
                      <div className="dateMainNewsItem">
                        <span>{getItemDetails(4).date}</span>
                      </div>
                    </div>
                  </div>
                </Link>
                <MainNewsComponent {...getItemDetails(5)} />
                <MainNewsComponent {...getItemDetails(6)} />
                <MainNewsComponent {...getItemDetails(7)} />
                <MainNewsComponent {...getItemDetails(8)} />
              </aside>
              <div className="col-12">
                <div className="landingSourcesBox">
                  <h4>
                    اخبار اختصاصی شما جمع‌آوری شده از بیش از ۱۰۰ خبرگزاری معتبر
                  </h4>
                  <div dir="ltr" className="landingSourcesContent">
                    <Carousel
                      responsive={responsive}
                      ssr
                      showDots={false}
                      arrows={false}
                      slidesToSlide={1}
                      infinite
                      className="landingNewsCarousel"
                      containerClass="container-with-dots"
                      itemClass="image-item"
                      deviceType={""}
                      autoPlay={true}
                    >
                      {home.sources.map((item, index) => {
                        const key = `${item.id}${index}`;
                        return (
                          <img
                            src={item.logo}
                            key={key}
                            style={{ width: 70 }}
                          />
                        );
                      })}
                    </Carousel>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <LandingMaskBox
          dir="ltr"
          title="دسترسی به تمامی خبرگزاری ها"
          maskUrl={"/img/landing/mask1-2.png"}
          url={"/img/landing/mask2.jpg"}
          text="امکان انتخاب خبرگزاری دلخواه از میان معتبرترین خبرگزاری های کشور"
        />
        <LandingMaskBox
          dir="rtl"
          title="تعریف کلیدواژه خبری"
          maskUrl={"/img/landing/mask2-2.png"}
          url={"/img/landing/mask2.jpg"}
          text="کاربران با وارد کردن کلیدواژه های مورد نیازشان، اخبار مرتبط با همان کلیدواژه را مشاهده می کنند"
        />
        <LandingMaskBox
          dir="ltr"
          title="سهولت استفاده"
          maskUrl={"/img/landing/mask1-2.png"}
          url={"/img/landing/mask2.jpg"}
          text="پیاده سازی با بروزترین و بهینه ترین تکنولوژی های نرم افزاری"
        />
        <section className="landingLastSection">
          <h3>با میز خبر روزنامه اختصاصی خودت را داشته باش</h3>
          <p>
            میز خبر محصولی از هلدینگ دانش بنیان آریانا، یک اپلیکیشن تحت وب برای
            دسترسی آسان به خبرهای روز دنیا است که برای راحتی کاربران امکان ایجاد
            روزنامه اختصاصی را فراهم کرده است. کاربران می توانند خبرهای
            دلخواهشان را از معتبرترین خبرگزاری ها انتخاب و بر اساس آن اخبار مورد
            نیازشان را دنبال کنند.
          </p>
          <div className="landingLastSectionButtons">
            <Link href="/login">
              <a className="ladingBottomBtn1">
                <span className="la la-arrow-left"></span>
                <span>ورود/ثبت‌نام</span>
              </a>
            </Link>
            <Link href="/packages">
              <a className="ladingBottomBtn2">
                <span>نمایش پلن ها</span>
                <span className="la la-arrow-right"></span>
              </a>
            </Link>
          </div>
        </section>
      </Main>
      <Footer noTop darkOnly />
    </>
  );
};

/*
  .mb30 {
    margin-bottom: 30px;
  }
*/

const Main = styled.main`
  margin: 150px 0;
  @media only screen and (max-width: 991px) {
    margin: 30px 0 !important;
  }
  .asideNewsImgContent {
    flex: 1;
    display: "flex";
    flexdirection: "column";
    justifycontent: "space-between";
    background: rgba(21, 36, 74, 0.3);
    h2 {
      font-style: normal;
      font-weight: bold;
      font-size: 12px;
      flex: 1;
      padding: 13px;
      text-align: justify;
    }
    .dateMainNewsItem {
      height: 30px;
      padding: 0 13px;
      display: flex;
      justify-content: flex-end;
      align-items: center;
      border-top: 1px solid rgba(61, 120, 249, 0.199519);
      span {
        font-size: 11px;
        line-height: 13px;
        color: #ffffff;
      }
    }
  }
  .landingNewsMainSection {
    ${flexRow1};
    background: rgba(21, 36, 74, 0.3);
    height: 110px;
    @media only screen and (max-width: 991px) {
      margin-bottom: 30px;
    }
    @media only screen and (max-width: 767px) {
      flex-direction: column;
      height: auto !important;
    }
    p {
      font-size: 0.8rem;
      margin: 30px;
      text-align: justify;
    }
  }
  .landingHeaderButtons {
    display: flex;
    justify-content: space-between;
    margin-bottom: 50px;
    @media only screen and (max-width: 767px) {
      flex-direction: column;
      align-items: center;
      h1 {
        margin-bottom: 30px;
      }
    }
  }
  .landingNewsImgBox {
    .landingNewsImgContainer {
      width: 80px;
      height: inherit;
    }
  }
  .asideNewsImg {
    margin-bottom: 30px;
    .asideImgBox {
      height: 200px;
      width: 100%;
      background-size: cover;
      background-position: center;
    }
  }
  .landingMainSection {
    ${flex}
    margin-bottom: 30px;
    justify-content: center;
    flex-direction: column;
    @media only screen and (max-width: 991px) {
      img {
        max-width: 100%;
      }
      h2 {
        margin-bottom: 30px;
      }
    }
    @media only screen and (max-width: 767px) {
      h2 {
        font-size: 1.5rem !important;
        margin-top: 0 !important;
      }
    }
    h2 {
      font-size: 2.7rem;
      margin-top: -20px;
    }
  }
  .landingSourcesBox {
    margin-bottom: 160px;
    @media only screen and (max-width: 500px) {
      h4 {
        font-size: 0.8rem !important;
        text-align: center;
        padding: 0 5px;
        height: 50px !important;
      }
    }
    h4 {
      ${flex}
      justify-content: center;
      font-size: 0.9rem;
      height: 40px;
      color: #fff;
      background-color: ${defaultColors.blue};
    }
    .landingSourcesContent {
      height: 100px;
      background: #fff;
    }
  }
  .landingNewsCarousel {
    padding: 13px 20px 0;
  }
  .landingMask {
    position: relative;
    @media only screen and (max-width: 1275px) {
      margin-bottom: 30px;
    }
    .landingMaskMatchImgEl1 {
      position: relative;
    }
    .landingMaskMatchImg {
      ${flex}
      @media only screen and (max-width: 1275px) {
        justify-content: center !important;
        padding: 0 30px;
      }
      position: relative;
      img {
        max-width: 100%;
      }
      .landingMaskMatchImgEl2 {
        position: absolute;
        top: 0;
        z-index: 10;
        @media only screen and (max-width: 1275px) {
          display: none;
        }
      }
    }
    .landingMaskContent {
      ${flex};
      justify-content: center;
      flex-direction: column;
      align-items: flex-start;
      z-index: 100000;
      max-width: 338px;
      position: absolute;
      top: 150px;
      height: 100%;
      @media only screen and (max-width: 1275px) {
        max-width: 100%;
        position: relative;
        top: initial;
        height: initial;
        margin-bottom: 15px;
        align-items: flex-start;
        right: initial !important;
        left: initial !important;
        h3,
        p {
          text-align: right;
        }
      }
      @media only screen and (max-width: 500px) {
        h3 {
          font-size: 1rem !important;
        }
      }
      h3 {
        font-size: 1.2rem;
        margin-bottom: 10px;
      }
      p {
        font-size: 0.85rem;
      }
    }
  }
  .landingLastSectionButtons {
    ${flex}
    @media only screen and (max-width: 500px) {
      flex-direction: column;
      width: 100%;
      a {
        width: 100% !important;
        margin: 0 0 15px !important;
      }
    }
    .ladingBottomBtn1 {
      background-color: ${defaultColors.blue4};
    }
    .ladingBottomBtn2 {
      background-color: ${defaultColors.blue};
    }
    a {
      ${flexRow1}
      padding: 0 15px;
      border-radius: 4px;
      width: 207px;
      height: 45px;
      span {
        color: #fff;
      }
      .la {
        ${flex}
        font-size: 1.6rem;
      }
    }
    a + a {
      margin-right: 10px;
    }
  }
  .landingLastSection {
    ${flex}
    justify-content: center;
    flex-direction: column;
    padding: 0 30px;
    margin: 190px auto 160px;
    @media only screen and (max-width: 991px) {
      margin: 30px auto;
    }
    max-width: 570px;
    h3 {
      text-align: center;
      margin-bottom: 5px;
      font-size: 1.3rem;
    }
    p {
      font-size: 0.75rem;
      text-align: center;
      margin-bottom: 30px;
    }
  }
`;

const Content2 = styled.div`
  margin-bottom: 30px;
  h4 {
    ${flex}
    justify-content: center;
    font-size: 0.9rem;
    height: 40px;
    color: #fff;
    background-color: ${defaultColors.blue};
  }
  .landingNewsContent2Box {
    background-color: ${defaultColors.blue3};
    padding: 20px;
    h3 {
      font-size: 1.2rem;
      margin-bottom: 5px;
    }
    p {
      font-size: 0.75rem;
      line-height: 2;
    }
  }
`;

const Content = styled.div`
  background-color: ${defaultColors.blue2};
  position: relative;
  flex: 1;
  height: 110px;
  border-right: 6px solid ${defaultColors.blue};
  .landingNewsContentBox {
    padding-top: 10px;
    height: inherit;
    display: flex;
    flex-direction: column;
    h4 {
      font-size: 0.83rem;
      padding: 0 10px;
    }
    p {
      font-size: 0.8rem;
      padding: 0 10px;
      flex: 1;
    }
    .landingNewsContentDate {
      display: flex;
      border-top: 1px solid rgba(61, 120, 249, 0.199519);
      margin-top: 5px;
      align-items: center;
      justify-content: flex-end;
      font-size: 0.8rem;
      height: 28px;
      padding: 0 10px;
    }
  }
`;

const MainNewsItem = styled.div((props) => {
  const { image } = props;
  return {
    height: 110,
    display: "flex",
    background: "rgba(55, 78, 136, 0.3)",
    justifyContent: "space-between",
    marginBottom: 30,
    ".imgMainNewsItem": {
      width: 120,
      backgroundImage: `url(${image})`,
      backgroundSize: "cover",
      backgroundPosition: "center",
    },
    ".contentMainNewsItem": {
      flex: 1,
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-between",
      h2: {
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: 12,
        flex: 1,
        padding: 13,
        textAlign: "justify",
      },
      ".dateMainNewsItem": {
        height: 30,
        padding: "0 13px",
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center",
        borderTop: "1px solid rgba(61, 120, 249, 0.199519)",
        span: {
          fontSize: "11px",
          lineHeight: "13px",
          color: "#FFFFFF",
        },
      },
    },
  };
});

export default connect((state) => state)(Landing);
