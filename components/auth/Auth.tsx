import React, { useEffect } from "react";
import styled from "styled-components";
import { connect, useDispatch, useSelector } from "react-redux";
import { defaultColors } from "../../redux/reducer/colors/colorReducer";
import AuthBox from "./AuthBox";
import AuthForm from "./AuthForm";
import Button from "../reuse/Button";
import Head from "next/head";
import Link from "next/link";
import { handleForgot, resetCodeMode, resendCode } from "../../redux/actions";
import useTimer from "../../hooks/useTimer";
import { useRouter, Router } from "next/router";

export const HeaderBox = (props) => {
  const { title, description } = props;

  return (
    <>
      <header className="authBoxHeader">
        <h2>{title}</h2>
        <p>{description}</p>
      </header>
    </>
  );
};

const Timer = () => {
  const [time, isFinish, reset] = useTimer();
  const dispatch = useDispatch();

  return (
    <button
      type="button"
      onClick={() => {
        if (isFinish) {
          reset();
          dispatch(resendCode(() => {}));
        }
      }}
    >
      ارسال مجدد کد {time > 0 ? `: ${time}` : ""}
    </button>
  );
};

export const FooterBox = (props) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { auth } = useSelector(({ auth }) => ({ auth }));
  const { top, bottom } = props;

  return (
    <footer className="authBoxBottom">
      {router.pathname === "/login" ? (
        <>
          {auth.forgot_mode === "code" &&
            top.title !== "نمایش پلن های اشتراک" && <Timer />}
        </>
      ) : (
        <>{auth.code === "register" && <Timer />}</>
      )}
      {top.url === "forgot_pass" ? (
        <>
          <button
            type="button"
            onClick={() => {
              if (router.pathname !== "/login") {
                router.push("/login").then(() => {
                  dispatch(handleForgot());
                });
              } else {
                dispatch(handleForgot());
              }
            }}
          >
            {top.title}
          </button>
        </>
      ) : (
        <Link href={top.url}>
          <a
            onClick={() => {
              if (top.title === "بازگشت") {
                dispatch(resetCodeMode());
              }
            }}
          >
            {top.title}
          </a>
        </Link>
      )}
      {bottom && (
        <Link href={bottom.url}>
          <a>{bottom.title}</a>
        </Link>
      )}
    </footer>
  );
};

const Auth = (props) => {
  const {
    handleValidate,
    handleSubmit,
    initialValues,
    isRegister,
    validationSchema,
  } = props;

  const dispatch = useDispatch();
  const { auth } = useSelector((state) => state);

  useEffect(() => {
    return () => {
      dispatch(resetCodeMode());
    };
  }, []);

  return (
    <>
      <Head>
        <style>
          {`
            body {
              background-color: #181818 !important;
            }

            @media only screen and (max-width: 1050px) {
              body {
                background-color: rgb(33, 33, 33) !important;
              }
            }
          `}
        </style>
      </Head>
      <Box>
        <div
          className="authParent"
          style={{ flexDirection: isRegister ? "row-reverse" : "row" }}
        >
          <AuthBox
            isActive={!isRegister}
            style={isRegister ? { order: 10 } : {}}
          >
            <HeaderBox
              title={
                isRegister
                  ? "اگر در صورت داشتن حساب کاربری، وارد شوید"
                  : "ورود از طریق شماره تلفن همراه"
              }
              description={
                isRegister
                  ? "در غیر این صورت میتوانید بر روی گزینه ثبت‌نام زده و ثبت‌نام کنید"
                  : "بعد از ثبت نام می‌توانید تنها با وارد کردن شماره همراه خود وارد سایت شوید."
              }
            />
            {isRegister ? (
              <>
                <Link href="/login">
                  <a>
                    <Button className="btnRegisterRecomment">ورود</Button>
                  </a>
                </Link>
              </>
            ) : (
              <main className="authBoxMain">
                <AuthForm
                  handleValidate={handleValidate}
                  handleSubmit={handleSubmit}
                  initialValues={initialValues}
                  validationSchema={validationSchema}
                />
              </main>
            )}
            {!isRegister && (
              <>
                {!auth.code ? (
                  <FooterBox
                    top={{
                      title: "رمز عبور خود را فراموش کرده ام",
                      url: "forgot_pass",
                    }}
                    // bottom={{ title: "ورود از طریق ایمیل", url: "#" }}
                  />
                ) : (
                  <>
                    <FooterBox
                      top={{
                        title: "بازگشت",
                        url: "login",
                      }}
                      // bottom={{ title: "ورود از طریق ایمیل", url: "#" }}
                    />
                  </>
                )}
              </>
            )}
          </AuthBox>
          <AuthBox isActive={isRegister}>
            <HeaderBox
              title="اگر حساب کاربری ندارید، ثبت نام کنید"
              description="بعد از ثبت نام حتما باید اشتراک خریداری کنید"
            />
            {isRegister ? (
              <main className="authBoxMain">
                <AuthForm
                  handleValidate={handleValidate}
                  handleSubmit={handleSubmit}
                  initialValues={initialValues}
                  isRegister={isRegister}
                  validationSchema={validationSchema}
                />
              </main>
            ) : (
              <>
                <Link href="/register">
                  <a>
                    <Button className="btnRegisterRecomment">ثبت نام</Button>
                  </a>
                </Link>
              </>
            )}
            {!isRegister || !auth.code ? (
              <FooterBox
                bottom={{ title: "نمایش پلن های اشتراک", url: "/packages" }}
                top={{
                  title: "رمز عبور را فراموش کرده‌ام",
                  url: "forgot_pass",
                }}
              />
            ) : (
              <FooterBox
                top={{ title: "بازگشت", url: "/register" }}
                // bottom={{ title: "پشتیبانی سایت", url: "#" }}
              />
            )}
          </AuthBox>
        </div>
      </Box>
    </>
  );
};

const flex: string = `
  display: flex;
  align-items: center;
`;

const Box = styled.div`
  .authParent {
    ${flex}
    justify-content: center;
    padding: 0 30px;
    @media only screen and (max-width: 1050px) {
      flex-direction: column !important;
      padding: 0 0 !important;
      width: 100%;
    }
  }
  .authBoxHeader {
    ${flex}
    flex-direction: column;
    justify-content: center;
    margin-bottom: 30px;
    h2,
    p {
      color: #fff;
    }
    h2 {
      margin-bottom: 5px;
      font-size: 1.1rem;
    }
    p {
      font-size: 0.75rem;
    }
  }
  .authBoxMain {
    padding: 0 30px;
    margin-bottom: 20px;
    width: 100%;
    @media only screen and (max-width: 500px) {
      padding: 0 0 !important;
    }
  }
  .authBoxBottom {
    ${flex}
    flex-direction: column;
    a,
    button {
      background-color: transparent;
      color: ${defaultColors.blue};
      font-size: 0.9rem;
      margin-bottom: 15px;
    }
  }
  .btnRegisterRecomment {
    margin-bottom: 30px;
  }
`;

export default connect((state) => state)(Auth);
