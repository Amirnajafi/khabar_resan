import React from "react";
import { Formik } from "formik";
import {
  defaultColors,
  mainColors,
} from "../../redux/reducer/colors/colorReducer";
import { connect, useDispatch } from "react-redux";
import styled from "styled-components";
import { toPersian, setIfNumber } from "../../helpers/props";
import { confirmCode, forgotPass, handleResetPass } from "../../redux/actions";
import * as Yup from "yup";

const newPass = Yup.object().shape({
  code: Yup.string().required("پر کردن کادر کد اجباری می‌باشد"),
  password: Yup.string()
    .min(8, "حداقل ۸ کاراکتر وارد کنید")
    .max(40, "بیشتر از ۴۰ کاراکتر مجاز نیست")
    .required("رمز عبور خود را وارد کنید"),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref("password"), null], "رمزعبور مطابقت ندارد")
    .required("تکرار رمزعبور اجباری می‌باشد"),
});

const codeSchema = Yup.object().shape({
  code: Yup.string().required("پر کردن کادر کد اجباری می‌باشد"),
});

const initialCode = {
  code: "",
};

const AuthForm = (props) => {
  const {
    initialValues,
    handleSubmit,
    handleValidate,
    isRegister,
    validationSchema,
    auth,
    confirmCode,
  } = props;

  const { key, role, code, mobile } = auth;
  const dispatch = useDispatch();

  const renderMainForm = () => {
    return (
      <Formik
        initialValues={initialValues}
        validate={handleValidate}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue,
        }) => {
          const mobileErr = errors.mobile && touched.mobile;
          const passErr = errors.password && touched.password;
          const confirmErr = errors.confirmPassword && touched.confirmPassword;

          return (
            <Form
              onSubmit={handleSubmit}
              className="newsLoginForm"
              autocomplete="off"
            >
              <label
                htmlFor="loginMobile"
                style={{
                  color: mobileErr ? defaultColors.danger2 : "#fff",
                }}
              >
                {mobileErr ? errors.mobile : "شماره همراه"}
              </label>
              <div
                className="loginPhoneInput"
                style={{
                  borderColor: mobileErr ? defaultColors.danger2 : "#545454",
                }}
              >
                <input
                  type="mobile"
                  name="mobile"
                  id="loginMobile"
                  autoComplete="off"
                  onChange={(e) => setIfNumber(e, setFieldValue, 11)}
                  onBlur={handleBlur}
                  value={toPersian(values.mobile)}
                />
                {/* <span>
                <span>|</span>
                {`(+۹۸) ایران`}
              </span> */}
              </div>
              <>
                <label
                  htmlFor="loginPass"
                  style={{
                    color: passErr ? defaultColors.danger2 : "#fff",
                  }}
                >
                  {passErr ? errors.password : "رمز عبور"}
                </label>
                <input
                  type="password"
                  className="mainInput"
                  name="password"
                  autoComplete="off"
                  id="loginPass"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                  style={{
                    borderColor: passErr ? defaultColors.danger2 : "#545454",
                    marginBottom: 15,
                  }}
                />

                {isRegister && (
                  <>
                    <label
                      htmlFor="confirmPassword"
                      style={{
                        color: confirmErr ? defaultColors.danger2 : "#fff",
                      }}
                    >
                      {confirmErr ? errors.confirmPassword : "تکرار رمز عبور"}
                    </label>
                    <input
                      type="password"
                      className="mainInput"
                      name="confirmPassword"
                      autoComplete="off"
                      id="confirmPassword"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.confirmPassword}
                      style={{
                        borderColor: confirmErr
                          ? defaultColors.danger2
                          : "#545454",
                      }}
                    />
                  </>
                )}
              </>
              <button type="submit" disabled={isSubmitting}>
                <span className="buttonLoginSubmit">
                  {isRegister ? "ادامه مراحل ثبت‌نام" : "ورود"}
                </span>
                <span className="la la-arrow-left"></span>
              </button>
            </Form>
          );
        }}
      </Formik>
    );
  };

  const renderCodeForm = () => {
    return (
      <Formik
        initialValues={initialCode}
        onSubmit={async (values, actions) => {
          const { setSubmitting, setErrors } = actions;
          setSubmitting(true);
          await confirmCode(values.code, () => {
            setErrors({ code: "کد وارد شده صحیح نیست" });
          });
          setSubmitting(false);
        }}
        validationSchema={codeSchema}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue,
        }) => {
          const codeErr = errors.code && touched.code;

          return (
            <Form
              onSubmit={handleSubmit}
              className="newsLoginForm"
              autocomplete="off"
            >
              <label
                htmlFor="code"
                style={{
                  color: codeErr ? defaultColors.danger2 : "#fff",
                }}
              >
                {codeErr
                  ? errors.code
                  : `کد ارسال شده به ۰${toPersian(mobile)} را وارد کنید`}
              </label>
              <div
                className="loginPhoneInput"
                style={{
                  borderColor: codeErr ? defaultColors.danger2 : "#545454",
                }}
              >
                <input
                  type="text"
                  name="code"
                  id="code"
                  style={{ textAlign: "center" }}
                  autoComplete="off"
                  onChange={(e) => setIfNumber(e, setFieldValue, 6)}
                  onBlur={handleBlur}
                  value={values.code ? toPersian(values.code) : ""}
                />
              </div>
              <button type="submit" disabled={isSubmitting}>
                <span className="buttonLoginSubmit">تائید کد و ثبت‌نام</span>
                <span className="la la-arrow-left"></span>
              </button>
            </Form>
          );
        }}
      </Formik>
    );
  };

  const renderMobileForm = () => {
    return (
      <Formik
        initialValues={{ mobile: "" }}
        validate={({ mobile }) => {
          const errors: any = {};
          if (!mobile.trim()) {
            errors.mobile = "شماره موبایل اجباری می‌باشد";
          } else if (mobile.length < 7) {
            errors.mobile = "شماره موبایل کوتاه نمی‌باشد";
          }
          return errors;
        }}
        onSubmit={async ({ mobile }, actions) => {
          const { setSubmitting, setErrors } = actions;

          setSubmitting(true);
          await dispatch(
            forgotPass(mobile, (err) => {
              // handle err ...
              if (err.response.status === 401) {
                setErrors({ mobile: "این شماره ثبت نام نکرده است" });
              }
            })
          );
          setSubmitting(false);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue,
        }) => {
          const mobileErr = errors.mobile && touched.mobile;

          return (
            <Form
              onSubmit={handleSubmit}
              className="newsLoginForm"
              autocomplete="off"
            >
              <label
                htmlFor="loginMobile"
                style={{
                  color: mobileErr ? defaultColors.danger2 : "#fff",
                }}
              >
                {mobileErr ? errors.mobile : "شماره همراه"}
              </label>
              <div
                className="loginPhoneInput"
                style={{
                  borderColor: mobileErr ? defaultColors.danger2 : "#545454",
                }}
              >
                <input
                  type="mobile"
                  name="mobile"
                  id="loginMobile"
                  autoComplete="off"
                  onChange={(e) => setIfNumber(e, setFieldValue, 11)}
                  onBlur={handleBlur}
                  value={toPersian(values.mobile)}
                />
              </div>
              <button type="submit" disabled={isSubmitting}>
                <span className="buttonLoginSubmit">
                  {isRegister ? "ادامه مراحل ثبت‌نام" : "ورود"}
                </span>
                <span className="la la-arrow-left"></span>
              </button>
            </Form>
          );
        }}
      </Formik>
    );
  };

  const renderForgotCodeForm = () => {
    return (
      <Formik
        initialValues={{ code: "", password: "", confirmPassword: "" }}
        onSubmit={async (values, actions) => {
          const { setSubmitting, setErrors } = actions;
          setSubmitting(true);
          await dispatch(
            handleResetPass(
              { activation_code: values.code, new_password: values.password },
              () => {
                setErrors({ code: "کد وارد شده صحیح نیست" });
              }
            )
          );
          setSubmitting(false);
        }}
        validationSchema={newPass}
      >
        {({
          values,
          errors,
          touched,
          handleBlur,
          handleSubmit,
          handleChange,
          isSubmitting,
          setFieldValue,
        }) => {
          const codeErr = errors.code && touched.code;
          const passErr = errors.password && touched.password;
          const confirmErr = errors.confirmPassword && touched.confirmPassword;

          return (
            <Form
              onSubmit={handleSubmit}
              className="newsLoginForm"
              autocomplete="off"
            >
              <label
                htmlFor="code"
                style={{
                  color: codeErr ? defaultColors.danger2 : "#fff",
                }}
              >
                {codeErr
                  ? errors.code
                  : `کد ارسال شده به ۰${toPersian(mobile)} را وارد کنید`}
              </label>
              <div
                className="loginPhoneInput"
                style={{
                  borderColor: codeErr ? defaultColors.danger2 : "#545454",
                }}
              >
                <input
                  type="text"
                  name="code"
                  id="code"
                  style={{ textAlign: "center" }}
                  autoComplete="off"
                  onChange={(e) => setIfNumber(e, setFieldValue, 6)}
                  onBlur={handleBlur}
                  value={values.code ? toPersian(values.code) : ""}
                />
              </div>
              <label
                htmlFor="loginPass"
                style={{
                  color: passErr ? defaultColors.danger2 : "#fff",
                }}
              >
                {passErr ? errors.password : "رمز عبور"}
              </label>
              <input
                type="password"
                className="mainInput"
                name="password"
                autoComplete="off"
                id="loginPass"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                style={{
                  borderColor: passErr ? defaultColors.danger2 : "#545454",
                  marginBottom: 15,
                }}
              />
              <>
                <label
                  htmlFor="confirmPassword"
                  style={{
                    color: confirmErr ? defaultColors.danger2 : "#fff",
                  }}
                >
                  {confirmErr ? errors.confirmPassword : "تکرار رمز عبور"}
                </label>
                <input
                  type="password"
                  className="mainInput"
                  name="confirmPassword"
                  autoComplete="off"
                  id="confirmPassword"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.confirmPassword}
                  style={{
                    borderColor: confirmErr ? defaultColors.danger2 : "#545454",
                  }}
                />
              </>
              <button type="submit" disabled={isSubmitting}>
                <span className="buttonLoginSubmit">تائید و ورود</span>
                <span className="la la-arrow-left"></span>
              </button>
            </Form>
          );
        }}
      </Formik>
    );
  };

  const renderForgot = () => {
    switch (auth.forgot_mode) {
      case "mobile":
        return renderMobileForm();

      case "code":
        return renderForgotCodeForm();

      default:
        return renderMobileForm();
    }
  };

  switch (code) {
    case "none":
      return renderMainForm();

    case "register":
      return renderCodeForm();

    case "forgot":
      return renderForgot();

    default:
      return renderMainForm();
  }
};

const flexRow1: string = `
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const flex: string = `
  display: flex;
  align-items: center;
`;

const Form = styled.form`
  label {
    font-size: 0.8rem;
    margin-bottom: 3px;
    display: flex;
  }
  .mainInput {
    width: 100%;
    background: #212121;
    border: 1px solid #545454;
    height: 40px;
    border-radius: 4px;
    color: #fff;
    padding: 0 5px;
    font-weight: 100;
    font-size: .9rem;
    &::placeholder {
      color: #fff;
    }
  }
  .loginPhoneInput {
    ${flexRow1}
    padding: 0 5px;
    width: 100%;
    background: #212121;
    border: 1px solid #545454;
    height: 40px;
    margin-bottom: 10px;
    border-radius: 4px;
    input {
      flex: 1;
      text-align: left;
      color: #fff;
      font-weight: 100;
      margin-left: 10px;
      font-size: .8rem;
      padding-right: 5px;
      &::placeholder {
        color: #fff;
      }
    }
    span {
      color: ${defaultColors.blue};
      font-size: .75rem;
      margin-left: 3px;
      span {
        display: inline-block;
        margin-left: 10px;
        color: #fff;
        font-weight: 500;
      }
    }
  }
  button {
    ${flexRow1}
    background-color: ${mainColors.blue};
    height: 40px;
    width: 100%;
    margin-top: 25px;
    padding: 0 15px;
    border-radius: 4px;
    .buttonLoginSubmit {
      font-weight: 500;
      font-size: .9rem;
    }
    span {
      ${flex}
      color: #fff;
    }
    .la-arrow-left {
      font-size: 1.6rem;
    }
  }
`;

export default connect((state) => state, { confirmCode })(AuthForm);
