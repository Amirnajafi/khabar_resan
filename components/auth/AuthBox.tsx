import React from "react";
import styled from "styled-components";

const AuthBox = (props) => {
  const { isActive, children, style } = props;

  return (
    <AuthContainer
      style={{
        border: `5px solid ${isActive ? "#00000000" : "#212121"}`,
        backgroundColor: isActive ? "#212121" : "#00000000",
        ...style,
      }}
    >
      {children}
    </AuthContainer>
  );
};

const AuthContainer = styled.div`
  width: 449px;
  height: 528px;
  border-radius: 4px;
  padding: 70px 50px;
  margin: 0 15px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  @media only screen and (max-width: 1050px) {
    width: 340px !important;
    padding: 0px !important;
    height: auto;
    margin-bottom: 50px;
    margin: 15px 5px 50px !important;
  }
`;

export default AuthBox;
