import React from "react";
import styled from "styled-components";
import { defaultColors } from "../../redux/reducer/colors/colorReducer";
import { flex } from "../../public/styles/modules";
import { toPersian } from "../../helpers/props";

const PackagesInfoBox = (props) => {
  const {
    type,
    imgUrl,
    title,
    description,
    renderPrice,
    percent,
    onClick,
  } = props;

  const bgColor = () => {
    switch (type) {
      case "primary":
        return defaultColors.blue4;

      case "success":
        return defaultColors.success;

      default:
        return defaultColors.blue4;
    }
  };

  return (
    <PackagesInfoBoxEL style={{ backgroundColor: bgColor() }} onClick={onClick}>
      <div className="packagesInfoImage">
        {percent && (
          <span className="packagesInfoImagePercent">
            {`٪${toPersian(percent.toString())}`}
          </span>
        )}
        <img src={imgUrl || "/img/packages/journalism.png"} alt="طرح شماره ۱" />
      </div>
      <div className="packagesInfoContent">
        <div className="packagesInfoContentOffer">
          <h3>{title}</h3>
          {renderPrice && renderPrice()}
        </div>
        <p>{description}</p>
      </div>
      <div className="packagesInfoIcon">
        <span className="la la-arrow-left"></span>
      </div>
    </PackagesInfoBoxEL>
  );
};

const PackagesInfoBoxEL = styled.div`
  ${flex}
  transition: all linear .22s;
  cursor: pointer;
  &:hover {
    transform: scale(1.03);
    opacity: 0.8;
  }
  background-color: ${defaultColors.success};
  border-radius: 4px;
  padding: 8.5px 30px;
  min-height: 110px;
  margin-bottom: 15px;
  .packagesInfoImage {
    position: relative;
    padding: 0 15px;
    @media only screen and (max-width: 600px) {
      img {
        width: 60px;
      }
    }
    img {
      vertical-align: middle;
    }
    .packagesInfoImagePercent {
      ${flex}
      justify-content: center;
      border-radius: 50%;
      position: absolute;
      top: -10px;
      left: 10px;
      background-color: ${defaultColors.danger2};
      width: 30px;
      height: 30px;
      font-size: 0.9rem;
      color: #fff;
    }
  }
  .packagesInfoIcon {
    margin-right: 15px;
    @media only screen and (max-width: 600px) {
      display: none;
    }
    span {
      color: #fff;
      font-size: 1.5rem;
    }
  }
  .packagesInfoContent {
    flex: 1;
    h3,
    p {
      color: #fff;
    }
    h3 {
      font-size: 1.1rem;
    }
    p {
      font-size: 0.75rem;
    }
  }
  .packagesInfoContentOffer {
    ${flex}
    margin-bottom: 8px;
    @media only screen and (max-width: 600px) {
      flex-direction: column;
      align-items: flex-start;
      h3 {
        order: 2;
      }
    }
  }
`;

export default PackagesInfoBox;
