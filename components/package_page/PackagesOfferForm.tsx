import React from "react";
import styled from "styled-components";
import { Formik } from "formik";
import { flex } from "../../public/styles/modules";
import Button from "../reuse/Button";
import { darkColors } from "../../redux/reducer/colors/colorReducer";

const PackagesOfferForm = (props) => {
  const { initialValues, handleValidate, handleSubmit } = props;

  return (
    <>
      <Formik
        initialValues={initialValues}
        validate={handleValidate}
        onSubmit={handleSubmit}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => {
          return (
            <>
              <PackagesOfferFormBox onSubmit={handleSubmit}>
                <input
                  type="text"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.offerCode}
                  name="offerCode"
                />
                <Button>اعمال</Button>
              </PackagesOfferFormBox>
            </>
          );
        }}
      </Formik>
    </>
  );
};

const PackagesOfferFormBox = styled.form`
  ${flex}
  margin-bottom: 30px;
  input {
    border: 1px solid ${darkColors.border6};
    flex: 1;
    height: 40px;
    border-radius: 4px;
  }
  button {
    height: 40px !important;
    width: 130px !important;
    margin-right: 15px;
  }
  @media only screen and (max-width: 600px) {
    button {
      width: 80px !important;
    }
  }
`;

export default PackagesOfferForm;
