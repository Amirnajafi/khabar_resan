import React from "react";
import styled from "styled-components";
import { defaultColors } from "../../redux/reducer/colors/colorReducer";
import { flex } from "../../public/styles/modules";
import { toPersian } from "../../helpers/props";

const PackagesPrice = (props) => {
  const { offerPrice, mainPrice } = props;

  return (
    <PackagesPriceBox>
      {mainPrice && (
        <div className="packagesMainPrice">
          {mainPrice && (
            <span>{`${toPersian(mainPrice.toString())}`} ریال</span>
          )}
          {offerPrice && <div />}
        </div>
      )}
      {offerPrice && (
        <span className="packagesOfferPrice">
          {`${toPersian(mainPrice.toString())}`} ریال
        </span>
      )}
    </PackagesPriceBox>
  );
};

const PackagesPriceBox = styled.div(() => {
  return {
    ".packagesMainPrice": {
      backgroundColor: "#fff",
      padding: "2px 5px",
      borderRadius: "4px",
      fontSize: "0.9rem",
      marginLeft: "10px",
      marginRight: "10px",
      fontWeight: 500,
      color: defaultColors.danger2,
      position: "relative",
      "@media only screen and (max-width: 600px)": {
        margin: "0 0 5px !important",
      },
      div: {
        height: "1px",
        width: "95%",
        zIndex: 100,
        position: "absolute",
        backgroundColor: "#000",
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
        margin: "auto",
        transform: "rotate(-15deg)",
      },
    },
    ".packagesOfferPrice": {
      color: "#fff",
      fontSize: "0.85rem",
      fontWeight: 500,
    },
  };
});

export default PackagesPrice;
