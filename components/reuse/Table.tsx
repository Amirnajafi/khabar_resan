import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { defaultColors } from "../../redux/reducer/colors/colorReducer";

const Table = (props) => {
  const { colors, headers, rows } = props;

  const renderHeader = () => {
    return (headers || []).map((item, index) => {
      const key = `mainTablehead${item.name}${item.id}${index}`;
      return (
        <th key={key}>
          {index !== 0 && <span>|</span>}
          {item.name}
        </th>
      );
    });
  };

  const renderRows = () => {
    return (rows || []).map((item, index) => {
      const key = `mainTableRow${item.id}${index}`;
      return (
        <tr key={key} style={{ borderColor: colors.border6 }}>
          {(headers || []).map((head, i) => {
            const headKey = `mainTableHeadKey${head.id}${i}`;
            return (
              <td style={{ color: colors.font8 }} key={headKey}>
                {i !== 0 && <span>|</span>}
                {item[head.id] || " "}
              </td>
            );
          })}
        </tr>
      );
    });
  };

  return (
    <TableBox>
      <ReuseTable>
        <thead>
          <tr style={{ backgroundColor: colors.main5 }}>{renderHeader()}</tr>
        </thead>
        <tbody>{renderRows()}</tbody>
      </ReuseTable>
    </TableBox>
  );
};

const TableBox = styled.div(() => ({
  overflowY: "auto",
}));

const ReuseTable = styled.table`
  width: 100%;
  border-collapse: collapse;
  min-width: 800px;
  td,
  th {
    color: #fff;
    text-align: right;
    font-size: 0.85rem;
    padding: 0 15px;
    border: 0;
    font-weight: normal;
    span {
      color: ${defaultColors.grey};
      margin-left: 5px;
    }
  }
  tr {
    height: 40px;
    min-width: 800px;
  }

  thead {
    th {
      color: ${defaultColors.grey};
    }
  }

  tbody {
    tr {
      border-bottom: 1px solid;
    }
  }
`;

export default connect((state) => state)(Table);
