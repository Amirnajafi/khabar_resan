import React from "react";
import styled from "styled-components";
import DropDownBtn from "./DropDownBtn";

const DropDown = (props) => {
  const { children, title, open, handleToggle, btnClassName, dpStyle } = props;

  return (
    <DropDownBox open={open} dpStyle={dpStyle}>
      <DropDownBtn
        title={title}
        onClick={handleToggle}
        className={`dropDownBtn ${btnClassName}`}
      />
      <div className="dropDownBox">{children}</div>
    </DropDownBox>
  );
};

const DropDownBox = styled.div((props) => {
  const { open, dpStyle } = props;

  return {
    ".dropDownBox": {
      height: open ? "auto" : 0,
      overflow: "hidden",
      marginBottom: 15,
      ...dpStyle,
    },
    ".dropDownBtn": {
      marginBottom: "0 !important",
    },
    position: "sticky",
    top: "30px",
  };
});

export default DropDown;
