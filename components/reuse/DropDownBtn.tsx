import React from "react";
import Button from "./Button";
import styled from "styled-components";

const DropDownBtn = (props) => {
  const { title, onClick, className } = props;

  return (
    <DropDownBtnBox onClick={onClick}>
      <Button className={className} model="darkBlue" fullWidth>
        <span>{title}</span>
        <span className="la la-angle-down"></span>
      </Button>
    </DropDownBtnBox>
  );
};

const DropDownBtnBox = styled.div((props) => {
  return {
    button: {
      marginBottom: 15,
      height: 40,
      display: "flex",
      justifyContent: "space-between",
      padding: "0 15px",
      alignItems: "center",
      span: {
        display: "flex",
        fontSize: ".9rem",
        alignItems: "center",
      },
      ".la": {
        fontSize: ".7rem",
      },
    },
  };
});

export default DropDownBtn;
