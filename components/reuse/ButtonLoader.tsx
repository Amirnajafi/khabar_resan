import React from "react";
import Loading from "../Loading";

const ButtonLoader = (props) => {
  const { children, onClick, type, className, style, disabled } = props;

  return (
    <button
      onClick={onClick}
      type={type}
      className={className}
      style={style}
      disabled={disabled}
    >
      <Loading />
      {/* {children} */}
    </button>
  );
};

export default ButtonLoader;
