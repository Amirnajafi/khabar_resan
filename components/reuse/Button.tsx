import React from "react";
import styled from "styled-components";
import {
  defaultColors,
  mainColors,
} from "../../redux/reducer/colors/colorReducer";
import { connect } from "react-redux";

const Button = (props) => {
  const {
    model,
    children,
    type,
    className,
    style,
    fullWidth,
    colors,
    onClick,
  } = props;

  const checkColors = () => {
    switch (model) {
      case "success":
        return {
          color: defaultColors.success,
          backgroundColor: defaultColors.success2,
        };

      case "danger":
        return {
          color: defaultColors.danger,
          backgroundColor: defaultColors.danger2,
        };

      case "primary":
        return {
          color: "#fff",
          backgroundColor: defaultColors.blue,
        };

      case "darkBlue":
        return {
          color: "#fff",
          backgroundColor: mainColors.blue,
        };

      case "warning":
        return {
          color: defaultColors.warn,
          backgroundColor: defaultColors.warn2,
        };

      default:
        return {
          color: "#fff",
          backgroundColor: defaultColors.blue,
        };
    }
  };

  return (
    <Btn
      colors={colors}
      fullWidth={fullWidth}
      className={`${className ? className : ""}`}
      type={type || "submit"}
      mainStyles={style}
      checkColors={checkColors}
      onClick={onClick ? onClick : null}
    >
      {children}
    </Btn>
  );
};

const Btn = styled.button((props) => {
  const { mainStyles, fullWidth, checkColors } = props;

  return {
    borderRadius: "4px",
    fontWeight: 500,
    color: checkColors().color,
    backgroundColor: checkColors().backgroundColor,
    width: fullWidth ? "100%" : 207,
    height: 45,
    ...mainStyles,
  };
});

export default connect((state) => state)(Button);
