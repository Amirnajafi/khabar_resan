import React, { useState } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { mainColors } from "../redux/reducer/colors/colorReducer";

const ToggleButton = (props) => {
  const [open, setOpen] = useState(true);
  const { text, children, colors } = props;

  const handleToggle = () => setOpen(!open);

  return (
    <>
      <ToggleContainer open={open}>
        <div
          className="toggleButton"
          onClick={() => handleToggle()}
          style={{
            backgroundColor: colors.blue2,
          }}
        >
          <span className="newsAsideToggleTextBtn">{text}</span>
          <span className={`la la-angle-${open ? "up" : "down"}`}></span>
        </div>
        <div className="toggleBox">{children}</div>
      </ToggleContainer>
    </>
  );
};

const ToggleContainer = styled.div((props) => {
  const { open } = props;

  return {
    overflow: "hidden",
    ".toggleButton": {
      cursor: "pointer",
      display: "flex",
      "align-items": "center",
      "justify-content": "space-between",
      padding: "5px 10px",
      "border-radius": "4px",
      "margin-bottom": "10px",
      span: {
        "font-size": "0.85rem",
      },
      ".newsAsideToggleTextBtn": {
        "font-weight": 500,
        color: `${mainColors.blue}`,
      },
      ".la": {
        color: "#5d5d5d",
        "font-size": "0.6rem",
      },
    },
    ".toggleBox": {
      maxHeight: open ? "auto" : 0,
    },
  };
});

export default connect((state) => state)(ToggleButton);
