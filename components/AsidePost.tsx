import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { mainColors } from "../redux/reducer/colors/colorReducer";
import { trunc, toPersian } from "../helpers/props";
import moment from "jalali-moment";
import Link from "next/link";

const AsidePost = (props) => {
  const { colors, newsDetails } = props;

  const { item } = newsDetails;
  const { similar } = item;

  return (
    <>
      <Box>
        <div
          className="asideHeaderPost"
          style={{
            backgroundColor: colors.main4,
            border: `1px solid ${colors.border1}`,
          }}
        >
          <span className="icon-miz-logo"></span>
          <span
            className="asideHeaderPostTitle"
            style={{ color: colors.font1 }}
          >
            اخبار مشابه این خبر
          </span>
        </div>
        {(similar || []).map((item, index) => {
          const key = `asideNewsPostItem${item.id}${index}`;
          if (index > 7) {
            return null;
          }

          return (
            <div className="asidePostItem" key={key}>
              <Link href={`/news/[id]`} as={`/news/${item.id}`}>
                <a
                  className="imgAsidePost"
                  style={{ backgroundImage: `url(${item.image})` }}
                ></a>
              </Link>
              <div className="contentAsidePost">
                <span className="datePostAside">
                  {toPersian(
                    moment(item.create_date, "YYYY-MM-DD hh:mm:ss")
                      .locale("fa")
                      .format("DD MMMM YYYY")
                  )}
                </span>
                <p>
                  <Link href={`/news/[id]`} as={`/news/${item.id}`}>
                    <a>{trunc(item.title, 40)}</a>
                  </Link>
                </p>
              </div>
            </div>
          );
        })}
      </Box>
    </>
  );
};

const flex: string = `
  display: flex;
  align-items: center;
`;

const Box = styled.div`
  .asideHeaderPost {
    ${flex};
    padding: 10px;
    margin-bottom: 15px;
    .icon-miz-logo {
      color: ${mainColors.blue};
      font-size: 0.7rem;
      margin-left: 10px;
    }
    .asideHeaderPostTitle {
      font-weight: 500;
      font-size: 0.9rem;
    }
  }
  .asidePostItem {
    display: flex;
    margin-bottom: 15px;
    .imgAsidePost {
      border: 1px solid #99999910;
      width: 95px;
      margin-left: 10px;
      height: 85px;
      border-radius: 4px;
      background-position: center center;
      background-size: cover;
    }
    .contentAsidePost {
      flex: 1;
      .datePostAside {
        color: #bdbdbd;
        font-size: 0.85rem;
        margin-right: 5px;
      }
      p {
        a {
          font-size: 0.9rem;
          color: ${mainColors.font3};
          font-weight: 500;
        }
      }
    }
  }
`;

export default connect((state) => state)(AsidePost);
