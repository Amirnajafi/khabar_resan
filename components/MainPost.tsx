import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import HtmlParser from "react-html-parser";
import Link from "next/link";
import { saveNews, saveOneNews } from "../redux/actions";

const MainPost = (props) => {
  const { newsDetails, auth } = useSelector((state) => state);
  const { item } = newsDetails;
  const { colors } = props;
  const dispatch = useDispatch();

  return (
    <>
      <Box colors={colors}>
        <div className="mainPostLinks">
          <Link href={auth === "user" ? "/" : "/login"}>
            <a>آخرین اخبار :: </a>
          </Link>
          <Link href={`/news/[id]`} as={`/news/${item.id}`}>
            <a>{item.title}</a>
          </Link>
        </div>
        <div className="mainPostTop">
          <div
            className="mainPostTopSource"
            style={{ backgroundColor: colors.btn, color: colors.font5 }}
          >
            {item.source_name}
          </div>
          <div className="mainPostTopDate">
            {auth.role === "user" && (
              <i
                className={`icon-bookmark bookmark-icon`}
                style={{
                  color: item.bookmark ? colors.font5 : colors.border5,
                  cursor: "pointer",
                }}
                onClick={async () => {
                  await dispatch(
                    saveOneNews(
                      item.mongo_id || item.id,
                      item.bookmark ? "delete" : "post"
                    )
                  );
                }}
              ></i>
            )}
            <span style={{ color: colors.font6 }} className="icon-clock"></span>
            <span style={{ color: colors.font6 }}>{item.date}</span>
          </div>
        </div>
        <div className="mainPostContent">{HtmlParser(item.html)}</div>
      </Box>
    </>
  );
};

const Box = styled.article((props) => {
  const { colors } = props;

  return {
    ".mainPostLinks": {
      "margin-bottom": "30px",
      a: {
        color: "#b2b2b2",
        "font-weight": 500,
        "font-size": "0.85rem",
        display: "inline-block",
        "margin-left": "5px",
      },
    },
    ".mainPostTop": {
      display: "flex",
      "align-items": "center",
      "margin-bottom": "30px",
      ".mainPostTopSource": {
        "font-size": "0.9rem",
        "border-radius": "4px",
        padding: "3px 13px",
        "font-weight": 500,
        "margin-left": "10px",
      },
      ".mainPostTopDate": {
        display: "flex",
        "align-items": "center",
        ".icon-clock": {
          display: "flex",
          "align-items": "center",
          "font-size": "1.2rem",
          "margin-left": "10px",
          "&::before": {
            margin: "0 !important",
          },
        },
        ".bookmark-icon": {
          fontSize: "1.2rem",
          marginLeft: 10,
          cursor: "pointer",
        },
      },
    },
    ".mainPostContent": {
      h1: {
        color: colors.font1,
        "font-size": "1.6rem",
        "text-align": "justify",
        "margin-bottom": "15px",
      },
      img: {
        "max-width": "100%",
        "border-radius": "4px",
        "margin-bottom": "15px",
      },
      video: {
        maxWidth: "100%",
      },
      p: {
        color: colors.font1,
        "text-align": "justify",
        "margin-bottom": "15px",
      },
      div: {
        color: colors.font1,
        "text-align": "justify",
        "margin-bottom": "15px",
      },
    },
  };
});

export default connect((state) => state)(MainPost);
