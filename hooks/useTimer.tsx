import { useState, useEffect } from "react";

const mainTime = 60;

const useTimer = () => {
  let [time, setTime] = useState(mainTime);
  const [isFinish, setIsFinish] = useState(false);

  const reset: any = () => {
    time = mainTime;
    setTime(time);
    setIsFinish(false);
    const setter = setInterval(() => {
      if (time < 1) {
        clearInterval(setter);
        setIsFinish(true);
        return;
      }

      time = time - 1;
      setTime(time);
    }, 1000);
  };

  useEffect(() => {
    const setter = setInterval(() => {
      if (time < 1) {
        clearInterval(setter);
        setIsFinish(true);
        return;
      }

      time = time - 1;
      setTime(time);
    }, 1000);
    return () => clearInterval(setter);
  }, []);

  return [time, isFinish, reset];
};

export default useTimer;
