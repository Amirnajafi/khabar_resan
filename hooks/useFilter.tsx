import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { editProfile, fetchNews } from "../redux/actions";
import { toPersian } from "../helpers/props";

export default (setShowFilter) => {
  const { user } = useSelector((state) => state);
  const dispatch = useDispatch();

  const [filterTxt, setFilterTxt] = useState("");
  const [keywordTxt, setKeywordTxt] = useState("");
  const [secondRender, setSecondRender] = useState(false);

  const getKeyWords = () => {
    let arr = [];
    for (let i = 0; i < activeFilter().keywords.length; i++) {
      const item = activeFilter().keywords[i];
      if (item.active) {
        arr.push(item.name);
      }
    }
    return arr;
  };

  useEffect(() => {
    setSecondRender(true);
  }, []);

  useEffect(() => {
    if (secondRender) {
      dispatch(fetchNews());
    }
  }, [user.item.filters]);

  const getFilters = () => ({
    sources: activeFilter().sources,
    keywords: getKeyWords(),
  });

  const setActiveFilter = async (index) => {
    const { item } = user;
    const { filters } = item;
    let mirrorFilters = [...filters];
    for (let i = 0; i < mirrorFilters.length; i++) {
      const _item = mirrorFilters[i];
      if (i === index) {
        _item.active = true;
      } else {
        _item.active = false;
      }
    }
    const data = { ...item, filters: [...mirrorFilters] };
    await dispatch(editProfile(data));
  };

  const handleFilterTxtChange = (txt) => {
    if (txt.length < 23) {
      setFilterTxt(txt);
    }
  };

  const handleKyewordTxtChange = (txt) => {
    if (txt.length < 15) {
      setKeywordTxt(txt);
    }
  };

  const addNewFilter = async (loadingCallback?, customText?, obj = {}) => {
    if ((filterTxt.trim() && filterTxt.trim().length > 3) || customText) {
      const { item } = user;
      const payload = {
        name: customText
          ? `${customText} ${toPersian(
              item.customFilterNumber ? item.customFilterNumber + 1 : 1
            )}`
          : filterTxt,
        keywords: [],
        sources: [],
        active: true,
        ...obj,
      };

      setActiveFilter(-1);
      setShowFilter(false);

      const filters = item.filters ? [...item.filters, payload] : [payload];
      setFilterTxt("");

      const data = {
        ...item,
        filters,
        customFilterNumber: customText
          ? item.customFilterNumber + 1
          : item.customFilterNumber,
      };
      if (loadingCallback) loadingCallback(true);
      await dispatch(editProfile(data));
      if (loadingCallback) loadingCallback(false);
    }
  };

  const deleteFilter = async (index, loadingCallback?) => {
    const { item } = user;
    const { filters } = item;

    if (filters) {
      let filteredArr = filters.filter((filter, i) => i !== index);
      const data = { ...item, filters: [...filteredArr] };
      if (loadingCallback) loadingCallback(true);
      await dispatch(editProfile(data));
      if (loadingCallback) loadingCallback(false);
    }
  };

  const addNewOption = async (id, removeIndex?, changeActiveIndex?) => {
    const { item } = user;
    const { filters } = item;
    const activeFilter = filters.filter((filter) => filter.active === true)[0];
    if (!activeFilter) {
      if (id === "changeKeyword") {
        addNewFilter(null, "فیلتر اختصاصی", {
          keywords: [{ name: keywordTxt, active: true }],
          sources: [],
        });
      } else {
        addNewFilter(null, "فیلتر اختصاصی", {
          keywords: [],
          sources: [id],
        });
      }
      setKeywordTxt("");
      return;
    }
    const { sources, keywords, name, active } = activeFilter;
    let payload = { sources, keywords, name, active };

    switch (id) {
      case "changeKeyword":
        if (removeIndex) {
          let keyArray = payload.keywords.filter(
            (a, i) => i !== removeIndex.index
          );
          payload.keywords = keyArray;
        } else {
          if (changeActiveIndex) {
            let keyArray = [...payload.keywords];
            for (let i = 0; i < keyArray.length; i++) {
              const itemKeyArray = keyArray[i];
              if (i === changeActiveIndex.index) {
                itemKeyArray.active
                  ? (itemKeyArray.active = false)
                  : (itemKeyArray.active = true);
              }
            }
          } else {
            !keywords.includes(keywordTxt) &&
              keywordTxt.trim() &&
              (payload.keywords = [
                ...keywords,
                { name: keywordTxt, active: true },
              ]);
          }
        }
        break;

      default:
        sources.includes(id)
          ? (payload.sources = [...sources.filter((source) => source !== id)])
          : (payload.sources = [...sources, id]);
        break;
    }

    let mirrorFilters = [...filters];

    mirrorFilters[filters.indexOf(activeFilter)] = payload;

    const data = { ...item, filters: [...mirrorFilters] };

    if (id === "changeKeyword" && !removeIndex) {
      setKeywordTxt("");
    }

    await dispatch(editProfile(data));
  };

  const activeFilter: any = (type?) => {
    if (user.item.filters) {
      const _activeFilter = user.item.filters.filter(
        (item) => item.active === true
      )[0];
      if (_activeFilter) {
        return type && type === "checkExists" ? true : { ..._activeFilter };
      }
    }
    return type && type === "checkExists" ? false : {};
  };

  return {
    filterTxt,
    setFilterTxt: handleFilterTxtChange,
    addNewFilter,
    deleteFilter,
    keywordTxt,
    handleKyewordTxtChange,
    addNewOption,
    activeFilter,
    setActiveFilter,
    getKeyWords,
    getFilters,
  };
};
